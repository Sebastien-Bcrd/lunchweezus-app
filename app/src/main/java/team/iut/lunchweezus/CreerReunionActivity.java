package team.iut.lunchweezus;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.Beans.Geoloc.DataAdresse;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.ui.Invites.InviterListeSaved;
import team.iut.lunchweezus.ui.utils;

public class CreerReunionActivity extends ActivityFonction implements AdapterView.OnClickListener, AdapterView.OnItemSelectedListener{

    //variables pour remplir la liste
    public ArrayList<Profil> profils_saved;

    //variables pour la PHOTO
    public Bitmap bitmap;
    public static final int READ_EXTERNAL_STORAGE = 0;//variable pour identifier l'option dont on accpete
    private static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private static final int PICK_IMAGE_GALLERY_REQUEST_CODE = 609;
    String path;

    private ImageView imgUpImage;
    private EditText editTextDateRe, editTextHeureRe;
    private TextInputLayout objet_layout, adresse_layout, ville_layout, place_layout, Dure_layout;
    private TextInputEditText objet_edit_text, place_edit_text, Dure_edit_text, adresse_edit_text;
    private AutoCompleteTextView ville_edit_text;
    private RelativeLayout layoutUpImage;
    public InviterListeActivity inviterListeActivity;
    public boolean inviterOuvert = false;
    private Spinner spnTypeReunion;
    private TextView errorSpnTxt;
    private String typeReunion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_reunion);
        inviterOuvert = false;

        Debut();
        profils_saved = new ArrayList<>();//garde la liste choisi pour la montrer apres

        Toolbar creertoolbar = findViewById(R.id.creer_toolbar);
        setSupportActionBar(creertoolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Créer une réunion");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        creertoolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        creertoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!inviterOuvert) {
                    ChangeActivity(CreerReunionActivity.this, AccueilActivity.class);
                    finish();
                    overridePendingTransition(R.anim.creer_accueil_up, R.anim.creer_accueil_off);
                } else {
                    creerListeSaved();
                    inviterListeActivity.getFragmentManager().beginTransaction().remove(inviterListeActivity).commit(); // Permet de fermer l'activité listInvité
                    inviterOuvert = false;
                }
            }
        });
        imgUpImage = findViewById(R.id.imgUpImage);
        editTextDateRe = findViewById(R.id.editTextDateRe);
        editTextHeureRe = findViewById(R.id.editTextHeureRe);
        objet_layout = findViewById(R.id.objet_layout);
        adresse_layout = findViewById(R.id.adresse_layout);
        ville_layout = findViewById(R.id.ville_layout);
        place_layout = findViewById(R.id.place_layout);
        Dure_layout = findViewById(R.id.Dure_layout);
        Dure_edit_text = findViewById(R.id.Dure_edit_text);
        objet_edit_text = findViewById(R.id.objet_edit_text);
        adresse_edit_text = findViewById(R.id.adresse_edit_text);
        ville_edit_text = findViewById(R.id.ville_edit_text);
        place_edit_text = findViewById(R.id.place_edit_text);
        layoutUpImage = findViewById(R.id.layout_UpImage);

        FloatingActionButton FABaccepterRe = findViewById(R.id.FABaccepterRe);
        Button btnInviter = findViewById(R.id.btnInviter);


        spnTypeReunion = findViewById(R.id.spnTypeReunion);
        getTypeReunion();


        layoutUpImage.setOnClickListener(    new View.OnClickListener() {
            @Override
            //Differents Permissions pour la CAMERA et le Telephone GALLERY
            public void onClick(View view) {
                if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
                    }
                } else {
                    callgalery();
                }
            }
        });

        FABaccepterRe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> values = new ArrayList<>();

                LinearLayout errorSpn = (LinearLayout) spnTypeReunion.getSelectedView();
                errorSpnTxt = errorSpn.findViewById(R.id.txtSpinner_type_reunion);

                //verifier si les champs ne sont pas vide
                if (Dure_edit_text.getText().toString().isEmpty()) {
                    Dure_layout.setError("Requis"); // hide error
                } else {
                    Dure_layout.setError(null); // hide error
                    values.add("Durée");
                }

                if (adresse_edit_text.getText().toString().isEmpty()) {
                    adresse_layout.setError("Requis");
                } else {
                    adresse_layout.setError(null);
                    values.add("Adresse");
                }

                if (ville_edit_text.getText().toString().isEmpty()) {
                    ville_layout.setError("Requis");
                } else {
                    ville_layout.setError(null);
                    values.add("Ville");
                }

                if (place_edit_text.getText().toString().isEmpty()) {
                    place_layout.setError("Requis");
                } else if (Integer.parseInt(place_edit_text.getText().toString()) <= 1) {
                    place_layout.setError("Pas assez de places");
                } else {
                    place_layout.setError(null);
                    values.add("Places");
                }

                if (editTextDateRe.getText().toString().isEmpty()) {
                    editTextDateRe.setError("Requis");
                } else {
                    editTextDateRe.setError(null);
                    values.add("Date");
                }

                if (spnTypeReunion.getSelectedItemPosition() == 0) {
                    errorSpnTxt.setTextColor(getResources().getColor(R.color.colorRefuse));
                } else {
                    errorSpnTxt.setTextColor(getResources().getColor(R.color.textWhite));
                    values.add("" + spnTypeReunion.getSelectedItemPosition());
                }

                Date inputTime;
                Date inputDate;
                try {
                    inputTime = new SimpleDateFormat("HH:mm").parse(editTextHeureRe.getText().toString());
                    int inputTimeCompare = inputTime.getMinutes() + (inputTime.getHours() * 100);
                    inputDate = new SimpleDateFormat("dd-MM-yyyy").parse(editTextDateRe.getText().toString());

                    Date currentTime = Calendar.getInstance().getTime();
                    int currentTimeCompare = currentTime.getMinutes() + (currentTime.getHours() * 100);
                    Date currentDate = new SimpleDateFormat("dd-MM-yyyy").parse(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

                    if (editTextHeureRe.getText().toString().isEmpty()) {
                        editTextHeureRe.setError("Requis");
                    } else if (inputTimeCompare <= currentTimeCompare && inputDate.equals(currentDate)) {
                        editTextHeureRe.setError("Vous ne pouvez pas créer de réunion avant l'heure actuelle");
                    } else {
                        editTextHeureRe.setError(null);
                        values.add("Heure");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (objet_edit_text.getText().toString().isEmpty()) {
                    objet_layout.setError("Requis");
                } else {
                    objet_layout.setError(null);
                    values.add("Objet");
                }

                if (values.size() == 8) {
                    utils.toaster(CreerReunionActivity.this, "Envoi en cours...", 0);
                    Reunion reunion = new Reunion();
                    reunion.objet_reunion = objet_edit_text.getText().toString();
                    reunion.duree_reunion = Dure_edit_text.getText().toString();
                    reunion.adresse_reunion = adresse_edit_text.getText().toString();
                    reunion.lieu_reunion = ville_edit_text.getText().toString();
                    reunion.nbPlace_reunion = Integer.parseInt(place_edit_text.getText().toString());
                    reunion.date_reunion = editTextDateRe.getText().toString();
                    reunion.heure_reunion = editTextHeureRe.getText().toString();
                    reunion.type_reunion = typeReunion;

                    if (bitmap != null)
                        reunion.photo_reunion = convertImgString(bitmap);
                    reunion.id_profil = sys.userProfil.id_profil;

                    if (profils_saved.size() < reunion.nbPlace_reunion) {
                        /// Création tableau pour encode JSon l'id du profil avec l'id de la réunion
                        int[] idInviter = new int[profils_saved.size()];
                        for (int i = 0; i < profils_saved.size(); i++) {
                            idInviter[i] = profils_saved.get(i).id_profil;
                        }
                        String inviter = new Gson().toJson(idInviter);
                        sys.AddReunion(reunion, inviter);
                    } else {
                        utils.toaster(CreerReunionActivity.this, "Vous invitez trop de personnes !", 0);
                    }
                }
            }
        });
        btnInviter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                inviterListeActivity = new InviterListeActivity();
                transaction.replace(R.id.cont_liste, inviterListeActivity);
                transaction.addToBackStack(null);
                transaction.commit();
                sys.getAllProfils();
            }
        });

        appelApi("");
    }

    /**
     * Permet d'afficher la liste des utilisateurs invités sur le formulaire
     */
    public void creerListeSaved() {
        FrameLayout frameLayout = findViewById(R.id.cont_saved);
        frameLayout.removeAllViews();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.cont_saved, new InviterListeSaved());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * CSystem fait appel à cette fonction quand la récupération des profils a réussi, permet de les
     * afficher sur le model de sélection de personnes à inviter
     *
     * @param profils
     */
    @Override
    public void SuccessGetAllProfil(final List<Profil> profils) {
        inviterListeActivity.dataset(profils);
    }

    //implementaire OnClick sur l'image Calendar et Horloge
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgcalendarRe:
                final Calendar calendar = Calendar.getInstance();
                //variables pour la DATE
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int years, int Months, int DayOfMonth) {

                        String mois, jour;
                        if ((Months + 1) < 10) {
                            mois = "0" + (Months + 1);
                        } else {
                            mois = "" + (Months + 1);
                        }
                        if ((DayOfMonth) < 10) {
                            jour = "0" + DayOfMonth;
                        } else {
                            jour = "" + DayOfMonth;
                        }

                        String dateReunion = jour + "-" + mois + "-" + years;
                        editTextDateRe.setText(dateReunion);//Commence par years apres Months et Days
                    }
                },
                        year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                datePickerDialog.getButton(DatePickerDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
                datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                break;
            case R.id.imgOrlogeRe:
                final Calendar calendarO = Calendar.getInstance();
                int hour = calendarO.get(Calendar.HOUR_OF_DAY);
                int minute = calendarO.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int Minute) {
                        editTextHeureRe.setText(hourOfDay + ":" + Minute);
                    }
                }, hour, minute, true);
                timePickerDialog.show();
                timePickerDialog.getButton(TimePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                timePickerDialog.getButton(TimePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                timePickerDialog.getButton(TimePickerDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                callgalery();
        }

    }

    /**
     * Crée un modal listant les choix possibles pour la source de l'image à envoyer
     */
    private void callgalery() {

        final CharSequence[] options = {"Caméra", "Galerie", "Annuler"};
        final AlertDialog.Builder alertOp = new AlertDialog.Builder(CreerReunionActivity.this, R.style.AlertDialogCustom);
        alertOp.setTitle("Choisissez une source");
        alertOp.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (options[i].equals("Caméra")) {
                    prendrePhoto();
                } else {
                    if (options[i].equals("Galerie")) {
                        openImagesDocument();
                    } else {
                        dialogInterface.dismiss();
                    }
                }
            }
        });
        alertOp.show();
    }

    /**
     * Ouvre l'appareil photo pour prendre une photo
     */
    private void prendrePhoto() {
        //CHEMIN de la photo prise depuis la CAMERA
        //varible qu'on crée pour la photo prise depuis la CAMERA
        File fileImage = new File(Environment.getExternalStorageDirectory(), "LunchWeezUs");
        boolean isCreate = fileImage.exists();
        String nom_img = "";
        if (isCreate) {
            nom_img = System.currentTimeMillis() / 1000 + ".jpg";
        }
        path = Environment.getExternalStorageDirectory() + File.separator + "LunchWeezUs" + File.separator + nom_img;

        File imgUpImage = new File(path);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri imageUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = getApplicationContext().getPackageName() + ".provider";
            imageUri = FileProvider.getUriForFile(CreerReunionActivity.this, authorities, imgUpImage);
        } else {
            imageUri = Uri.fromFile(imgUpImage);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        startActivityForResult(intent, CAMERA_ACTION_PICK_REQUEST_CODE);
    }

    /**
     * Ouvre le navigateur de documents pour choisir une image
     */
    private void openImagesDocument() {
        Intent pictureIntent = new Intent(Intent.ACTION_PICK);
        pictureIntent.setType("image/*");
        startActivityForResult(pictureIntent, PICK_IMAGE_GALLERY_REQUEST_CODE);
    }

    /**
     * Selon le retour des fonctions précédentes, on ouvre un découpeur d'image pour que l'utilisateur
     * découpe l'image au format nécessaire, ou on récupère l'image et l'affiche si la découpe vient
     * d'être effectuée. Cette fonction est gérée automatiquement par les activitées
     *
     * @param requestCode Code de requête
     * @param resultCode  Code de résultat
     * @param data        Données reçues depuis l'activité qui vient d'être fermée (appareil photo, documents,
     *                    ou découpeur d'image)
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_ACTION_PICK_REQUEST_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                //savoir si le processus a fini ou pas
                                public void onScanCompleted(String s, Uri uri) {
                                    Log.i("Chemin d'hebergement", "Path: " + path);
                                    System.out.println("Path: " + path);
                                }
                            });
                    uri = Uri.parse("file:" + path);
                    openCropActivity(uri, uri);
                    break;

                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        uri = UCrop.getOutput(data);
                        showImage(uri);
                    }
                    break;

                case PICK_IMAGE_GALLERY_REQUEST_CODE:

                    File file = null;
                    try {
                        Uri sourceUri  = data.getData();
                        file = getImageFile();
                        Uri destinationUri = Uri.fromFile(file);
                        openCropActivity(sourceUri, destinationUri);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * Permet d'encoder l'image en base 64 avant l'envoi au serveur. Cette fonction compresse
     * également l'image au format jpeg.
     *
     * @param bitmap Image à encoder
     * @return Retourne l'image convertie en base 64
     */
    public String convertImgString(Bitmap bitmap) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, array);
        byte[] imageByte = array.toByteArray();

        return Base64.encodeToString(imageByte, Base64.DEFAULT);
    }

    /**
     * La classe CSystem fait appel à cette fonction une fois que la création d'une réunion a réussi
     *
     * @param resultat   Vaut true si la réunion a été créée avec succès, false si la création a échoué
     * @param id_reunion ID de la réunion, permet de l'afficher ensuite
     */
    public void SuccessAddReunion(boolean resultat, int id_reunion) {
        if (resultat) // Si la réunion est bien ajouter
        {
            utils.toaster(this, "Réunion créée avec succès !", 0);
            Reunion reunionCreer = new Reunion();
            reunionCreer.setId(id_reunion);
            sys.reunionSelected = reunionCreer;
            sys.siViensDeCreeReu = true;
            ChangeActivity(CreerReunionActivity.this, ReunionDetailsActivity.class);
            finish();
        } else // Si la réunion n'a pu être ajouter
        {
            utils.toaster(this, "La création de la réunion a échoué", 0);
        }
    }

    /**
     * Si la réunion n'a pas pu être créé, la classe CSystem fait appel à cette fonction
     *
     * @param message Message d'erreur
     */
    public void ErreurAddReunion(String message) {
        utils.toaster(getApplicationContext(), "La création de la réunion a échoué", 0);
    }

    /**
     * Ouvre le découpeur d'image
     *
     * @param sourceUri      Image source
     * @param destinationUri Chemin de destination, où sauvegarder l'image
     */
    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.white));
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(1010, 720)
                .withAspectRatio(87, 62)
                .start(this);
    }

    /**
     * Permet d'afficher l'image sur le formulaire
     *
     * @param imageUri Chemin de l'image
     */
    private void showImage(Uri imageUri) {
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), imageUri);
            imgUpImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Appelle l'API Foursquare pour afficher les restaurants sur la case "Etablissement" du formulaire
     *
     * @param query Ce que l'utilsateur a écrit dans la case "Etablissement"
     */
    public void appelApi(String query) {
        if (sys.dataAdresses == null) {
            sys.ApiGeoloc(query);
        } else {
            successAppelApi(sys.dataAdresses);
        }
    }

    /**
     * CSystem fait appel à cette fonction quand l'appel à l'API Foursquare a réussi
     *
     * @param dataAdresses Liste contenant les adresses trouvées par l'API
     */
    @Override
    public void successAppelApi(List<DataAdresse> dataAdresses) {
        sys.dataAdresses = dataAdresses;

        String[] data = new String[dataAdresses.size()];
        for (int i = 0; i < dataAdresses.size(); i++) {
            if (dataAdresses.get(i).getLocation().getAddress() == null) {
                dataAdresses.get(i).getLocation().setAddress("non renseigné");
            }
            data[i] = dataAdresses.get(i).getName() + ", " + dataAdresses.get(i).getLocation().getAddress();
        }

        final AutoCompleteTextView etab = ville_edit_text;
        final TextInputEditText adresse = adresse_edit_text;
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.combobox, data);
        etab.setAdapter(arrayAdapter);

        etab.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView textView = (TextView) view;
                String[] res = textView.getText().toString().split(",");

                etab.setText(res[0]);
                adresse.setText(res[1].substring(1));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!inviterOuvert) {
            ChangeActivity(CreerReunionActivity.this, AccueilActivity.class);
            finish();
            overridePendingTransition(R.anim.creer_accueil_up, R.anim.creer_accueil_off);
        } else {
            creerListeSaved();
            inviterOuvert = false;
        }
    }

    /**
     * Permet d'initiliser le spinner qui permet de choisir le type de reunion
     */
    public void getTypeReunion() {
        final String[] strTypeReunion = new String[3];
        strTypeReunion[0] = "Type";

        strTypeReunion[1] = "Privée";
        strTypeReunion[2] = "Publique";

        spnTypeReunion.setOnItemSelectedListener(CreerReunionActivity.this);
        List<String> listTypeReunion = new ArrayList<>();
        Collections.addAll(listTypeReunion, strTypeReunion);
        ArrayAdapter<String> cmbAdapter = new ArrayAdapter<>(this, R.layout.spineer_type_reunion_liste, R.id.txtSpinner_type_reunion, listTypeReunion);
        spnTypeReunion.setAdapter(cmbAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spnTypeReunion) {
            typeReunion = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // DO NOT REMOVE
    }
    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists())
            System.out.println("File exists");
        else
            System.out.println("File not exists");
        File file = File.createTempFile(
                imageFileName, ".jpg", storageDir
        );
        String currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }
}
