package team.iut.lunchweezus.Logique;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Est la classe qui permettra d'envoyer les requetes au Web Service
 */
public class RequestQueueSingleton {

    private static RequestQueueSingleton instance;
    private RequestQueue requestQueue;
    private static Context context;

    /**
     * Contructeur de la class
     * @param contexte est l'activité actuel
     */
    public RequestQueueSingleton(Context contexte) {
        context = contexte;
        requestQueue = getRequestQueue();

    }

    public static synchronized RequestQueueSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new RequestQueueSingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * Ajoute la requete a envoyer
     * @param req est l'objet Request que l'on va envoyer
     * @param <T> est l'objet du type de Request
     */
    public <T> void addToRequestQueue(Request<T> req) {

        getRequestQueue().add(req);
    }
}
