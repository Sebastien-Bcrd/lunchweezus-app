package team.iut.lunchweezus.Logique;

import android.app.AlertDialog;
import android.content.Context;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Result;

import team.iut.lunchweezus.AccueilActivity;
import team.iut.lunchweezus.ActivityFonction;
import team.iut.lunchweezus.Beans.Accueil;
import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Beans.DetailReunion;
import team.iut.lunchweezus.Beans.Geoloc.DataAdresse;
import team.iut.lunchweezus.Beans.Geoloc.DataApi;
import team.iut.lunchweezus.Beans.Note;
import team.iut.lunchweezus.Beans.Notification;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Resultat;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Beans.Site;
import team.iut.lunchweezus.LoginActivity;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.Sqlite.CommentaireDataSource;
import team.iut.lunchweezus.Sqlite.InviterDataSource;
import team.iut.lunchweezus.Sqlite.ParticipantDataSource;
import team.iut.lunchweezus.Sqlite.ProfilDataSource;
import team.iut.lunchweezus.Sqlite.ReunionDataSource;
import team.iut.lunchweezus.Sqlite.JWTDataSource;
import team.iut.lunchweezus.ui.utils;

/**
 * CSystem est le système logique qui se fera passer entre les activités
 * Permettra le lien entre la communication du webService et l'affichage
 */
public class CSystem implements Serializable {

    private static final boolean modeDebug = false; // Permet d'activité le mode débug

    public Profil userProfil = null; // Contiendra le profil de l'utilisateur

    private CActivitys activitys; /// Contiens les activités
    private ActivityFonction actualActivity; /// Contiens l'activité actuel

    //public boolean connected = true; // Permet de savoir si l'on est connecté ou pas
    //public boolean started; // Permet de savoir si le système est starté
    //private boolean etat = false;
    public boolean siViensDeCreeReu = false; // si l'activity précédente est CreeReunionActivity
    public Reunion reunionSelected = null; // Contien la réunion selectionné
    public DetailReunion detailReunionSelected = null; // Contien le détail de réunion Actuel
    public int status = -1;

    //public List<Reunion> AllReunions; // Contiendra la list de toute les réunions
    public List<DataAdresse> dataAdresses; // Contiendra tout la list des adresses recu par l'api, cela nous évitera de rappler plusieur fois l'api

    private List<Reunion> reunions; // Contiendra la liste complete des réunions
    //private List<Commentaire> comments;
    private GsonBuilder gt; // Contiendra la classe permettant de faire les convertions JSON to class

    /// Ici on a toute les classes pour Sqlite
    public ProfilDataSource profilDataSource;
    public ReunionDataSource reunionDataSource;
    public ParticipantDataSource participantDataSource;
    public InviterDataSource inviterDataSource;
    public JWTDataSource JWTDataSource;
    public CommentaireDataSource commentaireDataSource;

    private RequestQueueSingleton requestQueueSingleton; // Contien l'objet qui permet d'envoyer les requêtes

    String note = "";
    private String baseURL = "http://soda-armatislc.com/lunch/index.php?"; //   "http://51.83.40.171/index2.php?";  ||    "http://soda-armatislc.com/lunch/index.php?" || http://192.168.1.96/apilunchweezus-master/index.php?   http://lwu.julienb.fr/index.php?

    public static final String URL_SITE = "http://soda-armatislc.com/lunch"; // http://soda-armatislc.com/lunch            ||       http://lwu.julienb.fr


    /**
     * Constructeur par défaut
     */
    public CSystem() {

    }

    /**
     * Permet de de lancer le CSytem quand on arrive sur une nouvelle activité
     * Est appeler par Debut() de FonctionActivity
     * Doit etre appelé par la fonction Debut() de FonctionActivity dans chaque nouvelle activity qui hérite de FonctionActivity
     *
     * @param actualActivity l'activité actuelle qui étend de FonctionActivity
     */
    public void Start(ActivityFonction actualActivity) {
        this.actualActivity = actualActivity;
        activitys = new CActivitys();
        activitys.Initialise(actualActivity);
        profilDataSource = new ProfilDataSource(actualActivity);
        reunionDataSource = new ReunionDataSource(actualActivity);
        participantDataSource = new ParticipantDataSource(actualActivity);
        inviterDataSource = new InviterDataSource(actualActivity);
        JWTDataSource = new JWTDataSource(actualActivity);
        commentaireDataSource = new CommentaireDataSource(actualActivity);

        requestQueueSingleton = new RequestQueueSingleton(actualActivity);
        gt = new GsonBuilder();


    }

    /**
     * Permet de stopper le systeme avant de lancer une nouvelle activité
     * Car pour passer CSysteme entre les activités on doit le sérialisé donc on set à null tout les attribu non sérializable
     * Est Appeler par la fonction ChangeActivity de FonctionActivity
     */
    public void Stop() {
        activitys = null;
        profilDataSource = null;
        reunionDataSource = null;
        participantDataSource = null;
        inviterDataSource = null;
        commentaireDataSource = null;
        requestQueueSingleton = null;
        JWTDataSource = null;
        gt = null;
        actualActivity = null;
        requestQueueSingleton = null;
    }


    ///// FONCTIONS STOCKAGE LOCAL

    /**
     * Permet de faire connaître un profil à CSyteme qui sera skocker dans userProfil
     * Range un profil dans SQlite pour le garder en mémoire
     *
     * @param prfl le profil utilisateur
     * @return
     */
    public long SetUserProfil(Profil prfl) {

        prfl.setId_profil(0); // On placera le profil utilisateur en index 0
        profilDataSource = new ProfilDataSource(this.actualActivity);
        profilDataSource.deleteProfil(prfl);
        long t = profilDataSource.insertProfil(prfl);

        userProfil = prfl;
        return t;
    }

    /// Permet de récupérer le profil utilisateur local

    /**
     * Permet de récupérer le profil utilisateur en local depuis SQlite
     *
     * @return le Profil utilisateur de type Profil
     */
    public Profil GetUserProfilLocal() {
        Profil profil = profilDataSource.getProfilById(0); // va chercher tout les profils.
        if (profil != null) // Vérifie si la liste n'est pas vide
        {
            userProfil = profil;
            return userProfil;
        }
        return null;
    }

    /**
     * Ajout un objet DetailReunion en local dans SQlite
     * Range chaque attribu dans les bonnes tables de SQlite
     *
     * @param d un Objet DetailReunion
     */
    public void AddLocalDetailReunion(DetailReunion d) {
        reunionDataSource.deleteReunion(d.getReunion()[0]);
        reunionDataSource.insertReunion(d.getReunion()[0]);
        profilDataSource.deleteProfil(d.getOrganisateur()[0]);
        profilDataSource.insertProfil(d.getOrganisateur()[0]);
        for (int i = 0; i < d.getParticipants().length; i++) {
            profilDataSource.deleteProfil(d.getParticipants()[i]);
            profilDataSource.insertProfil(d.getParticipants()[i]);
            participantDataSource.DeleteParticipant(d.getParticipants()[i], d.getReunion()[0]);
            participantDataSource.insertParticipant(d.getParticipants()[i], d.getReunion()[0]);
        }

        for (int i = 0; i < d.getComments().length; i++) {
            commentaireDataSource.deleteCommentaire(d.getComments()[i]);
            commentaireDataSource.insertCommentaire(d.getComments()[i], d.getReunion()[0]);
        }
    }

    /**
     * Permet de récupérer un objet DetailReunion depuis SQlite
     *
     * @param id_reunion l'id de la réunion que l'on veut
     * @return si cette réunion existe dans SQlite alors on return un objet DetailReunion sinon null
     */
    private DetailReunion GetLocalDetailReunion(int id_reunion) {
        try {
            Reunion[] reu = new Reunion[1];
            reu[0] = reunionDataSource.getReunionById(id_reunion);
            Profil[] organisateur = new Profil[1];
            organisateur[0] = profilDataSource.getProfilById(reu[0].id_profil);
            List<Profil> participants = participantDataSource.getAllProfilsWithReunion(reu[0], profilDataSource);
            Profil[] part = participants.toArray(new Profil[0]);
            List<Commentaire> commentaires = commentaireDataSource.getCommentairesWithreunion(reu[0]);
            DetailReunion detail = new DetailReunion();
            detail.setComments(commentaires.toArray(new Commentaire[0]));
            detail.setParticipants(part);
            detail.setReunion(reu);
            detail.setOrganisateur(organisateur);
            return detail;
        } catch (Exception e) {
            return null;
        }
    }

    ///// FONCTIONS
    ///// RECUPERATION
    ///// WEB SERVICE

    ////////////////////////////////////
    /* FONCTIONS INSCRIPTION-CONNEXION
     /*********************************/

    /**
     * Permet de s'inscrire
     * Envoie les données sur le WebService et appel la fonction SuccessInscription() de InscriptionActivity en cas de succés
     * et appel ErreurInscription() de InscriptionActivity en cas d'erreur
     *
     * @param prfl le profil que l'on va inscrire
     * @param pwd  le mot de passe du profil que l'on va inscrire
     */
    public void inscrireProfil(final Profil prfl, final String pwd) {
        final CSystem sys = actualActivity.sys;
        final String url = baseURL + "action=SignUp";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Resultat res = gt.getResultatFromJson(response);
                        if (res.getRes()) {
                            sys.activitys.inscriptionActivity.SuccessInscription();
                        } else {
                            sys.activitys.inscriptionActivity.ErreurInscription(res.getMsg());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.activitys.inscriptionActivity.ErreurInscription("Problème de connexion avec le serveur");
            }

        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nom", prfl.getNom_profil());
                params.put("prenom", prfl.getPrenom_profil());
                params.put("mail", prfl.getMatricule_profil());
                params.put("password", pwd);
                params.put("site", "" + prfl.getId_site());
                params.put("fonction", "" + prfl.getFonction_profil());
                params.put("reponse_profil", "" + prfl.getReponse_profil());
                Log.i("DebugRSecrrete", "*********************" + prfl.getReponse_profil());
                params.put("token", FirebaseInstanceId.getInstance().getToken());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(stringRequest);
    }

    /**
     * Permet de vérifier la connexion user
     * Envoie les données au WebService et appel la fonction Success() de LoginActivity en cas de succés
     * et appel Error() de LoginActivity en cas d'erreur
     *
     * @param login    le login de connexion
     * @param password le mot de passe de connexion
     */
    public void CheckPassword(final String login, final String password) {
        final String url = baseURL + "action=SignIn";
        final CSystem sys = this;

        final StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Profil profil = gt.getProfilFromJson(jsonObject.getString("0"));
                    if (profil.nom_profil != null && profil.prenom_profil != null) {
                        checkJWT(response);
                        sys.SetUserProfil(profil);
                        sys.activitys.loginActivity.Success(true);
                    } else {
                        sys.activitys.loginActivity.Success(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    sys.activitys.loginActivity.Success(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.activitys.loginActivity.Error("Problème de connexion avec le serveur");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mail", login);
                params.put("password", password);
                params.put("token", FirebaseInstanceId.getInstance().getToken());
                return params;
            }

        };

        requestQueueSingleton.addToRequestQueue(jsonArrayRequest);
    }


    //////////////////////
    /* FONCTIONS PROFIL
     /*******************/

    /**
     * Permet de récupérer le profil connecté
     * Récupère le profil déjà connecté dans SQlite, l'envoie au WebService et appel la fonction ProfilSync() de MainActivity en cas de succés
     * et appel ProfilNonSync() de MainActivity en cas d'erreur
     * Stoke aussi le profil récupérer en cas de succés dans SQlite avec la fonction SetUserProfil()
     */
    public void GetUserProfil() {
        final CSystem sys = actualActivity.sys;
        final String url = baseURL + "action=Afficher";

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("profil");
                    Profil profil = gt.getProfilFromJson(jsonArray.getString(0));
                    if (profil.nom_profil != null && profil.prenom_profil != null) {
                        checkJWT(response);
                        sys.SetUserProfil(profil);
                        sys.userProfil = profil;
                        sys.activitys.mainActivity.ProfilSync();
                    } else {
                        sys.GetUserProfilLocal();
                        sys.activitys.mainActivity.ProfilNonSync();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    sys.GetUserProfilLocal();
                    sys.activitys.mainActivity.ProfilNonSync();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.GetUserProfilLocal();
                sys.activitys.mainActivity.ProfilNonSync();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonArrayRequest);
    }

    /**
     * Permet de modifier le profil utilisateur
     * Envoie les données au WebService et appel la fonction SuccessModifierProfil() de ModifierProfilActivity en cas de succés
     * et appel ErreurModifierProfil() de ModifierProfilActivity en cas d'erreur
     *
     * @param prfl le profil utilisateur
     */
    public void modifierProfil(final Profil prfl) {
        final String url = baseURL + "action=UpdateProfil";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (1, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Resultat result = gt.getResultatFromJson(response);
                        if (result.getRes()) {
                            try {
                                checkJWT(response);
                                sys.activitys.modifierProfilActivity.SuccessModifierProfil(prfl);
                            } catch (Exception e) {
                                e.printStackTrace();
                                sys.activitys.modifierProfilActivity.ErreurModifierProfil("N'a pas pu modifier les informations");
                            }
                        } else {
                            sys.activitys.modifierProfilActivity.ErreurModifierProfil("N'a pas pu modifier les informations");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.modifierProfilActivity.ErreurModifierProfil("Problème de connexion avec le serveur");
                    }

                }
                ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nom", prfl.getNom_profil());
                params.put("prenom", prfl.getPrenom_profil());
                params.put("fonction", prfl.getFonction_profil());
                params.put("site", "" + prfl.getId_site());
                params.put("jwt", JWTDataSource.getJWT());
                params.put("mail", "" + prfl.getMail_profil());
                params.put("oldPassword", "" + prfl.getOldPassword());
                params.put("newPassword", "" + prfl.getNewPassword());
                params.put("reponse", "" + prfl.getReponse_profil());
                if (prfl.getPhoto_profil() != null) params.put("photo", prfl.getPhoto_profil());

                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de supprimer toutes les données d'un utilisateur
     * Envoie la demande de suppression du profil utilisateur au Webservice et appel la fonction succesSupProfil() de ProfilActivity en cas de succés
     * et appel erreurSupProfil() de ProfilActivity en cas d'erreur
     */
    public void supprimerProfil() {
        final String url = baseURL + "action=suppProfil";
        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                actualActivity.sys.activitys.profilActivity.succesSupProfil();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                actualActivity.sys.activitys.profilActivity.erreurSupProfil();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet d'obtenir la liste de réunions où un profil a participé ou qu'il a organisé
     * Envoie les données au Webservice, nous retourne une List de Reunion et appel la fonction SuccessYourReunions() de ActivityFonction en cas de succés
     * et appel ErreurYourReunions() de ActivityFonction en cas d'erreur
     * Insert la réunion dans SQlite en cas de succés
     */
    public void getYourReunions() {

        final String url = baseURL + "action=AfficherEventProfil";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                reunions = new ArrayList<>();
                sys.reunionDataSource.DeleteAllReunion();
                try {
                    checkJWT(response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray eventProfil = jsonObject.getJSONArray("eventProfil");
                    for (int i = 0; i < eventProfil.length(); i++) {
                        Reunion reu = gt.getReunionFromJson(eventProfil.getJSONObject(i).toString());
                        sys.reunionDataSource.deleteReunion(reu);
                        sys.reunionDataSource.insertReunion(reu);
                        reunions.add(reu);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                actualActivity.SuccessYourReunions(reunions);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                actualActivity.ErreurYourReunions("Hors ligne");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonArrayRequest);
    }

    /**
     * Permet de récuperer tout les profils
     * demande de récupérer tout les profils au WebService et appel la fonction SuccessGetAllProfil() de ActivityFonction en cas de succés
     * et appel ErreurGetAllProfil() de ActivityFonction en cas d'erreur
     * Stoke aussi les profils dans SQlite en cas de succés
     */
    public void getAllProfils() {
        final String url = baseURL + "action=AfficherAll";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<Profil> allProfils = new ArrayList<>();
                try {
                    checkJWT(response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("profils");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Profil profil = gt.getProfilFromJson(jsonArray.getString(i));
                        allProfils.add(profil);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                actualActivity.SuccessGetAllProfil(allProfils);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                actualActivity.ErreurGetAllProfil("hors ligne");
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonArrayRequest);
    }

    ////////////////////////
    /* FONCTIONS RÉUNION
     /*******************/

    /**
     * Va chercher la liste complete des réunions et retourne une list de type Reunion
     * Envoie les données au WebService, reçoit la liste des réunions et appel la fonction successAllReunion() de ActivityFonction en cas de succés
     * et appel errorAllReunion() de ActivityFonction ainsi récupère les réunions dans SQlite et appel quand même succesAllReunion() en cas d'erreur
     *
     * @param profil le profil utilisateur
     */
    public void GetListReunionAll(Profil profil) {
        final String url = baseURL + "action=listEvent";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                Accueil accueil = gt.getAccueilFromJson(response);
                reunions = new ArrayList<>(Arrays.asList(accueil.getEvent()));
                sys.activitys.accueilActivity.successAllReunion(reunions, accueil.isHasNotif());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.activitys.accueilActivity.successAllReunion(sys.reunionDataSource.getAllReunions(), false);
                sys.activitys.accueilActivity.errorAllReunion("Hors ligne");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };


        requestQueueSingleton.addToRequestQueue(jsonRequest);

    }

    /**
     * Permet d'ajouter une réunions
     * Envoie les données au WebService et appel la fonction SuccessAddReunion() de CreerReunionActivity en cas de succés
     * et appel ErreurAddReunion() de CreerReunionActivity en cas d'erreur
     *
     * @param reunion   la réunion que l'on veut ajouter
     * @param idInviter est un string format json qui contien tout les id des invités, fais à partir d'un String[] , exemple : String[] {"1", "2", "4"}
     */
    public void AddReunion(final Reunion reunion, final String idInviter) {
        final String url = baseURL + "action=CreateEvent";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Resultat result = gt.getResultatFromJson(response);
                        checkJWT(response);
                        if (result.getRes()) {
                            reunion.setId(Integer.parseInt(result.getMsg()));
                            reunionDataSource.deleteReunion(reunion);
                            reunionDataSource.insertReunion(reunion);
                            // Ici les fonctions d'affichage
                            sys.activitys.creerReunionActivity.SuccessAddReunion(true, Integer.parseInt(result.getMsg()));
                        } else {
                            sys.activitys.creerReunionActivity.SuccessAddReunion(false, -1);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.creerReunionActivity.ErreurAddReunion("Probleme de connexion avec le serveur");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("objet", reunion.getObjet_reunion());
                params.put("heure", reunion.getHeure_reunion());
                params.put("date", reunion.getDate_reunion());
                params.put("adresse", reunion.getAdresse_reunion());
                params.put("lieu", reunion.getLieu_reunion());
                params.put("nbPlace", String.valueOf(reunion.getNbPlace_reunion()));
                params.put("duree", reunion.getDuree_reunion());
                params.put("listInviter", idInviter);
                params.put("id", String.valueOf(reunion.getId_profil()));
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                params.put("typeReunion", reunion.getType_reunion());
                if (reunion.getPhoto_reunion() != null)
                    params.put("photo", reunion.getPhoto_reunion());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de modifier une réunion
     * Envoie les données au WebService et appel la fonction SuccessModifReunion() de ActivityFonction en cas de succés
     * et appel ErreurModifReunion() de ActivityFonction en cas d'erreur
     * Mofdifie aussi la réunion dans SQlite en cas de succés
     *
     * @param reunion est la nouvelle réunion modifier
     */
    public void ModifierReunion(final Reunion reunion) {

        final CSystem sys = actualActivity.sys;
        final String url = baseURL + "action=UpdateEvent";

        StringRequest stringRequest = new StringRequest
                (1, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        Resultat result = gt.getResultatFromJson(response);
                        if (result.getRes()) {
                            sys.reunionDataSource.deleteReunion(reunion);
                            sys.reunionDataSource.insertReunion(reunion);
                            actualActivity.SuccessModifReunion(true);
                        } else {
                            actualActivity.SuccessModifReunion(false);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        actualActivity.ErreurModifReunion("Problème avec le serveur");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("objet", reunion.getObjet_reunion().replace("'", "''"));
                params.put("heure", reunion.getHeure_reunion());
                params.put("date", reunion.getDate_reunion());
                params.put("adresse", reunion.getAdresse_reunion().replace("'", "''"));
                params.put("lieu", reunion.getLieu_reunion().replace("'", "''"));
                params.put("nbPlace", String.valueOf(reunion.getNbPlace_reunion()));
                params.put("duree", reunion.getDuree_reunion());
                params.put("typeReunion", reunion.getType_reunion());
                params.put("id", String.valueOf(reunion.getId()));
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                if (reunion.getPhoto_reunion() != null) {
                    params.put("photo", reunion.getPhoto_reunion());
                }
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(stringRequest);

    }

    /**
     * Permet de récuperer une réunion spécific
     * Envoie les données au WebService, récupère les détails d'une réunion et appel la fonction SuccessDetailReunion() de ActivityFonction en cas de succés
     * et appel ErreurDetailReunion() de ActivityFonction en cas d'erreur
     * Stocke aussi dans SQlite les detail de la réunion récupérer avec la fonction AddLocalDetailReunion() si l'utilisateur est particiapant, organisateur ou invités
     *
     * @param id est l'id de la réunion que l'on veut
     */
    public void getReunion(final int id) {

        final String url = baseURL + "action=detailEvent" + "&id=" + id;
        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response);
                    checkJWT(response);
                    final DetailReunion detail = gt.getDetailsFromJson(jsonObject.getString("reunion"));

                    if (detail.getReunion().length > 0) {
                        getStatusProfil(detail.getReunion()[0].id_reunion, new onEventListener() {
                            @Override
                            public void onSuccess(Object notation) {
                                if (status == 3 || status == 2 || status == 1) {
                                     sys.AddLocalDetailReunion(detail);
                                }
                                actualActivity.SuccessDetailReunion(detail, true);
                            }
                            @Override
                            public void onFailure(Exception e) {
                                e.printStackTrace();
                            }
                        });
                    } else {
                        utils.toaster(actualActivity.getApplicationContext(), "Cette réunion n'existe pas ou plus", 0);
                        actualActivity.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                DetailReunion detail = sys.GetLocalDetailReunion(id);
                if (detail != null) {
                    actualActivity.SuccessDetailReunion(detail, false);
                    actualActivity.ErreurDetailReunion("Hors ligne");
                } else {
                    actualActivity.ErreurDetailReunion("Impossible de récupérer la réunion en hors ligne");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de supprimer une réunion spécific
     * Envoie les données au WebService et appel la fonction succesSupprimerReunion() de reunionDetailsActivity en cas de succés
     * et appel erreurSupprimerReunion() de reunionDetailsActivity en cas d'erreur
     * Supprimer aussi la réunion de Sqlite en cas de succés
     *
     * @param id      est l'id de la réunion que l'on veut supprimer
     * @param message est le message de l'organisateur qaund il supprime la réunion
     */
    public void supprimerReunion(final int id, final String message) {
        final String url = baseURL + "action=suppEvent";
        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                actualActivity.sys.activitys.reunionDetailsActivity.succesSupprimerReunion();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                actualActivity.sys.activitys.reunionDetailsActivity.erreurSupprimerReunion();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", String.valueOf(id));
                params.put("message", message);
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de quitter une réunion
     * Envoie les données au WebService et appel la fonction succesQuiterReunion() de reunionDetailsActivity en cas de succés
     * et appel erreurQuiterReunion() de reunionDetailsActivity en cas d'erreur
     *
     * @param idReunion est l'id de la rénion que l'on quitte
     */
    public void quitterReunion(final int idReunion) {
        final String url = baseURL + "action=partir";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                Resultat res = gt.getResultatFromJson(response);
                actualActivity.sys.activitys.reunionDetailsActivity.succesQuiterReunion(res.getRes());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                actualActivity.sys.activitys.reunionDetailsActivity.erreurQuiterReunion();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idReunion", String.valueOf(idReunion));
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }


    ////////////////////////////////////
    /* FONCTIONS COMMENTAIRES ET NOTES
     /*********************************/

    /**
     * Permet d'ajouter un commentaire à une réunion spécific
     * Envoie les données au WebService et appel la fonction successCommenter() de reunionDetailsActivity en cas de succés
     * et appel erreurCommenter() de reunionDetailsActivity en cas d'erreur
     *
     * @param comment est le commentaire que l'on veut envoyer
     * @param notif   permet de savoir si c'est un commentaire d'une personne qui quitte la réunion ou non : true si c'est un commentaire et false si c'est quitter la réunion
     */
    public void addCommentaire(final Commentaire comment, final boolean notif) {

        final String url = baseURL + "action=commenter";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (1, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        Resultat result = gt.getResultatFromJson(response);
                        sys.activitys.reunionDetailsActivity.successCommenter(result.getRes(), comment);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.reunionDetailsActivity.erreurCommenter();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", String.valueOf(comment.id_reunion));
                params.put("contenu_commentaire", comment.getContenu_commentaire());
                params.put("date", String.valueOf(comment.getDate()));
                params.put("heure", String.valueOf(comment.getHeure()));
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                if (notif)
                    params.put("notif", "true");
                else
                    params.put("notif", "false");
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet d'ajouter une note à une réunion
     * Envoie les données au WebService et appel la fonction successNoter() de reunionDetailsActivity en cas de succés
     * et appel erreurNoter() de reunionDetailsActivity en cas d'erreur
     *
     * @param note   est note que vous attribué à la réunion
     * @param detail est le Détail de réunion que vous noter
     */
    public void addNote(final Note note, final DetailReunion detail) {
        final String url = baseURL + "action=noter";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (1, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        Resultat result = gt.getResultatFromJson(response);
                        sys.activitys.reunionDetailsActivity.successNoter(result.getRes(), note, detail);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.reunionDetailsActivity.erreurNoter();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("note", String.valueOf(note.valeur_note));
                params.put("idR", String.valueOf(note.id_reunion));
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de mettre a jour une note
     * Envoie les données au WebService et appel la fonction successUpdateNoter() de reunionDetailsActivity en cas de succés
     * et appel erreurNoter() de reunionDetailsActivity en cas d'erreur
     *
     * @param note   est la note modifier
     * @param detail est le détail de la réunion ou l'on modifie la note
     */
    public void updateNote(final Note note, final DetailReunion detail) {
        final String url = baseURL + "action=updateNote";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (1, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        Resultat result = gt.getResultatFromJson(response);
                        sys.activitys.reunionDetailsActivity.successUpdateNoter(result.getRes(), note, detail);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.reunionDetailsActivity.erreurNoter();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("note", String.valueOf(note.valeur_note));
                params.put("idN", String.valueOf(note.id_note));
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }


    /**
     * Permet de faire un appel a l'api pour obtenir la note d'un utilisateur
     *
     * @param callback
     * @return Note de l'utilisateur
     */
    public String getNote(final onEventListener callback) {

        final String url = baseURL + "action=noteUser";

        final StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            note = String.valueOf(jsonObject.getDouble("AVG"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Attend la réponse et répond au callback
                        callback.onSuccess(note);

                    }
                    // gestion de l'erreur d'obtention de réponse
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            //Ajout des paramètres de la méthode post pour récupérer la note
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        // on ajout de requeste a dans la fille d'attente des requêtes
        requestQueueSingleton.addToRequestQueue(jsonRequest);
        return note;
    }


    /////////////////////////////////////////
    /* FONCTIONS INVITATION/PARTICIPATION
     /*************************************/

    /**
     * permet d'ajouter un participant à une réunion
     * Envoie les données au WebService et appel la fonction SuccessRejoindre() de reunionDetailsActivity en cas de succés
     * et appel ErreurRejoindre() de reunionDetailsActivity en cas d'erreur
     *
     * @param idReunion est l'id de la réunion que l'on veut ajouter un participant au format String
     */
    public void addParticipant(final String idReunion) {
        final String url = baseURL + "action=rejoindre";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Resultat result = gt.getResultatFromJson(response);
                        checkJWT(response);
                        if (result.getRes()) {
                            sys.activitys.reunionDetailsActivity.SuccessRejoindre();
                        } else {
                            sys.activitys.reunionDetailsActivity.ErreurRejoindre("Impossible de rejoindre");
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        sys.activitys.reunionDetailsActivity.ErreurRejoindre("Probleme de connexion avec le serveur");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", idReunion);
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet d'inviter un participant
     * Envoie les données au WebService et appel la fonction successInviter() de ActivityFonction en cas de succés
     * et appel erreurInviter() de ActivityFonction en cas d'erreur
     *
     * @param idInviter est un string format json qui contien tout les id des invités, fais à partir d'un String[] , exemple : String[] {"1", "2", "4"}
     */
    public void inviter(final String idInviter) {

        final String url = baseURL + "action=inviter";

        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        checkJWT(response);
                        Resultat result = gt.getResultatFromJson(response);
                        actualActivity.successInviter(result.getRes());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(actualActivity, error, url);
                        actualActivity.erreurInviter("Probleme avec le serveur");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", "" + sys.reunionSelected.getId());
                params.put("objet", sys.reunionSelected.getObjet_reunion());
                params.put("listInviter", idInviter);
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet d'accepter une invitation
     * Envoie les données au WebService et appel la fonction SuccessAccepterInvitation() de ReunionDetailsActivity en cas de succés
     * et appel ErreurInvitationsAR() de ReunionDetailsActivity en cas d'erreur
     *
     * @param idReunion est l'id de la réunion au quel on est invité
     */
    public void accepterInvitation(final int idReunion) {

        final String url = baseURL + "action=accepter";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                Resultat res = gt.getResultatFromJson(response);
                sys.activitys.reunionDetailsActivity.SuccessAccepterInvitation(res.getRes());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.activitys.reunionDetailsActivity.ErreurInvitationsAR("Probleme de connexion avec le serveur");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", "" + idReunion);
                params.put("currentDate", CSystem.getDateTime());
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }
    //todo annonce
   /*public void Annonce(final int idReunion , final ActivityFonction activityFonction, final String date) {

        final String url =baseURL+"action=awareNotif";

        final CSystem sys = activityFonction.sys;

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Resultat result = gt.getResultatFromJson(response);


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        activityFonction.ErreurAnnonce("pas de réponse");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", "" + idReunion);
                params.put("currentDate","" + date);
                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }
*/


    /**
     * Permet de refuser une invitation
     * Envoie les données au WebService et appel la fonction SuccessRefuserInvitation() de ReunionDetailsActivity en cas de succés
     * et appel ErreurInvitationsAR() de ReunionDetailsActivity en cas d'erreur
     *
     * @param idReunion est l'id de la réunion qu'on a refuser l'invitation
     */
    public void refuserInvitation(final int idReunion) {

        final String url = baseURL + "action=refuser";
        final CSystem sys = actualActivity.sys;

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                Resultat res = gt.getResultatFromJson(response);
                sys.activitys.reunionDetailsActivity.SuccessRefuserInvitation(res.getRes());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                sys.activitys.reunionDetailsActivity.ErreurInvitationsAR("Problème de connexion avec le serveur");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", "" + idReunion);
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de récuperer toute les notifications de l'utilisateur
     * Envoie les données au WebService et appel la fonction successNotifications() de ActivityFonction en cas de succés
     * et appel ErreurNotifications() de ActivityFonction en cas d'erreur
     */
    public void getNotifications() {
        final String url = baseURL + "action=AfficheNotification";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    checkJWT(response);
                    JSONArray jsonArray = new JSONArray(jsonObject.getString("notif"));
                    List<Notification> notifications = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Notification notification = gt.getNotificationFromJson(jsonArray.getString(i));
                        notifications.add(notification);
                    }
                    actualActivity.successNotifications(notifications);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                actualActivity.ErreurNotifications("Hors ligne");
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet d'envoyer la notification Check
     * Envoie les données au WebService et appel la fonction successCheckNotifications() de NotificationsActivity en cas de succés
     *
     * @param idNotif est l'id de notification check
     */
    public void setNotifChecked(final int idNotif, final onEventListener callback) {
        final String url = baseURL + "action=checkNotification";
        final CSystem sys = actualActivity.sys;


        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    checkJWT(response);
                    if(jsonObject.getString("res").equalsIgnoreCase("True"))
                        callback.onSuccess(jsonObject.getString("res"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                params.put("idN", "" + idNotif);
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de supprimer toute les notifications check de l'utilisateur
     * Envoie les données au WebService et appel la fonction successNotifications() de ActivityFonction en cas de succés
     */
    public void suppCheckedNotification() {
        final String url = baseURL + "action=suppCheckedNotification";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String result = jsonObject.getString("res");
                    checkJWT(response);

                    if(result.equalsIgnoreCase("True")){
                        actualActivity.successSuppNotif(result);
                    }else {
                        actualActivity.erreurSuppNotif(result);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * supprimer toute les notifications de l'utilisateur
     * Envoie les données au WebService et appel la fonction successNotifications() de ActivityFonction en cas de succés
     */
    public void suppAllNotif() {
        final String url = baseURL + "action=suppAllNotification";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                checkJWT(response);
                List<Notification> notifications = new ArrayList<>();
                actualActivity.successNotifications(notifications);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Supprime une notification en particulier quand celui-ci la swipe hors de l'écran
     * Envoie les données au WebService et appelle la fonction successNotifications() de ActivityFonction en cas de succés
     *
     * @param idN ID de la notification concernée
     */
    public void suppNotif(final int idN) {
        final String url = baseURL + "action=suppNotification";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    checkJWT(response);
                    String result = jsonObject.getString("res");
                    checkJWT(response);

                    if(result.equalsIgnoreCase("True")){
                        actualActivity.successSuppNotif(result);
                    }else{
                        actualActivity.erreurSuppNotif(result);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idN", "" + idN);
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de révupérer la list complete des sites
     * Envoie les données au WebService, Récupére la list des sites et appel la fonction successGetAllSite() de ActivityFonction en cas de succés
     */
    public void getAllSite() {
        final String url = baseURL + "action=allSite";

        StringRequest jsonRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    List<Site> sites = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        sites.add(gt.getSiteFromJson(jsonArray.getString(i)));
                    }
                    actualActivity.successGetAllSite(sites);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
                utils.toaster(actualActivity, "Impossible de récupérer les sites", 0);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet Appeler l'API Foursquare
     * Envoie les données au WebService, Récupére les data de l'api et appel la fonction successAppelApi() de ActivityFonction en cas de succés
     *
     * @param query est la recheche que l'on veut
     */
    public void ApiGeoloc(final String query) {
        final CSystem sys = actualActivity.sys;
        final String url = "https://api.foursquare.com/v2/venues/search?" +
                "client_id=2VK25YYLILAOUFKR4BL3FWOPSXLYQH3HWYQNI30PS1TROP0Z" +
                "&client_secret=M4VOG30U5JSGBOGHQNDR0YCLLXMBLIZMJB3NVNCKU2Z53QRM" +
                "&ll=" + sys.userProfil.getGps_site() +
                "&query=" + query +
                "&radius=5000" +
                "&limit=50" +
                "&intent=browse" +
                "&categoryId=4d4b7105d754a06374d81259" +
                "&v=20190614";

        StringRequest stringRequest = new StringRequest(1, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                DataApi dataApi = gt.getDataApiFromJson(response);
                List<DataAdresse> adresses = Arrays.asList(dataApi.getResponse().getVenues());
                actualActivity.successAppelApi(adresses);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        });

        requestQueueSingleton.addToRequestQueue(stringRequest);
    }

    /**
     * Permet de savoir si le profil utilisateur est organisateur, particapant ou invités
     *
     * @param idR ID de la réunion
     * @return return un int : 0 s'il est rien du tout, 1 s'il est participant, 2 s'il est inviter, 3 s'il est organisateur
     */
    public void getStatusProfil(final int idR, final onEventListener callback) {
        final String url = baseURL + "action=getStatusProfil";
        final CSystem sys = actualActivity.sys;
        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response);
                    checkJWT(response);
                    sys.status = jsonObject.getInt("status");
                    callback.onSuccess(jsonObject.getInt("status"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AfficheErreur(actualActivity, error, url);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idR", "" + idR);
                params.put("jwt", JWTDataSource.getJWT());
                return params;
            }
        };
        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }


    /**
     * Permet d'afficher les photos du webServiuce avec gestion du cache
     *
     * @param nomImage  est le nom de l'image.jpg
     * @param imageView est imageView sur le quel on veut afficher
     * @param type      si c'est une image de réunion ou de profil, true : réunion , false : profil
     */
    public static void AfficherPhoto(final String nomImage, final ImageView imageView, final boolean type) {
        String path = URL_SITE;

        if (type) { // C'est une photo d'événement
            path += "/img_Events/";
        } else {
            path += "/img_Profil/";
        }

        Picasso.get().setIndicatorsEnabled(modeDebug); // Permet de voir visuellement si la photo est en cache ou en direct

        Picasso.get()
                .load(path + nomImage)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                .centerCrop()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        if (!type) { // si c'est une photo de profil
                            imageView.setImageDrawable(imageView.getContext().getDrawable(R.drawable.defaultuser));
                        }
                    }
                });
    }

    /**
     * Permet de réinitialiser le mot de passe
     * Envoie les données au WebService et appel la fonction Success() de LoginActivity en cas de succés
     * et appel Error() de LoginActivity en cas d'erreur
     *
     * @param mail             le mail de l'utilisateur (son login)
     * @param reponse          la reponse à la question secrete
     * @param newPassword      le nouveau mot de passe voulu
     * @param activityFonction L'activité actuelle qui hérite de FonctionActivity
     */
    public void ResetPassword(final String mail, final String reponse, final String newPassword, final ActivityFonction activityFonction) {
        final String url = baseURL + "action=ResetPassword";

        final CSystem sys = activityFonction.sys;

        StringRequest jsonRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String res = jsonObject.getString("res");
                            System.out.println(res);
                            if (res.equalsIgnoreCase("True")) {
                                sys.activitys.resetPasswordActivity.Success(true);
                            } else {
                                sys.activitys.resetPasswordActivity.Error("N'a pas pu modifier les informations");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AfficheErreur(activityFonction, error, url);
                        sys.activitys.resetPasswordActivity.Error("Problème de connexion avec le serveur");
                    }

                }
                ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mail", "" + mail);
                params.put("newPassword", "" + newPassword);
                params.put("reponse", "" + reponse);

                return params;
            }
        };

        requestQueueSingleton.addToRequestQueue(jsonRequest);
    }

    /**
     * Permet de récupérer la date courente en dd-MM-yyyy HH:mm:ss
     *
     * @return return un String avec la date courente au format dd-MM-yyyy HH:mm:ss
     */
    private static String getDateTime() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.forLanguageTag("fr"));
        return fmt.format(new Date());

    }

    /**
     * Gestion d'erreur Volley
     * Est utilisable si le modedebug est activé dans CSystem
     *
     * @param error la VolleyErreur
     * @return return un message d'erreur plus spécific à l'erreur
     */
    private String erreurVolley(VolleyError error) {
        if (modeDebug) {
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                //This indicates that the reuest has either time out or there is no connection
                return "Erreur de connexion vérifier le wifi ou la connection avec le serveur : \n" + error.getMessage();
            } else if (error instanceof AuthFailureError) {
                //Error indicating that there was an Authentication Failure while performing the request
                return "échec d'authentification lors de l'exécution de la requête. : \n" + error.getMessage();
            } else if (error instanceof ServerError) {
                //Indicates that the server responded with a error response
                return "Erreur avec la réponse du serveur : \n" + error.getMessage();
            } else if (error instanceof NetworkError) {
                //Indicates that there was network error while performing the request
                return "Erreur avec le réseaux lors de la requête : \n" + error.getMessage();
            } else if (error instanceof ParseError) {
                // Indicates that the server response could not be parsed
                return "La réponse du serveur ne pas être parse : \n" + error.getMessage();
            }
        }

        return "Problème avec le serveur";
    }

    /**
     * Affiche les erreur si le mode debug est activé
     *
     * @param context L'activité actuelle
     * @param error   le VolleyError
     * @param query   La requete d'envoie d'avant l'erreur
     */
    private void AfficheErreur(Context context, VolleyError error, String query) {
        String erreurMessage = erreurVolley(error);
        if (modeDebug) {
            View view = LayoutInflater.from(context).inflate(R.layout.erreur_modal, null);
            TextView textView = view.findViewById(R.id.tv);
            String errorMessage = "requete : " + query + "\n\n" + erreurMessage;
            textView.setText(errorMessage);
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setView(view)
                    .setNeutralButton("OK", null)
                    .show();
        }
    }

    /**
     * Permet de vider les données de Sqlite sauf le profil utilisateur
     */
    public void viderCache() {
        Profil profil = userProfil;
        reunionDataSource.DropAllTables();
        SetUserProfil(profil);
    }

    /**
     * Check la validité du Token JWT
     *
     * @param response
     */
    private void checkJWT(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("jwt") && !jsonObject.getString("jwt").isEmpty()) {
                JWTDataSource.insertJWT(jsonObject.getString("jwt"));
            } else {
                actualActivity.sys.reunionDataSource.DropAllTables();
                actualActivity.ChangeActivity(actualActivity, LoginActivity.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            actualActivity.sys.reunionDataSource.DropAllTables();
            actualActivity.ChangeActivity(actualActivity, LoginActivity.class);
        }
    }

} // Fin de la classe
