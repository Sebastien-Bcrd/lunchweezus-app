package team.iut.lunchweezus.Logique;

import com.google.gson.Gson;

import team.iut.lunchweezus.Beans.Accueil;
import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Beans.DetailReunion;
import team.iut.lunchweezus.Beans.Geoloc.DataApi;
import team.iut.lunchweezus.Beans.Note;
import team.iut.lunchweezus.Beans.Notification;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Resultat;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Beans.Site;


/**
 * Classe pour obtenir des beans issues des réponses du web service.
 */
public class GsonBuilder {

    com.google.gson.GsonBuilder builder = new com.google.gson.GsonBuilder();

    /**
     * Convertie un JsonString en Profil
     * @param jsonString le JsonString
     * @return un Objet Profil
     */
    public Profil getProfilFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Profil profil = gson.fromJson(jsonString, Profil.class);
        return profil;
    }

    /**
     * Convertie un JsonString en DataApi
     * @param jsonString le JsonString
     * @return un objet DataApi
     */
    public DataApi getDataApiFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        DataApi dataApi = gson.fromJson(jsonString, DataApi.class);
        return dataApi;
    }

    /**
     * Convertie un JsonString en Notification
     * @param jsonString le JsonString
     * @return un objet Notification
     */
    public Notification getNotificationFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Notification notification = gson.fromJson(jsonString, Notification.class);
        return notification;
    }

    /**
     * Convertie un JsonString en Accueil
     * @param jsonString le JsonString
     * @return un objet Accueil
     */
    public Accueil getAccueilFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Accueil accueil = gson.fromJson(jsonString, Accueil.class);
        return accueil;
    }

    /**
     * Convertie un JsonString en Reunion
     * @param jsonString le JsonString
     * @return un objet Reunion
     */
    public Reunion getReunionFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Reunion reunion = gson.fromJson(jsonString, Reunion.class);
        return reunion;
    }

    /**
     * Convertie un JsonString en Resultat
     * @param jsonString le JsonString
     * @return un objet Resultat
     */
    public Resultat getResultatFromJson(String jsonString) {
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Resultat result = gson.fromJson(jsonString, Resultat.class);
        return result;
    }

    public Note getNoteAvgFromJson(String jsonString) {
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Note note  = gson.fromJson(jsonString, Note.class);
        return note;
    }

    /**
     * Convertie un JsonString en Commentaire
     * @param jsonString le JsonString
     * @return un objet Commentaire
     */
    public Commentaire getCommentaireFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Commentaire comm = gson.fromJson(jsonString, Commentaire.class);
        return comm;
    }

    /**
     * Convertie un JsonString en DetailReunion
     * @param jsonString le JsonString
     * @return un objet DetailReunion
     */
    public DetailReunion getDetailsFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        DetailReunion details = gson.fromJson(jsonString, DetailReunion.class);
        return details;
    }

    /**
     * Convertie un JsonString en Site
     * @param jsonString le JsonString
     * @return un objet Site
     */
    public Site getSiteFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Site site = gson.fromJson(jsonString, Site.class);
        return site;
    }

    /**
     * Convertie un JsonString en Note
     * @param jsonString le JsonString
     * @return un objet Note
     */
    public Note getNoteFromJson(String jsonString) {

        builder.setPrettyPrinting();
        Gson gson = builder.create();
        Note note = gson.fromJson(jsonString, Note.class);
        return note;
    }

}
