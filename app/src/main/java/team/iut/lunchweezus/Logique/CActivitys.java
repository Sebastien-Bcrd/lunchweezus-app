package team.iut.lunchweezus.Logique;

import android.support.v7.app.AppCompatActivity;

import java.io.Serializable;

import team.iut.lunchweezus.AccueilActivity;
import team.iut.lunchweezus.CreerReunionActivity;
import team.iut.lunchweezus.InscriptionActivity;
import team.iut.lunchweezus.InviterListeActivity;
import team.iut.lunchweezus.InviterSurReuActivity;
import team.iut.lunchweezus.LoginActivity;
import team.iut.lunchweezus.MainActivity;
import team.iut.lunchweezus.MesReunionsActivity;
import team.iut.lunchweezus.ModifierProfilActivity;
import team.iut.lunchweezus.ModifierReunionActivity;
import team.iut.lunchweezus.NotificationsActivity;
import team.iut.lunchweezus.ParametresActivity;
import team.iut.lunchweezus.ProfilActivity;
import team.iut.lunchweezus.ResetPasswordActivity;
import team.iut.lunchweezus.ReunionDetailsActivity;
import team.iut.lunchweezus.UserListReunionsActivity;

/**
 * Cette Classe contiendra toute les activités de l'application en attribu
 * les activités devront hérité de ActivityFonction
 * Elle permettra de faire connaitre a CSystem l'activité actuel
 * Si vous rajoutez des activités il faut l'ajouter en attribu de cette classe et compléter la fonction Initialise comme ci-dessous dans le code
 */
public class CActivitys implements Serializable {

    public MainActivity mainActivity;
    public AccueilActivity accueilActivity;
    public CreerReunionActivity creerReunionActivity;
    public InscriptionActivity inscriptionActivity;
    public LoginActivity loginActivity;
    public MesReunionsActivity mesReunionsActivity;
    public ModifierProfilActivity modifierProfilActivity;
    public NotificationsActivity notificationsActivity;
    public ProfilActivity profilActivity;
    public ReunionDetailsActivity reunionDetailsActivity;
    public InviterListeActivity inviterListeActivity;
    public UserListReunionsActivity userListReunionsActivity;
    public ModifierReunionActivity modifierReunionActivity;
    public InviterSurReuActivity inviterSurReuActivity;
    public ResetPasswordActivity resetPasswordActivity;
    public ParametresActivity parametresActivity;

    ///

    /**
     * Permet d'initialiser les attributs ( l'activités actuel )
     * Si vous rajouter des Activités n'oublier de l'ajouter a la suite dans cette fonction
     * @param actualActivity est L'activités Actuel qui hérite de ActivityFonction
     */
    public void Initialise(AppCompatActivity actualActivity) {
        if (actualActivity instanceof MainActivity) {
            mainActivity = (MainActivity) actualActivity;
        }
        if (actualActivity instanceof AccueilActivity) {
            accueilActivity = (AccueilActivity) actualActivity;
        }
        if (actualActivity instanceof CreerReunionActivity) {
            creerReunionActivity = (CreerReunionActivity) actualActivity;
        }
        if (actualActivity instanceof InscriptionActivity) {
            inscriptionActivity = (InscriptionActivity) actualActivity;
        }
        if (actualActivity instanceof LoginActivity) {
            loginActivity = (LoginActivity) actualActivity;
        }
        if (actualActivity instanceof MesReunionsActivity) {
            mesReunionsActivity = (MesReunionsActivity) actualActivity;
        }
        if (actualActivity instanceof ModifierProfilActivity) {
            modifierProfilActivity = (ModifierProfilActivity) actualActivity;
        }
        if (actualActivity instanceof NotificationsActivity) {
            notificationsActivity = (NotificationsActivity) actualActivity;
        }
        if (actualActivity instanceof ProfilActivity) {
            profilActivity = (ProfilActivity) actualActivity;
        }
        if (actualActivity instanceof ReunionDetailsActivity) {
            reunionDetailsActivity = (ReunionDetailsActivity) actualActivity;
        }
//        if(actualActivity instanceof InviterListeActivity)
//        {
//            inviterListeActivity = (InviterListeActivity) actualActivity;
//        }
        if (actualActivity instanceof UserListReunionsActivity) {
            userListReunionsActivity = (UserListReunionsActivity) actualActivity;
        }
        if (actualActivity instanceof ModifierReunionActivity) {
            modifierReunionActivity = (ModifierReunionActivity) actualActivity;
        }
        if (actualActivity instanceof InviterSurReuActivity) {
            inviterSurReuActivity = (InviterSurReuActivity) actualActivity;
        }
        if (actualActivity instanceof ParametresActivity) {
            parametresActivity = (ParametresActivity) actualActivity;
        }
        if (actualActivity instanceof ResetPasswordActivity) {
            resetPasswordActivity = (ResetPasswordActivity) actualActivity;
        }
    }

}
