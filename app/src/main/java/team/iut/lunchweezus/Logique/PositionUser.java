package team.iut.lunchweezus.Logique;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import team.iut.lunchweezus.ActivityFonction;
import team.iut.lunchweezus.ui.utils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Cette classe permet de récuperer la position géographique du téléphone
 */
public class PositionUser {

    private FusedLocationProviderClient client;

    /**
     * Permet d'appeler la position et appel aussi la fonction ApiGeoloc de CSystem
     * range la latitude et longitude récupéré du téléphone dans ActivityFonction
     * @param activityFonction est l'activité actuel qui hérite de ActivityFonction
     * @param query est la requete que l'on cherche dans l'api GeoLoc (FourSquare)
     */
    public void callPosition(final ActivityFonction activityFonction, final String query)
    {
        requestPermission(activityFonction); // Permet de dmander les permssions
        client = LocationServices.getFusedLocationProviderClient(activityFonction);
        // Vérifie si les permissions sont accepter
        if(ActivityCompat.checkSelfPermission(activityFonction, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        client.getLastLocation().addOnSuccessListener(activityFonction, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null) {
                    activityFonction.lat = location.getLatitude();
                    activityFonction.lng = location.getLongitude();
                    activityFonction.sys.ApiGeoloc(query);
                }
                else
                {
                    utils.toaster(activityFonction, "localisation non activé", 0);
                }
            }
        });
    }

    /**
     * Permet de demander les permissions de localisation
     * @param activityFonction est l'activité actuel qui hérite de ActivityFonction
     */
    private void requestPermission(ActivityFonction activityFonction)
    {
        ActivityCompat.requestPermissions(activityFonction, new String[]{ACCESS_FINE_LOCATION}, 1);
    }
}
