package team.iut.lunchweezus.Logique;

public interface onEventListener<String> {

    void onSuccess(String notation);

    void onFailure(Exception e);
}
