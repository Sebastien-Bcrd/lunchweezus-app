package team.iut.lunchweezus.reu_fragments;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ReunionDetailsActivity;
import team.iut.lunchweezus.ui.utils;

public class ReunionDetailsFragment extends Fragment {

    // Constructeur
    public ReunionDetailsFragment() {
    }

    private ImageView photoOrg;
    private TextView objetReunion, nomOrganisateur, adresse, dateHeure;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reunion_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        photoOrg = getActivity().findViewById(R.id.photo_org);
        objetReunion = getActivity().findViewById(R.id.objetReunion);
        nomOrganisateur = getActivity().findViewById(R.id.nomOrganisateur);
        adresse = getActivity().findViewById(R.id.adresse);
        dateHeure = getActivity().findViewById(R.id.DateHeure);
    }

    public void setReunion(final Reunion reunion, final Profil organisateur) {
        if (organisateur.photo_profil != null && !organisateur.photo_profil.equals("")) {
            CSystem.AfficherPhoto(organisateur.photo_profil, photoOrg, false);
        }
        objetReunion.setText(reunion.objet_reunion);
        objetReunion.setSelected(true);
        nomOrganisateur.setText("Organisé par " + organisateur.prenom_profil + " " + organisateur.nom_profil);
        dateHeure.setText("Le " + reunion.date_reunion.replace('-', '/') + " à " + reunion.heure_reunion);
        adresse.setText(reunion.lieu_reunion + "\n" + reunion.adresse_reunion);

        adresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/search/?api=1&query=" + reunion.adresse_reunion));
                startActivity(intent);
            }
        });

        photoOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReunionDetailsActivity reunionDetailsActivity = (ReunionDetailsActivity) getContext();

                ReunionProfilFragment bottomSheet = new ReunionProfilFragment(organisateur, reunionDetailsActivity.sys);
                bottomSheet.show(getFragmentManager(), "Profil");

                reunionDetailsActivity.setReunionProfilFragement(bottomSheet);

                reunionDetailsActivity.sys.getYourReunions();
            }
        });

        nomOrganisateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReunionDetailsActivity reunionDetailsActivity = (ReunionDetailsActivity) getContext();

                ReunionProfilFragment bottomSheet = new ReunionProfilFragment(organisateur, reunionDetailsActivity.sys);
                bottomSheet.show(getFragmentManager(), "Profil");

                reunionDetailsActivity.setReunionProfilFragement(bottomSheet);

                reunionDetailsActivity.sys.getYourReunions();
            }
        });
    }

    public void addParticipantToList(Reunion reunion, Profil organisateur, final List<Profil> participants, final List<Profil> invites, final CSystem sys) {
        LinearLayout participantsListeLayout = getActivity().findViewById(R.id.listParticipants);
        if (participantsListeLayout.getChildCount() != 0) {
            participantsListeLayout.removeAllViewsInLayout();
        }
        ArrayList<LinearLayout> participantsListVar = new ArrayList<>();
        TextView participantsCount = getActivity().findViewById(R.id.participants_count);
        int i;

        int margins = utils.dpCalc(getActivity().getApplicationContext(), 16);

        int imageSize = utils.dpCalc(getActivity().getApplicationContext(), 40);

        participantsListVar.add(createParticipantForList(organisateur, margins, imageSize, true));

        for (i = 0; i < participants.size(); i++) {
            participantsListVar.add(createParticipantForList(participants.get(i), margins, imageSize, true));
        }

        for (i = 0; i < invites.size(); i++) {
            participantsListVar.add(createParticipantForList(invites.get(i), margins, imageSize, false));
        }

        for (i = 0; i < participantsListVar.size(); i++) {
            participantsListeLayout.addView(participantsListVar.get(i));
        }

        String text = i + "/" + reunion.nbPlace_reunion;

        participantsCount.setText(text);
    }

    public LinearLayout createParticipantForList(final Profil profil, int margins, int imageSize, boolean accepted) {
        LinearLayout participantElement = new LinearLayout(getActivity());
        LinearLayout.LayoutParams groupParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        groupParams.setMargins(0, margins, margins, 0);
        participantElement.setLayoutParams(groupParams);
        participantElement.setOrientation(LinearLayout.HORIZONTAL);
        participantElement.setGravity(Gravity.CENTER_VERTICAL);

        CardView profilCrop = new CardView(getActivity());
        LinearLayout.LayoutParams profilCropParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        profilCropParams.setMargins(0, 0, margins, 0);
        profilCrop.setLayoutParams(profilCropParams);
        profilCrop.setRadius(imageSize / 2);
        profilCrop.setCardElevation(0);
        profilCrop.setCardBackgroundColor(getResources().getColor(R.color.transparent));

        ImageView profilePic = new ImageView(getActivity());
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(imageSize, imageSize);
        profilePic.setLayoutParams(imageParams);
        if (profil.photo_profil != null && !profil.photo_profil.equals("")) {
            CSystem.AfficherPhoto(profil.photo_profil, profilePic, false);
        }
        profilCrop.addView(profilePic);

        String nomPrenom = profil.prenom_profil + " " + profil.nom_profil;
        TextView listNomPrenom = new TextView(getActivity());
        listNomPrenom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        listNomPrenom.setText(nomPrenom);
        listNomPrenom.setTextColor(getResources().getColor(R.color.textWhite));
        listNomPrenom.setTextSize(16);

        if (!accepted) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            profilePic.setColorFilter(filter);

            listNomPrenom.setTextColor(getResources().getColor(R.color.textSecondaryBlack));
        }

        participantElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReunionDetailsActivity reunionDetailsActivity = (ReunionDetailsActivity) getContext();

                ReunionProfilFragment bottomSheet = new ReunionProfilFragment(profil, reunionDetailsActivity.sys);
                bottomSheet.show(getFragmentManager(), "Profil");

                reunionDetailsActivity.setReunionProfilFragement(bottomSheet);

                reunionDetailsActivity.sys.getYourReunions();

            }
        });

        participantElement.addView(profilCrop);
        participantElement.addView(listNomPrenom);

        return participantElement;
    }
}
