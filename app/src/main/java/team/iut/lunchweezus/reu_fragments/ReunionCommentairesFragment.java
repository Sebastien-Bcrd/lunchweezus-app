package team.iut.lunchweezus.reu_fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ReunionDetailsActivity;
import team.iut.lunchweezus.ui.reunion.CommentaireAdapter;
import team.iut.lunchweezus.ui.reunion.CommentaireModel;
import team.iut.lunchweezus.ui.utils;

public class ReunionCommentairesFragment extends Fragment {

    private EditText commenter;
    public SwipeRefreshLayout refresh;

    // Constructeur
    public ReunionCommentairesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reunion_commentaires, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        commenter = getActivity().findViewById(R.id.commentaire_input);
        refresh = getActivity().findViewById(R.id.commentaire_refresh);
        FloatingActionButton envoyer = getActivity().findViewById(R.id.send_comment);
        envoyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReunionDetailsActivity reunionDetailsActivity = (ReunionDetailsActivity) getActivity();
                if (!commenter.getText().toString().isEmpty()) {
                    DateFormat heureFormat = new SimpleDateFormat("HH:mm:ss");
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date heureComment = new Date();
                    Date dateComment = new Date();
                    Commentaire commentaire = new Commentaire();
                    commentaire.setId_profil(reunionDetailsActivity.sys.userProfil.getId_profil());
                    commentaire.setContenu_commentaire(commenter.getText().toString());
                    commentaire.setId_reunion(reunionDetailsActivity.sys.reunionSelected.getId());
                    commentaire.setDate(dateFormat.format(dateComment));
                    commentaire.setHeure(heureFormat.format(heureComment));
                    reunionDetailsActivity.sys.addCommentaire(commentaire, true);
                    commenter.setText("");
                } else
                    utils.toaster(getActivity().getApplicationContext(), "Veuillez entrer un commentaire", 0);
            }
        });
    }

    /**
     * Ajoute les commentaires sur la liste de commentaires
     *
     * @param comments Liste d'objets contenant le commentaire
     */
    public void afficherComment(ArrayList<Commentaire> comments, int mProfileId) {
        RecyclerView messagesList = getActivity().findViewById(R.id.messages);

        messagesList.setLayoutManager(new LinearLayoutManager(getContext()));

        CommentaireAdapter commentaireAdapter = new CommentaireAdapter(getContext(), comments, mProfileId);

        if (commentaireAdapter.getItemCount() >= 1)
            messagesList.setAdapter(commentaireAdapter);
    }
}