package team.iut.lunchweezus.reu_fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.Logique.onEventListener;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.UserListReunionsActivity;
import team.iut.lunchweezus.ui.dialogs.FullScreenDialog;
import team.iut.lunchweezus.ui.utils;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;


@SuppressLint("ValidFragment")
public class ReunionProfilFragment extends BottomSheetDialogFragment {
    private Profil profil;
    private CSystem sys;
    ImageView photo;
    TextView name, site, organise, participations, note;
    LinearLayout orgGrp, partGrp;
    String job;

    public ReunionProfilFragment(Profil profil, CSystem sys) {
        this.profil = profil;
        this.sys = sys;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
        // DO NOT REMOVE
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Context contextThemeWrapper = new ContextThemeWrapper(getContext(), getContext().getTheme());

        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        View v = localInflater.inflate(R.layout.profil_bottom_sheet_fragment, container, false);

        photo = v.findViewById(R.id.profil_modal_pic);
        name = v.findViewById(R.id.profil_modal_username);
        site = v.findViewById(R.id.profil_modal_location);
        organise = v.findViewById(R.id.profil_modal_organisees);
        participations = v.findViewById(R.id.profil_modal_participations);
        note =  v.findViewById(R.id.profil_modal_note);
        orgGrp = v.findViewById(R.id.profil_modal_organisees_ll);
        partGrp = v.findViewById(R.id.profil_modal_participations_ll);



        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (profil.photo_profil != null && !profil.photo_profil.equals("")) {
            CSystem.AfficherPhoto(profil.photo_profil, photo, false);

            photo.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    FullScreenDialog photoDialog = new FullScreenDialog();

                    photo.invalidate();
                    BitmapDrawable drawable = (BitmapDrawable) photo.getDrawable();
                    Bitmap bmp;
                    if (drawable != null) {
                        bmp = drawable.getBitmap();

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();

                        Bundle b = new Bundle();
                        b.putByteArray("image", byteArray);

                        photoDialog.setArguments(b);

                        FragmentTransaction ft = getFragmentManager().beginTransaction();

                        photoDialog.show(ft, FullScreenDialog.TAG);
                    } else {
                        utils.toaster(getContext(), "Impossible d'afficher cette photo en plein écran", 0);
                    }
                }
            });
        }

        name.setText(this.profil.prenom_profil + " " + this.profil.nom_profil);

        job = this.profil.fonction_profil;
        job = (job.equals("null") || job.equals(NULL) || job.equals("")) ? "Travaille" : job;

        site.setText(job + " à " + sys.userProfil.libelle_site);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * Est appeler quand on recoit les stats du profil ( nombre organisé et nombre participations)
     *
     * @param reunions Liste contenant les réunions de l'utilisateur sélectionné
     */
    // todo callback sebastien/affichage note

    public void AfficherStat(List<Reunion> reunions ) {
        final List<Reunion> participer = reunions;
        final List<Reunion> organiser = construireListOrganiser(reunions);

        //Creation d'un callback qui attend la reponse
        sys.getNote(new onEventListener<String>() {

            //affiche la note si la reponse est recu
            @Override
            public void onSuccess(String notation) {
                note.setText(notation);
            }

            @Override
            public void onFailure(Exception e) {
                note.setText("0");
            }
        });


        int nbParticiper = participer.size();
        int nbOrganiser = organiser.size();


        if (nbOrganiser != 0)
            orgGrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeActivityReuList(getContext(), UserListReunionsActivity.class, 3, profil, organiser, participer );
                }
            });

        if (nbParticiper != 0)
            partGrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeActivityReuList(getContext(), UserListReunionsActivity.class, 2, profil, organiser, participer);
                }
            });




        organise.setText(Integer.toString(nbOrganiser));
        participations.setText(Integer.toString(nbParticiper));








    }

    /**
     * Crée une liste de réunions que l'utilisateur sélectionné a organisé
     *
     * @param reunions Liste de toutes les réunions de l'utilisateur, c'est à dire celles aux quelles
     *                 il participe et celles qu'il organise
     * @return Retourne la liste des réunions organisées
     */
    public List<Reunion> construireListOrganiser(List<Reunion> reunions) {
        List<Reunion> organiser = new ArrayList<Reunion>();
        for (int i = 0; i < reunions.size(); i++) {
            if (reunions.get(i).getId_profil() == profil.id_profil) {
                organiser.add(reunions.get(i));
            }
        }
        return organiser;
    }

    /**
     * Permet de changer d'activitée en passant un profil, sert à afficher une liste de réunions d'un utilisateur sur l'activité UserListReunionActivity
     *
     * @param actualActivity Activitée actuelle
     * @param nextActivity   Activitée visée
     * @param arg            Vaut 0 pour les participations, 1 pour les organisations, 2 pour les participations d'un utilisateur qui n'est pas vous, 3 pour les organisations d'un utilisateur qui n'est pas vous
     * @param profil         Objet contenant le profil de l'utilisateur
     */
    public void ChangeActivityReuList(Context actualActivity, Class<?> nextActivity, int arg, Profil profil, List<Reunion> organiser, List<Reunion> participer) {
        Intent intent = new Intent(actualActivity, nextActivity);
        sys.Stop();
        intent.putExtra("CSystem", sys); // Passe le System en paramètre pour l'activité suivante
        intent.putExtra("arg", arg);
        intent.putExtra("user", profil);
        intent.putExtra("organiser", organiser.toArray(new Reunion[organiser.size()]));
        intent.putExtra("participer", participer.toArray(new Reunion[participer.size()]));
        startActivity(intent);
    }
}
