package team.iut.lunchweezus.reu_fragments;

import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;

import team.iut.lunchweezus.Beans.Note;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.dialogs.FullScreenDialog;
import team.iut.lunchweezus.ui.utils;

public class ReunionNotesFragment extends Fragment {

    RatingBar avgRating;
    TextView avgRatingNb, nbAvis;
    public SwipeRefreshLayout refresh;
    public LinearLayout avisLayout;

    public ReunionNotesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reunion_notes, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        avgRating = view.findViewById(R.id.avg_rating);
        avgRatingNb = view.findViewById(R.id.avg_rating_txt);
        nbAvis = view.findViewById(R.id.nb_notes);
        refresh = view.findViewById(R.id.notes_refresh);
        avisLayout = view.findViewById(R.id.notes);
    }

    /**
     * Affiche une note sur la page. Est appelé plusieurs fois de suite pour afficher toutes les notes une par une
     *
     * @param note Objet contenant les informations de la note
     * @param me   Vaut true si il s'agit de ma note, false si il ne s'agit pas de ma note
     */
    public void addNote(Note note, boolean me) {
        FragmentActivity act = getActivity();

        int margins = utils.dpCalc(getActivity().getApplicationContext(), 8);
        int extraMargins = utils.dpCalc(getActivity().getApplicationContext(), 16);
        int imageSize = utils.dpCalc(getActivity().getApplicationContext(), 40);

        LinearLayout notesList = getActivity().findViewById(R.id.notes);
        LinearLayout messageElement = makeLL(extraMargins, margins, margins, 0, false);

        CardView profilCrop = new CardView(getActivity());
        LinearLayout.LayoutParams profilCropParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        profilCropParams.setMargins(0, 0, margins, 0);
        profilCrop.setLayoutParams(profilCropParams);
        profilCrop.setRadius(imageSize / 2);
        profilCrop.setCardElevation(0);
        profilCrop.setCardBackgroundColor(getResources().getColor(R.color.transparent));

        final ImageView profilePic = new ImageView(act);
        CSystem.AfficherPhoto(note.id_profil + ".jpg", profilePic, false);
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(imageSize, imageSize);
        profilePic.setLayoutParams(imageParams);

        profilCrop.addView(profilePic);

        profilCrop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                FullScreenDialog photoDialog = new FullScreenDialog();

                profilePic.invalidate();
                BitmapDrawable drawable = (BitmapDrawable) profilePic.getDrawable();
                Bitmap bmp;
                if (drawable != null) {
                    bmp = drawable.getBitmap();

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    Bundle b = new Bundle();
                    b.putByteArray("image", byteArray);

                    photoDialog.setArguments(b);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();

                    photoDialog.show(ft, FullScreenDialog.TAG);
                } else {
                    utils.toaster(getContext(), "Impossible d'afficher cette photo en plein écran", 0);
                }
            }
        });

        LinearLayout contentLL = makeLL(margins, 0, margins, 0, true);

        TextView name = utils.makeText(getActivity().getApplicationContext(), (me) ? "Vous" : note.prenom_profil + " " + note.nom_profil, 0, 0, 0, 0, 14, (me) ? getResources().getColor(R.color.colorAccentNight) : getResources().getColor(R.color.textWhite), -1, -1, false, ViewGroup.LayoutParams.WRAP_CONTENT);

        RatingBar rating = new android.support.v7.widget.AppCompatRatingBar(getContext());
        LinearLayout.LayoutParams rl = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        rl.setMargins(0, 0, margins, margins);
        rating.setLayoutParams(rl);
        rating.setIsIndicator(true);
        rating.setNumStars(5);
        rating.setRating(note.valeur_note);
        LayerDrawable rs = (LayerDrawable) rating.getProgressDrawable();
        rs.getDrawable(2).setColorFilter(ContextCompat.getColor(getContext(), R.color.textWhite), PorterDuff.Mode.SRC_ATOP);
        rs.getDrawable(1).setColorFilter(ContextCompat.getColor(getContext(), R.color.textWhite), PorterDuff.Mode.SRC_ATOP);
        rs.getDrawable(0).setColorFilter(ContextCompat.getColor(getContext(), R.color.textSecondaryAlphaWhite), PorterDuff.Mode.SRC_ATOP);

        contentLL.addView(name);
        contentLL.addView(rating);

        messageElement.addView(profilCrop);
        messageElement.addView(contentLL);

        notesList.addView(messageElement);
    }

    /**
     * Crée un LinearLayout afin d'y créer un conteneur pour les notes
     *
     * @param marginLeft   Marge à gauche
     * @param marginTop    Marge au dessus
     * @param marginRight  Marge à droite
     * @param marginBottom Marge au dessous
     * @param orientation  Vaut true si l'orientation du layout doit être vertical, false si l'orientation doit être horizontal
     * @return
     */
    private LinearLayout makeLL(int marginLeft, int marginTop, int marginRight, int marginBottom, boolean orientation) {
        LinearLayout out = new LinearLayout(getActivity());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        out.setLayoutParams(params);
        out.setOrientation(
                (orientation) ? LinearLayout.VERTICAL : LinearLayout.HORIZONTAL
        );

        return out;
    }

    /**
     * Affiche la moyenne des notes et le nombre de notes
     *      *
     * @param m Moyenne des notes
     * @param n Nombre total de notes
     */
    public void setCounts(float m, int n) {
        DecimalFormat df = new DecimalFormat("0.0");

        avgRating.setRating(m);
        avgRatingNb.setText(df.format(m));

        String a = " note";
        if (n > 1)
            a += "s";
        if (n > 0)
            nbAvis.setText(n + a);
        else
            nbAvis.setText("Aucunes notes");
    }
}
