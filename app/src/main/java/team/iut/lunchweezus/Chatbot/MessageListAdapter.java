package team.iut.lunchweezus.Chatbot;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import java.util.ArrayList;

import team.iut.lunchweezus.R;

public class MessageListAdapter extends BaseAdapter {

    private ArrayList<Message> mMessageList;
    Context context;

    /**
     * Contructeur par default
     * @param MessageList Liste des messages à afficher
     * @param context Context de l'application
     */
    public MessageListAdapter(ArrayList<Message> MessageList, Context context) {
        mMessageList = MessageList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Permet l'affichage des messages dans la listView
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder = new MessageViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Message message = mMessageList.get(position);
       //Permet de differencier les messages de l'utilisateur et du bot ( messanging )
        if (message.getPersonne().equals("client")) {
            convertView = messageInflater.inflate(R.layout.my_message, null);
            holder.messageB = convertView.findViewById(R.id.humanResponse);
            convertView.setTag(holder);
            holder.messageB.setText(message.getText());
        } else {
            convertView = messageInflater.inflate(R.layout.their_message, null);
            holder.messageH = convertView.findViewById(R.id.messageBot);

            convertView.setTag(holder);
            holder.messageH.setText(message.getText());
        }
        return convertView;
    }
}