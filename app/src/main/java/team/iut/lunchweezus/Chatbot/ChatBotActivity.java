package team.iut.lunchweezus.Chatbot;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import team.iut.lunchweezus.ActivityFonction;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.utils;

import static java.lang.Thread.sleep;

public class ChatBotActivity extends ActivityFonction {

    TextView input;
    TextView inputMsg, outputMsg;
    private MessageListAdapter messageAdapter;
    private ListView messagesView;
    //Definition de la liste de message
    ArrayList mesMessage = new ArrayList<Message>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatbot_activity);
        RetrieveFeedTask task = new RetrieveFeedTask();
        task.execute("bonjour");

        //Definition des messages
        input = findViewById(R.id.humanInput);
        inputMsg = findViewById(R.id.humanResponse);
        outputMsg = findViewById(R.id.messageBot);

        // adapter
        messageAdapter = new MessageListAdapter(mesMessage, this);
        messagesView = findViewById(R.id.messages_view);
        messagesView.setAdapter(messageAdapter);

        //Definition et affichage de la toolbar
        final Toolbar toolbar = findViewById(R.id.cb_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Chatbot");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    input = findViewById(R.id.humanInput);
                    inputMsg = findViewById(R.id.humanResponse);
                    String msg = input.getText().toString();
                    //ajout du message
                    Message msge = new Message(msg, "client");
                    mesMessage.add(msge);
                    //Envoie du message de l'utilisateur a l'api
                    RetrieveFeedTask task = new RetrieveFeedTask();
                    task.execute(msg);
                    input.setText("");
                    //ferme le clavier apres envoie du message
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    messageAdapter.notifyDataSetChanged();
                    return true;
                }
                return false;
            }
        });

        //definition du bouton pour l'envoie
        ImageButton button = findViewById(R.id.button);
        button.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        input = findViewById(R.id.humanInput);
                        inputMsg = findViewById(R.id.humanResponse);
                        String msg = input.getText().toString();
                        //ajout du message
                        Message msge = new Message(msg, "client");
                        mesMessage.add(msge);
                        //Envoie du message de l'utilisateur a l'api
                        RetrieveFeedTask task = new RetrieveFeedTask();
                        task.execute(msg);
                        input.setText("");
                        //ferme le clavier apres envoie du message
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        messageAdapter.notifyDataSetChanged();
                    }
                }
        );

    }


    /**
     * Permet de faire un call api a l'api google pour question L'ia et obtenir une réponse
     *
     * @param query Question a poser au chatbot
     * @return Reponse du bot
     * @throws UnsupportedEncodingException
     */
    public String GetText(String query) throws UnsupportedEncodingException {

        String text = "";
        BufferedReader reader = null;

        // Send data
        try {

            // Defined URL  where to send data
            URL url = new URL("https://api.api.ai/v1/query?v=20150910");

            // Send POST data request

            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);

            conn.setRequestProperty("Authorization", "Bearer ffb50158423b4c5398ccd32191544e43");
            conn.setRequestProperty("Content-Type", "application/json");

            //Create JSONObject here
            JSONObject jsonParam = new JSONObject();
            JSONArray queryArray = new JSONArray();
            queryArray.put(query);
            jsonParam.put("query", queryArray);
            jsonParam.put("lang", "FR");
            jsonParam.put("sessionId", "1234567890");


            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            Log.d("karma", "after conversion is " + jsonParam.toString());
            wr.write(jsonParam.toString());
            wr.flush();
            Log.d("karma", "json is " + jsonParam);

            // Get the server response

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;


            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Append server response in string
                sb.append(line + "\n");
            }

            text = sb.toString();

            JSONObject object1 = new JSONObject(text);
            JSONObject object = object1.getJSONObject("result");
            JSONObject fulfillment = null;
            String speech = null;
            fulfillment = object.getJSONObject("fulfillment");

            speech = fulfillment.optString("speech");

            Log.d("karma ", "response is " + text);
            //Creation
            return speech;

        } catch (Exception ex) {
            Log.d("karma", "exception at last " + ex);
        } finally {
            try {

            } catch (Exception ex) {
            }
        }
        Log.d("karma", "je vais return null ");
        return null;
    }


    /**
     * Permet de realiser des taches asyncrone
     * Excute en arrière plan une methode ( la methode getText)
     * et excute du code a la fin de l'execution de la methode ( met le message dans la liste )
     */
    class RetrieveFeedTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            String s = null;
            input = findViewById(R.id.humanInput);
            String msg = input.getText().toString();

            if (!msg.equals("")) {
                try {
                    s = GetText(msg);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.d("karma", "Exception occurred " + e);
                }
            } else {
                try {
                    s = GetText("heya");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.d("karma", "Exception occurred " + e);
                }
            }
            return s;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Message msge = new Message(s, "bot");
            mesMessage.add(msge);
            messageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        ChatBotActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }
}
