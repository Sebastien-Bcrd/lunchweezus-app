package team.iut.lunchweezus.Chatbot;

public class Message {

    private String text;
    private String personne;

    /**
     * Constructeur par default pour la création d'un nouveau message
     */
    public Message(String text, String personne) {
        this.text = text;
        this.personne = personne;
    }

    public String getText() {
        return text;
    }

    public String getPersonne() {
        return personne;
    }


}



