package team.iut.lunchweezus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import team.iut.lunchweezus.Beans.DetailReunion;
import team.iut.lunchweezus.Beans.Geoloc.DataAdresse;
import team.iut.lunchweezus.Beans.Notification;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Beans.Site;
import team.iut.lunchweezus.Logique.CSystem;

/**
 * Cette classe contiendra les fonctions de success et d'erreur que CSystem appelera lors d'une envoie au WebService
 * Elle contiendra aussi CSystem Attribu sys
 */
public class ActivityFonction extends AppCompatActivity {

    public CSystem sys; // Contien CSystem
    public double lat, lng;

    /**
     * permet de récupérer le CSystem du getExtra() et appel la fonciton start() de CSystem
     * Devra être appeler à chaque début d'activité (dans le onCreate() ou/et le onResume()) qui héritera de ActivityFonction
     * Attention ! Chaque activity qui hérite de ActivityFonction devra être connu en attribu par la class CActivitys
     */
    public void Debut()
    {
        sys = (CSystem) getIntent().getSerializableExtra("CSystem");
        sys.Start(this); // s'iniasilise dans CSystem
    }

    /**
     * Permet de changer de d'activité, appel la fonction Stop() de CSystem et passe le CSystem en putExtra() et start l'activité suivante
     * @param actualActivity L'activité actuel qui hérite de ActivityFonction
     * @param nextActivity L'activité qui sera start
     */
    public void ChangeActivity(Context actualActivity, Class<?> nextActivity)
    {
        Intent intent = new Intent(actualActivity,nextActivity);
        sys.Stop();
        intent.putExtra("CSystem", sys); // Passe le System en paramètre pour l'activité suivante
        startActivity(intent);
    }

    /**
     * Est appeler quand on recoit les informations du détails d'une réunion
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param detail est l'objet DetailReunion récupérer par le webService
     * @param con défini s'il y a une erreur, true : pas d'erreur, false : erreur
     */
    public void SuccessDetailReunion(final DetailReunion detail, final boolean con)
    {
    }

    /**
     * Est appeler quand ne recoit pas les informations du détails d'une réunion
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void ErreurDetailReunion(final String message)
    {
    }

    /**
     * Est Appeler quand on recoit les données de ces réunions
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param reunions est la list de Reunion que renvoie le WebService
     */
    public void SuccessYourReunions(final List<Reunion> reunions)
    {
    }

    /**
     * Est Appeler qaund on ne recoit pas les données ces réunions
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void ErreurYourReunions(final String message)
    {
    }

    /**
     * Est Appeler quand on récupere tout les profils
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param profils est la list Profil que renvoie le WebService
     */
    public void SuccessGetAllProfil(final List<Profil> profils){
    }

    /**
     * Est Appeler quand on ne récupere pas tout les profils
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void ErreurGetAllProfil(final String message){
    }

    /**
     * Est appeler quand la réponse de la modif d'une réunion est arrivé
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param resultat true : si la modif est bien effectuer. false : s'il y a une erreur
     */
    public void SuccessModifReunion(boolean resultat)
    {
    }

    /**
     * Est appeler quand on ne recoit pas la réponse de la modif d'une réunion
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void ErreurModifReunion(String message)
    {
    }

    /**
     * Est appeler quand on recoit les données de l'api geoloc (Foursquare)
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param dataAdresses est la List DataAdresse recu par l'api
     */
    public void successAppelApi(List<DataAdresse> dataAdresses)
    {
    }

    /**
     * Est appler quand on a réussie a ajouter des inviter
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param result true : l'invitation a bien été effectuer. false : s'il y a une erreur
     */
    public void successInviter(final boolean result)
    {
    }

    /**
     * Est appler quand on a pas réussie a ajouter des inviter
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void erreurInviter(String message)
    {
    }

    /**
     * Est appelé quand les notifs sont bien récupérées
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param notifications est la list Notification que renvoie le WebService
     */
    public void successNotifications(final List<Notification> notifications) {
    }

    /**
     * Est appelé quand les notifs ne sont pas récupérées
     * doit etre Override dans l'activity qui hérite ActivityFonction en cas d'utilisation de cette fonction
     * @param message contien le message d'erreur
     */
    public void ErreurNotifications(final String message)
    {
    }

    /**
     * Est appelé quand on récupére les sites du serveur
     * @param sites est la list Site que renvoie le WebService
     */
    public void successGetAllSite(final List<Site> sites)
    {
    }

    /**
     * Est appelé quand on a bien supprimer une notif
     * @param resultat
     */
    public void successSuppNotif(final String resultat)
    {
    }

    /**
     * Est appelé quand on a une erreur sur l'action de supprimer une notif
     * @param resultat
     */
    public void erreurSuppNotif(final String resultat)
    {
    }


    public void successAnnonce(final String message)
    {

    }
    public void ErreurAnnonce(final String message)
    {
    }

}
