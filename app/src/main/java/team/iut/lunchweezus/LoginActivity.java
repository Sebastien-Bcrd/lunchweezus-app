package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import team.iut.lunchweezus.ui.utils;

public class LoginActivity extends ActivityFonction {

    private TextInputEditText matricule_edit_text, password_edit_text;
    private TextInputLayout matricule_layout, password_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Debut();

        matricule_layout = findViewById(R.id.matricule_layout);
        matricule_edit_text = findViewById(R.id.matricule_edit_text);
        password_layout = findViewById(R.id.password_layout);
        password_edit_text = findViewById(R.id.password_edit_text);
        Button seconn = findViewById(R.id.btnseconn);
        TextView registre = findViewById(R.id.btnRegistre);
        TextView reinit = findViewById(R.id.btnReinit);

        //matricule_layout.setError(null); // hide error
        seconn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> values = new ArrayList<>();

                //verifier si les champs ne sont pas vide
                password_layout.setError(null); // hide error

                if (matricule_edit_text.getText().toString().isEmpty()) {
                    matricule_layout.setError("Requis"); // show error
                } else {
                    matricule_layout.setError(null); // hide error
                    values.add(matricule_edit_text.getText().toString());
                }

                if (password_edit_text.getText().toString().isEmpty()) {
                    password_layout.setError("Requis");
                } else {
                    password_layout.setError(null);
                    values.add("");
                }

                if (values.size() == 2) {
                    password_layout.setError(null); // hide error
                    String login = matricule_edit_text.getText().toString();
                    String pass = password_edit_text.getText().toString();
                    sys.CheckPassword(login, pass);
                }
            }
        });

        registre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeActivity(LoginActivity.this, InscriptionActivity.class);
                overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
            }
        });

        reinit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeActivity(LoginActivity.this, ResetPasswordActivity.class);
                overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
            }
        });


    }

    @Override
    public void onResume() {
        Debut();
        super.onResume();
    }

    /**
     * CSystem fait appel à cette fonction si la connexion a réussi
     *
     * @param con Vaut true si il n'y a pas de problème de connexion, false si il y a des problèmes
     *            comme un mot de passe éronné ou un login inexistant
     */
    public void Success(boolean con) {
        if (con) {
            ChangeActivity(LoginActivity.this, AccueilActivity.class);
            finish();
        } else {
            utils.toaster(getApplicationContext(), "Mauvais mot de passe ou login", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si la connexion a échoué
     *
     * @param message Message d'erreur
     */
    public void Error(String message) {
        utils.toaster(getApplicationContext(), "Impossible de se connecter", 0);
    }

    /**
     * Laisser cette fonction vide permet d'éviter de retourner sur l'animation de démarrage de l'application
     */
    @Override
    public void onBackPressed() {
        //do nothing
    }
}
