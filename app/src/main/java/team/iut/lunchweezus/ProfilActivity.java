package team.iut.lunchweezus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.Logique.onEventListener;
import team.iut.lunchweezus.ui.MyCardView;
import team.iut.lunchweezus.ui.dialogs.FullScreenDialog;
import team.iut.lunchweezus.ui.utils;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

public class ProfilActivity extends ActivityFonction {

    TextView username, userplace, organisations, participations, note; // Vues texte permettant d'afficher le nom & prénom de la personne, son site d'Armatis, son nombre de réu organisées, et son nombre de participations
    ImageView profilepic; // Contiendra la photo de profil
    LinearLayout orgGroup, partGroup, parent; // Groupes de vues sur les réunions organisées, le nombre de participations, et la liste de réunions en historique
    View placeholder; // Vue contenant le placeholder qui s'affiche si il n'y a aucune réunion en historique
    Bundle b; // Bundle permettant d'envoyer l'image de profil encodée en Base64 vers un modal permettant de l'afficher en plein écran
    List<Reunion> reunionsParticipations; // Liste contenant les réunions organisées
    List<Reunion> reunionOrganistaeur; // Liste contenant les réunions dont la personne en est participatant
    ProgressDialog p; // Contient le modal indiquant un cercle de chargement quand à la suppression d'un compte
    View profilepiccard; // Contient la vue contenant l'image de profil, est utilisé pour la transition vers la modification de profil
    String job; // Contient la fonction de la personne

    private FloatingActionButton editFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Debut();

        setContentView(R.layout.activity_profil);
        profilepic = findViewById(R.id.user_pic);
        profilepiccard = findViewById(R.id.user_pic_card);

        final Toolbar toolbar = findViewById(R.id.profil_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Mon profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        editFAB = findViewById(R.id.profil_edit_fab);
        editFAB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ProfilActivity.this, ModifierProfilActivity.class);
                // Pass data object in the bundle and populate details activity.
                sys.Stop();
                String contact = "transition";
                intent.putExtra(ModifierProfilActivity.EXTRA_CONTACT, contact);
                intent.putExtra("CSystem", sys);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(ProfilActivity.this, profilepiccard, "userPic");
                startActivity(intent, options.toBundle());
            }
        });

        sys.getYourReunions();

        orgGroup = findViewById(R.id.organisations_ll);
        partGroup = findViewById(R.id.participations_ll);
        parent = findViewById(R.id.historique_parent);

        organisations = findViewById(R.id.reu_organisees);
        participations = findViewById(R.id.reu_participations);
        note = findViewById(R.id.reu_note);

        reunionsParticipations = sys.reunionDataSource.getAllReunions();
        reunionOrganistaeur = sys.reunionDataSource.getReunionsOrganisateur(sys.userProfil);
        note.setText("1");

        final SwipeRefreshLayout refresh = findViewById(R.id.profil_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sys.getYourReunions();

                refresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Debut();

        Profil user = sys.GetUserProfilLocal();
        setInfo(user);
    }

    /**
     * Affiche les infos du profil
     *
     * @param profil Objet contenant un profil
     */
    private void setInfo(final Profil profil) {
        username = findViewById(R.id.username);
        userplace = findViewById(R.id.user_location);
        profilepic = findViewById(R.id.user_pic);
        organisations = findViewById(R.id.reu_organisees);
        participations = findViewById(R.id.reu_participations);
        note = findViewById(R.id.reu_note);

        username.setText(profil.prenom_profil + " " + profil.nom_profil);

        job = profil.fonction_profil;
        job = (job.equals("null") || job.equals(NULL) || job.equals("")) ? "Travaille" : job;

        userplace.setText(job + " à " + profil.libelle_site);

        if (profil.photo_profil != null && !profil.photo_profil.equals("")) {
            CSystem.AfficherPhoto(profil.photo_profil, profilepic, false);

            profilepic.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    FullScreenDialog photoDialog = new FullScreenDialog();

                    profilepic.invalidate();
                    BitmapDrawable drawable = (BitmapDrawable) profilepic.getDrawable();
                    Bitmap bmp;
                    if (drawable != null) {
                        bmp = drawable.getBitmap();

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();

                        b = new Bundle();
                        b.putByteArray("image", byteArray);

                        photoDialog.setArguments(b);

                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                        photoDialog.show(ft, FullScreenDialog.TAG);

                    } else {
                        utils.toaster(getApplicationContext(), "Impossible de récupérer votre photo de profil", 0);
                    }
                }
            });
        }
        //Appel de l'api pour recuperer la note de l'utilisateur
        sys.getNote(new onEventListener<String>() {
            @Override
            public void onSuccess(String notation) {
                note.setText(notation); // affichage de la note si l'appel est un succes .
            }

            @Override
            public void onFailure(Exception e) {
                note.setText("0");
            }
        });

        if (reunionOrganistaeur.size() != 0)
            orgGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeActivityReuList(ProfilActivity.this, UserListReunionsActivity.class, 1, profil);
                }
            });

        if (reunionsParticipations.size() != 0)
            partGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeActivityReuList(ProfilActivity.this, UserListReunionsActivity.class, 0, profil);
                }
            });

        organisations.setText(Integer.toString(reunionOrganistaeur.size()));
        participations.setText(Integer.toString(reunionsParticipations.size()));

    }

    /**
     * Réucpère une liste de réunions en historique uniquement
     *
     * @param reunions Liste contenant toutes les réunions de l'utilisateur
     * @return Retourne une liste de réunions en historique
     */
    public List<Reunion> construireHistorique(List<Reunion> reunions) {
        Date currentDate = new Date();
        List<Reunion> historique = new ArrayList<Reunion>();
        for (int i = 0; i < reunions.size(); i++) {
            try {
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date dateReunion = null;
                dateReunion = fmt.parse(reunions.get(i).date_reunion + " " + reunions.get(i).heure_reunion);
                if (currentDate.after(dateReunion)) {
                    historique.add(reunions.get(i));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return historique;
    }

    /**
     * CSystem fait appel à cetter fonction si la récupération des réunions a réussi
     *
     * @param reunions Réunions obtennues
     */
    @Override
    public void SuccessYourReunions(final List<Reunion> reunions) {
        editFAB.show(); // Affiche le bouton pour modifier son profil
        reunionsParticipations = reunions;
        reunionOrganistaeur = sys.reunionDataSource.getReunionsOrganisateur(sys.GetUserProfilLocal());

        organisations.setText(Integer.toString(reunionOrganistaeur.size()));
        participations.setText(Integer.toString(reunions.size()));
        List<MyCardView> listCards = utils.makeReunionCard(getApplicationContext(), this, construireHistorique(reunions));
        placeholder = findViewById(R.id.placeholder_profil);

        if (listCards.size() != 0) {
            parent.removeAllViewsInLayout();
            placeholder.setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < listCards.size(); i++) {
            parent.addView(listCards.get(i));
        }
    }

    /**
     * CSystem fait appel à cette fonction si la récupération des réunions a échoué
     *
     * @param message Message d'erreur
     */
    @Override
    public void ErreurYourReunions(final String message) {
        editFAB.hide(); // Cache le bouton pour modifier le profil
        List<Reunion> reunions = sys.reunionDataSource.getAllReunions();

        List<MyCardView> listCards = utils.makeReunionCard(getApplicationContext(), this, construireHistorique(reunions));

        placeholder = findViewById(R.id.placeholder_profil);

        if (listCards.size() != 0) {
            parent.removeAllViewsInLayout();
            placeholder.setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < listCards.size(); i++) {
            parent.addView(listCards.get(i));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profil_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.deconnexion:
                sys.userProfil.id_profil = 0;
                sys.profilDataSource.deleteProfil(sys.userProfil);
                sys.userProfil = null;
                /// ici supprimer les données
                sys.reunionDataSource.DropAllTables();
                finish();
                return true;
            case R.id.supprimer_compte:
                utils.alert(
                        this,
                        "Confirmation",
                        "Voulez-vous vraiment supprimer votre compte ?",
                        0,
                        "Oui",
                        "Non",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sys.supprimerProfil();
                                ProfilActivity.this.p = utils.loading(ProfilActivity.this, NULL, "Suppression du compte...");
                                p.show();
                            }
                        }, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                utils.toaster(getApplicationContext(), "Suppression du compte annulée", 0);
                            }
                        })
                        .show();
                return true;

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Permet de changer d'activitée en passant un profil, sert à afficher une liste de réunions d'un utilisateur sur l'activité UserListReunionActivity
     *
     * @param actualActivity Activitée actuelle
     * @param nextActivity   Activitée visée
     * @param arg            Vaut 0 pour les participations, 1 pour les organisations, 2 pour les participations d'un utilisateur qui n'est pas vous, 3 pour les organisations d'un utilisateur qui n'est pas vous
     * @param profil         Objet contenant le profil de l'utilisateur
     */
    public void ChangeActivityReuList(Context actualActivity, Class<?> nextActivity, int arg, Profil profil) {
        Intent intent = new Intent(actualActivity, nextActivity);
        sys.Stop();
        intent.putExtra("CSystem", sys); // Passe le System en paramètre pour l'activité suivante
        intent.putExtra("arg", arg);
        intent.putExtra("user", profil);
        startActivity(intent);
    }

    /**
     * CSystem fait appel à cette fonction si la suppression du profil a réussi
     */
    public void succesSupProfil() {
        sys.userProfil.id_profil = 0;
        sys.profilDataSource.deleteProfil(sys.userProfil);
        sys.userProfil = null;
        /// ici supprimer les données
        sys.reunionDataSource.DropAllTables();
        p.dismiss();
        finish();
    }

    /**
     * CSystem fait appel à cette fonction si la suppression du profil a échoué
     */
    public void erreurSupProfil() {
        p.dismiss();
        utils.toaster(ProfilActivity.this, "La suppression du profil a échoué", 0);
    }
}
