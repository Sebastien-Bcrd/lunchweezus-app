package team.iut.lunchweezus;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Site;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.ui.utils;

public class ModifierProfilActivity extends ActivityFonction implements AdapterView.OnItemSelectedListener {

    public static final int READ_EXTERNAL_STORAGE = 0;//variable pour identifier l'option dont on accpete
    private static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private static final int PICK_IMAGE_GALLERY_REQUEST_CODE = 609;
    String path;
    int idSite;
    public Bitmap bitmap;

    FloatingActionButton confirm;
    private ImageView profile_pic;
    TextInputEditText prenomText, nomText, fonctionText, mailText, oldPassword, newPassword, reponseQuestion;
    private TextInputLayout edit_layout_prenom, edit_layout_nom, edit_layout_job;
    public static final String EXTRA_CONTACT = "";
    private Spinner spnprofil, spnQuestion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_profil);

        Debut();

        profile_pic = findViewById(R.id.profile_pic);
        prenomText = findViewById(R.id.edit_prenom_title);
        nomText = findViewById(R.id.edit_nom_title);
        fonctionText = findViewById(R.id.edit_job_title);
        edit_layout_prenom = findViewById(R.id.edit_layout_prenom);
        edit_layout_nom = findViewById(R.id.edit_layout_nom);
        edit_layout_job = findViewById(R.id.edit_layout_job);
        mailText = findViewById(R.id.edit_mail_title);
        oldPassword = findViewById(R.id.edit_password_title);
        newPassword = findViewById(R.id.edit_newPassword_title);
        reponseQuestion = findViewById(R.id.edit_reponse_title);


        //variables pour le menu Spinner qui montre les Entreprises
        //pour le menu Spinner
        spnprofil = findViewById(R.id.spnprofil);//menu

        spnQuestion = findViewById(R.id.spnQuestion);
        getAllQuestion();

        if (sys.userProfil.photo_profil != null && !sys.userProfil.photo_profil.equals("")) {
            CSystem.AfficherPhoto(sys.userProfil.photo_profil, profile_pic, false);
        }
        prenomText.setText(sys.userProfil.prenom_profil);
        nomText.setText(sys.userProfil.nom_profil);
        if (!sys.userProfil.fonction_profil.equals("Fonction non définie")) {
            fonctionText.setText(sys.userProfil.fonction_profil);
        }
        spnprofil.setSelection(sys.userProfil.id_site - 1);
        RelativeLayout modifPhoto = findViewById(R.id.modif_photo_layout);//button pour choisir la photo

        final Toolbar toolbar = findViewById(R.id.modifier_profil_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Modifier mon profil");
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                supportFinishAfterTransition();
            }
        });

        modifPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Differents Permissions pour la CAMERA et le Telephone GALLERY
                if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
                    }
                } else {
                    callgalery();
                }
            }
        });

        sys.getAllSite();

        //btn Confirmer Modification
        confirm = findViewById(R.id.profil_confirm_fab);
        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int values = 0;

                if (nomText.getText().toString().isEmpty()) {
                    edit_layout_nom.setError("Requis"); // show error
                } else {
                    edit_layout_nom.setError(null);
                    ++values;
                }

                if (prenomText.getText().toString().isEmpty()) {
                    edit_layout_prenom.setError("Requis");
                } else {
                    edit_layout_prenom.setError(null);
                    ++values;
                }

                if (fonctionText.getText().toString().isEmpty()) {
                    edit_layout_job.setError("Requis");
                } else {
                    edit_layout_job.setError(null);
                    ++values;
                }


                if (values == 3) {
                    Profil user = new Profil();
                    user.id_profil = sys.userProfil.id_profil;
                    user.prenom_profil = prenomText.getText().toString();
                    user.nom_profil = nomText.getText().toString();
                    user.fonction_profil = fonctionText.getText().toString();
                    if(spnprofil.getSelectedItem().toString() != null) user.libelle_site = spnprofil.getSelectedItem().toString();
                    user.id_site = idSite;
                    user.mail_profil = mailText.getText().toString();
                    user.oldPassword = oldPassword.getText().toString();
                    user.newPassword = newPassword.getText().toString();
                    user.reponse_profil = reponseQuestion.getText().toString();
                    if (bitmap != null) user.photo_profil = convertImgString(bitmap);

                    utils.toaster(getApplicationContext(), "Modification en cours...", 0);

                    sys.modifierProfil(user);
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                callgalery();
        }

    }

    /**
     * Crée un modal listant les choix possibles pour la source de l'image à envoyer
     */
    private void callgalery() {

        final CharSequence[] options = {"Caméra", "Galerie", "Annuler"};
        final AlertDialog.Builder alertOp = new AlertDialog.Builder(ModifierProfilActivity.this, R.style.AlertDialogCustom);
        alertOp.setTitle("Choisissez une source");
        alertOp.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (options[i].equals("Caméra")) {
                    prendrePhoto();
                } else {
                    if (options[i].equals("Galerie")) {
                        openImagesDocument();
                    } else {
                        dialogInterface.dismiss();
                    }
                }
            }
        });
        alertOp.show();
    }

    /**
     * Selon le retour des fonctions précédentes, on ouvre un découpeur d'image pour que l'utilisateur
     * découpe l'image au format nécessaire, ou on récupère l'image et l'affiche si la découpe vient
     * d'être effectuée. Cette fonction est gérée automatiquement par les activitées
     *
     * @param requestCode Code de requête
     * @param resultCode  Code de résultat
     * @param data        Données reçues depuis l'activité qui vient d'être fermée (appareil photo, documents,
     *                    ou découpeur d'image)
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_ACTION_PICK_REQUEST_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                //savoir si le processus a fini ou pas
                                public void onScanCompleted(String s, Uri uri) {
                                    Log.i("Chemin d'hebergement", "Path: " + path);
                                    System.out.println("Path: " + path);
                                }
                            });
                    uri = Uri.parse("file:" + path);
                    openCropActivity(uri, uri);
                    break;

                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        uri = UCrop.getOutput(data);
                        showImage(uri);
                    }
                    break;

                case PICK_IMAGE_GALLERY_REQUEST_CODE:
                    File file = null;
                    try {
                        Uri sourceUri  = data.getData();
                        file = getImageFile();
                        Uri destinationUri = Uri.fromFile(file);
                        openCropActivity(sourceUri, destinationUri);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * Permet d'encoder l'image en base 64 avant l'envoi au serveur. Cette fonction compresse
     * également l'image au format jpeg.
     *
     * @param bitmap Image à encoder
     * @return Retourne l'image convertie en base 64
     */
    public String convertImgString(Bitmap bitmap) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, array);
        byte[] imageByte = array.toByteArray();

        return Base64.encodeToString(imageByte, Base64.DEFAULT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    /**
     * CSystem fait appel à cette fonction si la modification du profil a réussi
     *
     * @param user Contient le profil modifié
     */
    public void SuccessModifierProfil(Profil user) {
        user.photo_profil = user.id_profil + ".jpg";
        sys.SetUserProfil(user);
        sys.userProfil = user;

        utils.toaster(this, "Modification réussie !", 0);

        Picasso.get().invalidate(CSystem.URL_SITE + "/img_Profil/" + sys.userProfil.id_profil + ".jpg"); // A modifier lors du changement de serveur
        finish();
    }

    /**
     * CSystem fait appel à cette fonction si la modification du profil a échoué
     *
     * @param message Message d'erreur
     */
    public void ErreurModifierProfil(String message) {
        utils.toaster(getApplicationContext(), "Impossible de modifier le profil", 0);
    }

    /**
     * Ouvre l'appareil photo pour prendre une photo
     */
    private void prendrePhoto() {
        //CHEMIN de la photo prise depuis la CAMERA
        //varible qu'on crée pour la photo prise depuis la CAMERA
        File fileImage = new File(Environment.getExternalStorageDirectory(), "LunchWeezUs");
        boolean isCreate = fileImage.exists();
        String nom_img = "";
        if (isCreate) {
            nom_img = System.currentTimeMillis() / 1000 + ".jpg";
        }
        path = Environment.getExternalStorageDirectory() + File.separator + "LunchWeezUs" + File.separator + nom_img;

        File imgUpImage = new File(path);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri imageUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = getApplicationContext().getPackageName() + ".provider";
            imageUri = FileProvider.getUriForFile(ModifierProfilActivity.this, authorities, imgUpImage);
        } else {
            imageUri = Uri.fromFile(imgUpImage);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        startActivityForResult(intent, CAMERA_ACTION_PICK_REQUEST_CODE);
    }

    /**
     * Ouvre le découpeur d'image
     *
     * @param sourceUri      Image source
     * @param destinationUri Chemin de destination, où sauvegarder l'image
     */
    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.white));
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(256, 256)
                .withAspectRatio(1, 1)
                .start(this);
    }

    /**
     * Permet d'afficher l'image sur le formulaire
     *
     * @param imageUri Chemin de l'image
     */
    private void showImage(Uri imageUri) {
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), imageUri);
            profile_pic.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ouvre le navigateur de documents pour choisir une image
     */
    private void openImagesDocument() {
        Intent pictureIntent = new Intent(Intent.ACTION_PICK);
        pictureIntent.setType("image/*");
        startActivityForResult(pictureIntent, PICK_IMAGE_GALLERY_REQUEST_CODE);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spnprofil) {
            idSite = position + 1;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // DO NOT REMOVE
    }

    /**
     * CSystem fait appel à cette fonction si la récupération des sites d'Armatis a réussi
     *
     * @param sites Liste contenant les sites récupérés
     */
    @Override
    public void successGetAllSite(final List<Site> sites) {
        final String[] strVille = new String[sites.size()];
        int indexdefault = 0;
        for (int i = 0; i < sites.size(); i++) {
            strVille[i] = sites.get(i).libelle_site;
            if (sys.userProfil.getId_site() == sites.get(i).id_site) {
                indexdefault = i;
            }
        }

        spnprofil.setOnItemSelectedListener(this);
        List<String> listEntr = new ArrayList<>();
        Collections.addAll(listEntr, strVille);
        ArrayAdapter<String> cmbAdapter = new ArrayAdapter<>(this, R.layout.spineer_entreprises_liste, R.id.txtSpinner, listEntr);
        spnprofil.setAdapter(cmbAdapter);
        spnprofil.setSelection(indexdefault);

    }

    /**
     * Permet d'initiliser le spinner qui permet de choisir une question à laquelle répondre
     */
    public void getAllQuestion() {
        final String[] strQuestion = new String[4];
        strQuestion[0] = "Choisissez une question secrète...";

        strQuestion[1] = "Quel est le nom de votre meilleur ami d'enfance ?";
        strQuestion[2] = "Quel est votre ville de naissance ?";
        strQuestion[3] = "Quel est le nom de votre animal de compagnie ?";

        spnQuestion.setOnItemSelectedListener(ModifierProfilActivity.this);
        List<String> listQuestion = new ArrayList<>();
        Collections.addAll(listQuestion, strQuestion);
        ArrayAdapter<String> cmbAdapter3 = new ArrayAdapter<>(this, R.layout.spineer_questions_liste, R.id.txtSpinner_question, listQuestion);
        spnQuestion.setAdapter(cmbAdapter3);
    }

    /**
     * Permet de récupérer une image de la mémoire du téléphone et d'une carte microSD
     *
     * @return Fichier à croper
     * @throws IOException
     */
    private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "Camera"
        );
        System.out.println(storageDir.getAbsolutePath());
        if (storageDir.exists())
            System.out.println("File exists");
        else
            System.out.println("File not exists");
        File file = File.createTempFile(
                imageFileName, ".jpg", storageDir
        );
        String currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;
    }
}
