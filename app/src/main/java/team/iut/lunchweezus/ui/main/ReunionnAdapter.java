package team.iut.lunchweezus.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.AccueilActivity;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.utils;

public class ReunionnAdapter extends RecyclerView.Adapter<ReunionnAdapter.ReunionnViewHolder> {

    private List<Reunion> data;
    private AccueilActivity accueilActivity;

    public ReunionnAdapter(List<Reunion> data, AccueilActivity activity) {
        this.data = data;
        this.accueilActivity = activity;
    }

    @NonNull
    @Override
    public ReunionnViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ReunionnViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conteneur,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ReunionnViewHolder holder, int i) {
        final Reunion reunion = data.get(i);

        int margins = utils.dpCalc(accueilActivity.getApplicationContext(), 8);

        int height = utils.dpCalc(accueilActivity.getApplicationContext(), 248);

        int marginsBottom = utils.dpCalc(accueilActivity.getApplicationContext(), 88);

        // Il trouve le dernier élément du recyclerview et ajoute une marge plus grande pour le FAB
        if (i == data.size() - 1){

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    height
            );
            params.setMargins(margins, margins, margins, marginsBottom);
            holder.itemView.setLayoutParams(params);

        } else { // Sinon, pas de marge en bas (sinon il y a la marge du haut + celle du bas = double marge

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    height
            );
            params.setMargins(margins, margins, margins, 0);
            holder.itemView.setLayoutParams(params);
        }

        if(reunion.photo_reunion != null && !reunion.photo_reunion.equals("")) {
            CSystem.AfficherPhoto(reunion.photo_reunion, holder.imgFondConteneur, true);
        } else {
            holder.imgFondConteneur.setImageResource(R.drawable.reudefault);
        }

        holder.txtObjet.setText(reunion.objet_reunion);
        holder.txtObjet.setSelected(true);
        holder.txtLieu.setText(reunion.lieu_reunion);
        holder.txtLieu.setSelected(true);
        holder.txtPlace.setText(reunion.nbPlace_reunion + " places");

        try {
            String datePattern = "dd/MM/yyyy";
            String timePattern = "HH:mm";

            Date dateObj = new SimpleDateFormat("yyyy-MM-dd").parse(reunion.date_reunion);
            String date = new SimpleDateFormat(datePattern).format(dateObj);

            Date timeObj = new SimpleDateFormat("HH:mm:ss").parse(reunion.heure_reunion);
            String time = new SimpleDateFormat(timePattern).format(timeObj);

            holder.txtDate.setText(date + " à " + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final CSystem sys = accueilActivity.sys;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sys.reunionSelected = reunion;
                sys.status = 0;
                accueilActivity.ChangeAnimation();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ReunionnViewHolder extends RecyclerView.ViewHolder{

       TextView txtObjet;
       TextView txtLieu;
       TextView txtPlace;
       TextView txtDate;
       ImageView imgFondConteneur;

        public ReunionnViewHolder(View itemView) {
            super(itemView);
            imgFondConteneur = itemView.findViewById(R.id.imgFondConteneur);
            txtObjet = itemView.findViewById(R.id.txtObjet);
            txtPlace = itemView.findViewById(R.id.txtPlaces);
            txtDate = itemView.findViewById(R.id.txtDates);
            txtLieu = itemView.findViewById(R.id.txtLieu);
        }
    }
}
