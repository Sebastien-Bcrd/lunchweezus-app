package team.iut.lunchweezus.ui.notifications;

import android.graphics.drawable.Drawable;

/**
 * Modèle de notification servant à créer une vue contenant les détails d'une notification
 */
public class NotificationsModel {

    private String title, content, timestamp;
    private int status, type, notificationId, reunionId;
    private Drawable imageDrawable;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getReunionId() {
        return reunionId;
    }

    public void setReunionId(int reunionId) {
        this.reunionId = reunionId;
    }

    public Drawable getImageDrawable() {
        return imageDrawable;
    }

    public void setImageDrawable(Drawable imageDrawable) {
        this.imageDrawable = imageDrawable;
    }
}
