package team.iut.lunchweezus.ui.reunion;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class CommentaireModel {

    private String title, content;
    private boolean me;
    private int profileId;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public boolean getMe() {
        return me;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public int getProfileId() {
        return profileId;
    }
}
