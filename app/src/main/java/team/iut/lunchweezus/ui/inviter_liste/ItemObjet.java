package team.iut.lunchweezus.ui.inviter_liste;

public class ItemObjet {
    private String name;

    public ItemObjet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
