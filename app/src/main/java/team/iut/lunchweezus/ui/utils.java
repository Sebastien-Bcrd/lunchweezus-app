package team.iut.lunchweezus.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.ActivityFonction;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ReunionDetailsActivity;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

public class utils {

    /**
     * Crée une notification toast en bas de l'écran
     *
     * @param a Contexte de l'application
     * @param c Contenu du toast
     * @param t Durée d'affichage du toast
     *          0 = Court
     *          1 = Long
     */
    static public void toaster(Context a, String c, int t) {
        Toast.makeText(a, c, t).show();
    }

    /**
     * Crée une alert indiquant soit une information importante soit un choix
     *
     * @param c Contexte de l'activitée
     * @param t Titre du modal
     * @param m Message du modal, peut être NULL
     * @param b Permet de décider si on affiche juste un bouton neutre / OK ou un bouton
     *          positif / OUI & un bouton négatif / NON
     *          Par défaut, on affichera un bouton OK (Par exemple si une autre valeur que -1 ou 0
     *          est donnée à la méthode)
     *          -1 = Bouton neutre / OK
     *          0 = Boutons positif / OUI & négatif / NON
     * @param p Texte du bouton OK / OUI, peut être NULL (défaut: OK / Oui)
     * @param n Texte du bouton NON, peut être NULL (défaut: Non)
     * @return Retourne l'alert pour l'afficher dans le thread principal
     */
    static public AlertDialog.Builder alert(Context c, String t, String m, int b, String p, String n, final DialogInterface.OnClickListener pl, final DialogInterface.OnClickListener nl) {
        AlertDialog.Builder a = new AlertDialog.Builder(c, AlertDialog.THEME_DEVICE_DEFAULT_DARK);

        a.setTitle(t);

        if (m != null) {
            a.setMessage(m);
        }

        switch (b) {
            default: // Bouton OK
            case -1:
                if (p == null)
                    p = "OK";

                a.setNeutralButton(p, pl);
                break;
            case 0: // Bouton positif + négatif (comme Oui & Non)
                if (p == null)
                    p = "Oui";

                a.setPositiveButton(p, pl);

                if (n == null)
                    n = "Non";

                a.setNegativeButton(n, nl);
                break;
        }

        return a;
    }

    /**
     * Permet de créer un modal avec une ScrollView dedans, permettant l'affichage de contenu très
     * long comme des CGU ou un changelog
     *
     * @param c Contexte de l'application
     * @param t Titre du modal
     * @param v Contenu du modal
     * @return Retourne l'alert pour l'afficher dans le thread principal
     */
    static public AlertDialog.Builder viewDialog(Context c, String t, View v) {
        AlertDialog.Builder a = new AlertDialog.Builder(c, AlertDialog.THEME_DEVICE_DEFAULT_DARK);

        if (t != null)
            a.setTitle(t);

        a.setView(v);

        a.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return a;
    }

    /**
     * Crée un modal de chargement
     *
     * @param c Contexte de l'activitée
     * @param t Titre du modal, peut être NULL
     * @param m Message du modal
     * @return Retourne le modal de chargement pour pouvoir l'afficher et le fermer quand nécessaire
     */
    static public ProgressDialog loading(Context c, String t, String m) {
        ProgressDialog p = new ProgressDialog(c, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        if (!t.equals(NULL))
            p.setTitle(t);
        p.setMessage(m);
        p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return p;
    }

    /**
     * Crée un modal qui demande un texte
     *
     * @param c Contexte de l'activitée
     * @param m Message du modal
     * @param b Si il s'agît d'un prompt de suppression de réunion ou pour quitter une réunion
     * @return Retourne le modal pour pouvoir le paramétrer avant de l'afficher
     */
    static public AlertDialog.Builder prompt(final Context c, String m, final boolean b) {
        AlertDialog.Builder a = new AlertDialog.Builder(c, AlertDialog.THEME_DEVICE_DEFAULT_DARK);

        a.setTitle(m);

        a.setCancelable(true);

        a.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                toaster(c, b ? "Suppression annulée" : "Super, plus on est de fous plus on rit !", 0);
            }
        });

        return a;
    }

    /**
     * Convertis une valeur en pixels vers densitypixels (dp)
     * Utilisé pour la création de vues via code java
     *
     * @param c Contexte de l'application
     * @param v Valeur en pixels à convertir
     * @return Retourne une valeur en dp à utiliser pour les mesures des vues
     */
    static public int dpCalc(Context c, int v) {
        DisplayMetrics dm = c.getResources().getDisplayMetrics();

        v = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                v,
                dm
        );

        return v;
    }

    /**
     * Vérifie si une date est au bon format
     *
     * @param format Format voulu de la date
     * @param value  Date à vérifier
     * @return true ou false
     * true = format valide
     * false = format non valide
     */
    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    /**
     * Crée un TextView servant de titre pour une toolbar, permet l'usage d'une font personnalisé
     *
     * @param c     Contexte de l'activité
     * @param ab    La toolbar à modifier, utiliser getSupportActionBar() si vous avez un objet de type Toolbar
     * @param title Titre souhaité pour la toolbar
     */
    public static void makeToolbarTitle(Context c, ActionBar ab, String title) {
        TextView t = new TextView(c);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        t.setLayoutParams(lp);
        t.setText(title);
        t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        t.setTypeface(Typeface.SANS_SERIF);

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(t);
    }

    /**
     * Permet de changer l'apparence des onglets d'un TabLayout avec une vue personnalisée
     * (définie dans tab_title.xml)
     *
     * @param c     Contexte de l'activitée
     * @param tl    TabLayout à personnaliser
     * @param place Mettre true si utilisé sur AccueilActivity, false si utilisé dans ReunionDetailsActivity
     */
    public static void makeTabTitle(Context c, TabLayout tl, boolean place) {
        int iconColor = ContextCompat.getColor(c, R.color.textWhite);
        for (int i = 0; i < tl.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(c).inflate(R.layout.tab_title, null);
            switch (i) {
                case 0:
                    if (place)
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_event, 0, 0);
                    else
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_reunion_details, 0, 0);
                    break;
                case 1:
                    if (place)
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_reunion_details, 0, 0);
                    else
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_commentaires, 0, 0);
                    break;
                case 2:
                    if (place)
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_historique, 0, 0);
                    else
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_star, 0, 0);
                    break;
            }
            tv.setCompoundDrawablePadding(dpCalc(c, 2));
            tv.getCompoundDrawables()[1].setColorFilter(iconColor, PorterDuff.Mode.SRC_IN);
            tv.setTypeface(Typeface.SANS_SERIF);
            tl.getTabAt(i).setCustomView(tv);
        }
    }

    /**
     * Permet de créer une vue texte pour la création de vues
     *
     * @param c            Contexte de l'application
     * @param text         Texte de la vue
     * @param marginLeft   Marge à gauche
     * @param marginTop    Marge au dessus
     * @param marginRight  Marge à droite
     * @param marginBottom Marge en bas
     * @param size         Taille du texte en SP
     * @param color        Couleur du texte (ID de la couleur)
     * @param id           ID du texte si il faut le placer d'une certaine manière. Indiquer -1 si il n'y a pas besoin d'ID
     * @param above        Indiquer ici l'ID de la vue où la vue texte créée doit se placer au dessus de
     * @param right        Si la vue texte doit être à droite de la vue parent
     * @param width        Largeur de la vue, pour que celle-ci défile si le texte est trop long
     * @return Retourne une vue texte
     */
    public static TextView makeText(Context c, String text, int marginLeft, int marginTop, int marginRight, int marginBottom, int size, int color, int id, int above, boolean right, int width) {
        TextView out = new TextView(c);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                width,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);

        if (above != -1) {
            params.addRule(RelativeLayout.ABOVE, above);
        }

        if (right) {
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }

        out.setLayoutParams(params);
        out.setText(text);
        out.setTextColor(color);
        out.setTextSize(size);
        if (id != -1)
            out.setId(id);
        out.setTypeface(Typeface.SERIF);

        return out;
    }

    /**
     * Retourne une liste de vues Card de réunions prête à être affichée
     *
     * @param reunion Liste de réunions en historique
     * @return Retourne une liste de vues contenant ces réunions
     */
    public static List<MyCardView> makeReunionCard(final Context c, final ActivityFonction activityFonction, final List<Reunion> reunion) {
        List<MyCardView> listCards = new ArrayList<>();
        for (int i = 0; i < reunion.size(); i++) {
            final int index = i;
            int margins = dpCalc(c, 8);
            int cardHeight = dpCalc(c, 248);
            int cardRadius = dpCalc(c, 4);
            int cardElevation = dpCalc(c, 2);

            MyCardView item = new MyCardView(c);
            MyCardView.LayoutParams cardParams = new MyCardView.LayoutParams(
                    MyCardView.LayoutParams.MATCH_PARENT,
                    cardHeight
            );
            cardParams.setMargins(margins, 0, margins, margins);
            item.setLayoutParams(cardParams);
            item.setRadius(cardRadius);
            item.setElevation(cardElevation);
            item.setBackgroundColor(c.getResources().getColor(R.color.colorPrimaryNight));

            ImageView reuPhoto = new ImageView(c);
            LinearLayout.LayoutParams reuPhotoParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );
            reuPhoto.setLayoutParams(reuPhotoParams);
            reuPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
            if (reunion.get(i).photo_reunion != null && !reunion.get(i).photo_reunion.equals("")) {
                CSystem.AfficherPhoto(reunion.get(i).photo_reunion, reuPhoto, true);
            } else {
                reuPhoto.setImageDrawable(c.getResources().getDrawable(R.drawable.reudefault));
            }

            RelativeLayout desc = new RelativeLayout(c);
            RelativeLayout.LayoutParams descParams = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );
            desc.setLayoutParams(descParams);
            desc.setGravity(Gravity.BOTTOM);
            desc.setBackground(c.getResources().getDrawable(R.drawable.card_gradient));

            TextView local = makeText(c, reunion.get(i).lieu_reunion, dpCalc(c, 8), 0, dpCalc(c, 8), 0, 14, c.getResources().getColor(R.color.textSecondaryAlphaWhite), 1, -1, false, dpCalc(c, 190));
            local.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            local.setHorizontalFadingEdgeEnabled(true);
            local.setMarqueeRepeatLimit(99999);
            local.setSingleLine(true);
            local.setSelected(true);
            local.setTypeface(Typeface.SANS_SERIF);
            desc.addView(local);

            String datePattern = "dd/MM/yyyy";
            String timePattern = "HH:mm";

            try {
                Date dateObj = new SimpleDateFormat("yyyy-MM-dd").parse(reunion.get(i).date_reunion);
                String date = new SimpleDateFormat(datePattern).format(dateObj);

                Date timeObj = new SimpleDateFormat("HH:mm:ss").parse(reunion.get(i).heure_reunion);
                String time = new SimpleDateFormat(timePattern).format(timeObj);

                TextView dateHeure = makeText(c, date + " à " + time, 0, 0, dpCalc(c, 8), dpCalc(c, 8), 14, c.getResources().getColor(R.color.textSecondaryAlphaWhite), 2, -1, true, ViewGroup.LayoutParams.WRAP_CONTENT);
                dateHeure.setTypeface(Typeface.SANS_SERIF);
                desc.addView(dateHeure);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            TextView sujet = makeText(c, reunion.get(i).objet_reunion, dpCalc(c, 8), dpCalc(c, 8), 0, 0, 16, c.getResources().getColor(R.color.textWhite), 3, 1, false, dpCalc(c, 260));
            sujet.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            sujet.setHorizontalFadingEdgeEnabled(true);
            sujet.setMarqueeRepeatLimit(99999);
            sujet.setSingleLine(true);
            sujet.setSelected(true);
            desc.addView(sujet);

            TextView nbPlaces = makeText(c, reunion.get(i).nbPlace_reunion + " places", 0, dpCalc(c, 8), dpCalc(c, 8), 0, 16, c.getResources().getColor(R.color.textWhite), 4, 2, true, ViewGroup.LayoutParams.WRAP_CONTENT);
            desc.addView(nbPlaces);

            item.addView(reuPhoto);
            item.addView(desc);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activityFonction.sys.reunionSelected = reunion.get(index);
                    activityFonction.sys.status = -1;
                    activityFonction.ChangeActivity(activityFonction, ReunionDetailsActivity.class);
                }
            });

            listCards.add(item);
        }

        return listCards;
    }
}
