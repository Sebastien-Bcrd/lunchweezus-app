package team.iut.lunchweezus.ui.Invites;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import team.iut.lunchweezus.CreerReunionActivity;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.Invites.Adapter.inviterAdapter_Saved;

public class InviterListeSaved extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.activity_inviter_liste_saved,container,false);

        CreerReunionActivity reunionActivity = (CreerReunionActivity) getActivity();

        RecyclerView recyListeSaved = v.findViewById(R.id.RecyListeSaved);

        if (reunionActivity.profils_saved !=null && reunionActivity.profils_saved.size()>0){
            inviterAdapter_Saved adapter_saved = new inviterAdapter_Saved(reunionActivity.profils_saved, (CreerReunionActivity) getActivity());

            recyListeSaved.setLayoutManager(new LinearLayoutManager(reunionActivity));
            recyListeSaved.setAdapter(adapter_saved);
        }else{
            recyListeSaved.setVisibility(View.GONE);
        }

        return v;
    }

}
