package team.iut.lunchweezus.ui.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import team.iut.lunchweezus.AccueilActivity;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private LinearLayoutManager lin;
    private RecyclerView recyAccueil;
    public int position = 0;
    public  AccueilActivity accueilActivity;


    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        fragment.position = index;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void AfficherListReunion(List<Reunion> reunions)
    {
        final ReunionnAdapter adapter;
        adapter = new ReunionnAdapter(reunions, (AccueilActivity) getActivity());
        recyAccueil.setAdapter(adapter);
        recyAccueil.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_accueil, container, false);

        recyAccueil = root.findViewById(R.id.recyAccueil);
        recyAccueil.setLayoutManager(new LinearLayoutManager(getActivity()));

        accueilActivity = (AccueilActivity) root.getContext();

        if(position == 0) {
            if (accueilActivity.reunionsAvenir != null) {
                AfficherListReunion(accueilActivity.reunionsAvenir);
            }
        }
        if(position == 1) {
            if (accueilActivity.reunionsAvenir != null) {
                if(accueilActivity.reunionsAvenir.size() < 4) {
                    AfficherListReunion(accueilActivity.reunionsAvenir);
                }
                else
                {
                    List<Reunion> listFictive = new ArrayList<Reunion>(accueilActivity.reunionsAvenir);
                    List<Reunion> reuAleatoire = new ArrayList<Reunion>();
                    for(int i = 0; i < 3; i++)
                    {
                        int chiffreAleatoire = new Random().nextInt(listFictive.size());
                        reuAleatoire.add(listFictive.get(chiffreAleatoire));
                        listFictive.remove(chiffreAleatoire);
                    }
                    AfficherListReunion(reuAleatoire);
                }
            }
        }
        if(position == 2) {
            if (accueilActivity.reunionsPasser != null) {
                AfficherListReunion(accueilActivity.reunionsPasser);
            }
        }

        final SwipeRefreshLayout refresh = root.findViewById(R.id.accueil_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                accueilActivity.recupererAllReunion();
                refresh.setRefreshing(false);
            }
        });

        return root;
    }

}