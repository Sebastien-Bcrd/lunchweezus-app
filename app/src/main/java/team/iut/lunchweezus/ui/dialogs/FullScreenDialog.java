package team.iut.lunchweezus.ui.dialogs;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;

public class FullScreenDialog extends DialogFragment {

    public static String TAG = "FullScreenDialog";
    private String img;
    private ImageView imgV;
    private byte[] byteArray;
    private Bitmap bmp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        try {
            byteArray = getArguments().getByteArray("image");
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.photo_profil_fullscreen_modal, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_fullscreen_profil);
        toolbar.setNavigationIcon(R.drawable.ic_croix);
        toolbar.getNavigationIcon().setTint(getResources().getColor(R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        imgV = view.findViewById(R.id.photo_profil_fullscreen);

        imgV.setImageBitmap(bmp);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
