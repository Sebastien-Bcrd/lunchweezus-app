package team.iut.lunchweezus.ui.notifications;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import team.iut.lunchweezus.R;

public class NotificationsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NotificationsModel> modelArrayList;

    public NotificationsAdapter(Context context, ArrayList<NotificationsModel> modelArrayList) {
        this.context = context;
        this.modelArrayList = modelArrayList;
    }

    public void remove(int position) {
        int notificationId = modelArrayList.get(position).getNotificationId();
        modelArrayList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.conteneur_notification, null, true);

            holder.view = convertView.findViewById(R.id.notif_elm);
            holder.tvtitle = convertView.findViewById(R.id.notif_title);
            holder.tvcontent = convertView.findViewById(R.id.notif_content);
            holder.tvtimestamp = convertView.findViewById(R.id.notif_timestamp);
            holder.iv = convertView.findViewById(R.id.notif_icn);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (modelArrayList.get(position).getStatus() == 1) {
            holder.view.setAlpha(0.5f);
        }

        holder.tvtitle.setTypeface(Typeface.SERIF);
        holder.tvtitle.setText(modelArrayList.get(position).getTitle());

        holder.tvcontent.setText(modelArrayList.get(position).getContent());
        holder.tvcontent.setTypeface(Typeface.SERIF);

        holder.tvtimestamp.setText(modelArrayList.get(position).getTimestamp());
        holder.tvtimestamp.setTypeface(Typeface.SANS_SERIF);

        holder.iv.setImageDrawable(modelArrayList.get(position).getImageDrawable());

        return convertView;
    }

    private class ViewHolder {

        private LinearLayout view;
        protected TextView tvtitle, tvcontent, tvtimestamp;
        private ImageView iv;

    }

}
