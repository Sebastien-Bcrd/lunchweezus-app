package team.iut.lunchweezus.ui.inviter_sur_reu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.InviterSurReuActivity;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.utils;

public class AdapterInviterSurReu extends ArrayAdapter<Profil> {

    private List<Profil> profils;
    private InviterSurReuActivity inviterSurReuActivity;

    public AdapterInviterSurReu(Context context, List<Profil> values) {
        super(context, R.layout.conteneur_inviter, values);
        profils = values;
        inviterSurReuActivity = (InviterSurReuActivity) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater theInflater = LayoutInflater.from(getContext());

        View theView = theInflater.inflate(R.layout.conteneur_inviter, parent, false);

        LinearLayout.LayoutParams tvp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                utils.dpCalc(getContext(), 48)
        );
        theView.setLayoutParams(tvp);
        theView.setPadding(0, utils.dpCalc(getContext(), 16), 0, 0);

        final Profil profil = profils.get(position);

        TextView nom = theView.findViewById(R.id.txtNom_Inviter);
        ImageView photoProfil = theView.findViewById(R.id.img_inviter);
        final CheckBox checkBox = theView.findViewById(R.id.check_inviter);

        CSystem.AfficherPhoto(profil.photo_profil,photoProfil,false);
        nom.setText(profil.nom_profil+" "+profil.prenom_profil);

        for(int i = 0; i < inviterSurReuActivity.profils_saved.size(); i++) {
            if(profil.id_profil == inviterSurReuActivity.profils_saved.get(i).id_profil) {
                checkBox.setChecked(true);
            }
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked())
                {
                    inviterSurReuActivity.profils_saved.add(profil);
                }else {
                    for(int i = 0; i < inviterSurReuActivity.profils_saved.size(); i++) {
                        if(profil.id_profil == inviterSurReuActivity.profils_saved.get(i).id_profil) {
                            inviterSurReuActivity.profils_saved.remove(i);
                        }
                    }
                }
            }
        });

        return theView;
    }
}
