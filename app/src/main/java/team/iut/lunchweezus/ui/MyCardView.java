package team.iut.lunchweezus.ui;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;

public class MyCardView extends CardView {
    public MyCardView(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setBackgroundColor(@ColorInt int backgroundColor) {
        setCardBackgroundColor(backgroundColor);
    }
}
