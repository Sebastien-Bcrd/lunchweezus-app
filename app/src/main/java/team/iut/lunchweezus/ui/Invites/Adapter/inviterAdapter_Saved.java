package team.iut.lunchweezus.ui.Invites.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.CreerReunionActivity;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;

public class inviterAdapter_Saved extends RecyclerView.Adapter<inviterAdapter_Saved.inviterViewHolder> {

    private ArrayList<Profil> profils_saved;
    private CreerReunionActivity creerReunionActivity;

    public inviterAdapter_Saved(ArrayList<Profil> profils_saved, CreerReunionActivity crA) {
        this.profils_saved = profils_saved;
        this.creerReunionActivity = crA;
    }

    @NonNull
    @Override
    public inviterAdapter_Saved.inviterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new inviterAdapter_Saved.inviterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteneur_inviter_seved, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final inviterViewHolder holder, final int position) {

        holder.txtNom_Inviter_saved.setText(profils_saved.get(position).getNom_profil());
        CSystem.AfficherPhoto(profils_saved.get(position).photo_profil, holder.img_inviter_saved, false);
        holder.check_inviter_saved.setChecked(true);
        holder.check_inviter_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profils_saved.remove(position);
                creerReunionActivity.creerListeSaved();
            }
        });
    }


    @Override
    public int getItemCount() {
        return profils_saved.size();
    }

    class inviterViewHolder extends RecyclerView.ViewHolder{

        ImageView img_inviter_saved;
        TextView txtNom_Inviter_saved;
        CheckBox check_inviter_saved;

        public inviterViewHolder(View itemView) {
            super(itemView);
            img_inviter_saved = itemView.findViewById(R.id.img_inviter_saved);
            txtNom_Inviter_saved = itemView.findViewById(R.id.txtNom_Inviter_saved);
            check_inviter_saved = itemView.findViewById(R.id.check_inviter_saved);
        }
    }
}
