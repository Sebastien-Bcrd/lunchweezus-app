package team.iut.lunchweezus.ui.Invites.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.CreerReunionActivity;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;

public class inviterAdapter extends RecyclerView.Adapter<inviterAdapter.inviterViewHolder> {


    private CreerReunionActivity reunionActivity;
    private ArrayList<Profil> profils;
    private ArrayList<Profil> profils_saved;

    public inviterAdapter(CreerReunionActivity reunionActivity, ArrayList<Profil> profils, ArrayList<Profil> profils_saved) {
        this.reunionActivity = reunionActivity;
        this.profils = profils;
        this.profils_saved = profils_saved;
    }

    @Override
    public inviterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new inviterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.conteneur_inviter, parent, false));
    }

    @Override
    public void onBindViewHolder(inviterViewHolder holder, final int position) {
        final Profil data = profils.get(position);
        CSystem.AfficherPhoto(data.photo_profil, holder.img_inviter, false);
        holder.txtNom_Inviter.setText(data.getNom_profil()+" "+data.getPrenom_profil());

        for (int i = 0;i < profils_saved.size();i++){
            if (profils_saved.get(i).getNom_profil().equals(profils.get(position).getNom_profil()))
                holder.check_inviter.setChecked(true);
        }

        holder.check_inviter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox)view).isChecked())
                    reunionActivity.profils_saved.add(profils.get(position));
                else{
                    for (int i = 0;i < profils_saved.size();i++){
                        if (profils_saved.get(i).getNom_profil().equals(profils.get(position).getNom_profil()))
                            reunionActivity.profils_saved.remove(i);
                    }
                }
            }
        });


    }
    @Override
    public int getItemCount() {
        return profils.size();
    }

    class inviterViewHolder extends RecyclerView.ViewHolder{

        ImageView img_inviter;
        TextView txtNom_Inviter;
        CheckBox check_inviter;

        public inviterViewHolder(View itemView) {
            super(itemView);
            img_inviter = itemView.findViewById(R.id.img_inviter);
            txtNom_Inviter = itemView.findViewById(R.id.txtNom_Inviter);
            check_inviter = itemView.findViewById(R.id.check_inviter);
        }
    }
}
