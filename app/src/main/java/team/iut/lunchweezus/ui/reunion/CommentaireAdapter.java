package team.iut.lunchweezus.ui.reunion;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import team.iut.lunchweezus.ActivityFonction;
import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.R;
import team.iut.lunchweezus.ui.MyCardView;
import team.iut.lunchweezus.ui.utils;

public class CommentaireAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int TYPE_ME = 1, TYPE_OTHER = 0;
    private Context context;
    private ArrayList<Commentaire> commentaireArrayList;
    private int mProfileId;

    public CommentaireAdapter(Context context, ArrayList<Commentaire> commentaireArrayList, int mProfileId) {
        this.context = context;
        this.commentaireArrayList = commentaireArrayList;
        this.mProfileId = mProfileId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType) {
        View view;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (ViewType == TYPE_ME) {
            view = inflater.inflate(R.layout.conteneur_mon_commentaire, null, true);

            return new CommentMe(view);
        } else {
            view = inflater.inflate(R.layout.conteneur_commentaire, null, true);

            return new CommentOther(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (commentaireArrayList.get(position).getId_profil() == mProfileId) {
            return TYPE_ME;
        } else {
            return TYPE_OTHER;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == TYPE_ME) {
            ((CommentMe) viewHolder).setComm(commentaireArrayList.get(position));
        } else {
            ((CommentOther) viewHolder).setComm(commentaireArrayList.get(position));
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return commentaireArrayList.size();
    }

    private class CommentMe extends RecyclerView.ViewHolder {

        private TextView ctitle, ccontent;
        private ImageView iv;
        private int profileId;

        public CommentMe(View itemView) {
            super(itemView);
            ConstraintLayout.LayoutParams cl = new ConstraintLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            cl.setMargins(utils.dpCalc(context, 8), utils.dpCalc(context, 8), utils.dpCalc(context, 8), 0);
            itemView.setLayoutParams(cl);
            ctitle = itemView.findViewById(R.id.title_mon_comm);
            ccontent = itemView.findViewById(R.id.content_mon_comm);
            iv = itemView.findViewById(R.id.photo_mon_comm);
        }

        public void setComm(Commentaire comment) {
            String dateStr = comment.date_commentaire + " " + comment.heure_commentaire;

            String datePattern = "dd/MM/yyyy";
            String timePattern = "HH:mm";

            try {
                Date dateCom;

                SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (utils.isValidFormat("dd-MM-yyyy HH:mm:ss", dateStr))
                    dateCom = fmt.parse(dateStr);
                else
                    dateCom = fmt2.parse(dateStr);

                dateStr = new SimpleDateFormat(datePattern).format(dateCom) + " à " + new SimpleDateFormat(timePattern).format(dateCom);

                ctitle.setText(comment.prenom_profil + " - " + dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            ccontent.setText(comment.getContenu_commentaire());

            profileId = comment.getId_profil();

            CSystem.AfficherPhoto(profileId + ".jpg", iv, false);
        }
    }

    private class CommentOther extends RecyclerView.ViewHolder {

        private TextView ctitle, ccontent;
        private ImageView iv;
        private int profileId;

        public CommentOther(View itemView) {
            super(itemView);
            ConstraintLayout.LayoutParams cl = new ConstraintLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            cl.setMargins(utils.dpCalc(context, 8), utils.dpCalc(context, 8), utils.dpCalc(context, 8), 0);
            itemView.setLayoutParams(cl);
            ctitle = itemView.findViewById(R.id.title_comm);
            ccontent = itemView.findViewById(R.id.content_comm);
            iv = itemView.findViewById(R.id.photo_comm);
        }

        public void setComm(Commentaire comment) {
            String dateStr = comment.date_commentaire + " " + comment.heure_commentaire;

            String datePattern = "dd/MM/yyyy";
            String timePattern = "HH:mm";

            try {
                Date dateCom;

                SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (utils.isValidFormat("dd-MM-yyyy HH:mm:ss", dateStr))
                    dateCom = fmt.parse(dateStr);
                else
                    dateCom = fmt2.parse(dateStr);

                dateStr = new SimpleDateFormat(datePattern).format(dateCom) + " à " + new SimpleDateFormat(timePattern).format(dateCom);

                ctitle.setText(comment.prenom_profil + " - " + dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            ccontent.setText(comment.getContenu_commentaire());

            profileId = comment.getId_profil();

            CSystem.AfficherPhoto(profileId + ".jpg", iv, false);
        }
    }
}
