package team.iut.lunchweezus.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;

public class ParticipantDataSource {

    CSqlite dbHelper;
    private SQLiteDatabase database;
    private String[] allColumns = {"id_profil", "id_reunion"};

    public ParticipantDataSource(Context context) {
        dbHelper = new CSqlite(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        //dbHelper.close();
    }

    /// Permet d'inserer un Participant dans une réunion
    public long insertParticipant(Profil profil, Reunion reunion) {

        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put("id_profil", profil.getId_profil());
        values.put("id_reunion", reunion.getId());

        long id = -1;

        try {
            open();
            //on insère l'objet dans la BDD via le ContentValues
            id = database.insert("participant", null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /// Permet de récupérer toutes les réunions par rapport à un profil
    public List<Reunion> getAllReunionsWithProfil(Profil profil, ReunionDataSource reunionDataSource) {
        List<Reunion> reunions = new ArrayList<Reunion>();
        long id = profil.getId_profil();

        try {
            open(); // open bdd
            Cursor cursor = database.query("participant",
                    allColumns, "id_profil=" + id, null, null, null, null);


            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                int identifiant = cursor.getInt(1);
                Reunion reunion = reunionDataSource.getReunionById(identifiant);
                reunions.add(reunion);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return reunions;
    }

    /// Permet de récupérer toutes les Profils par rapport à une Reunion
    public List<Profil> getAllProfilsWithReunion(Reunion reunion, ProfilDataSource profilDataSource) {
        List<Profil> profils = new ArrayList<Profil>();
        long id = reunion.getId();

        try {
            open(); // open bdd
            Cursor cursor = database.query("participant",
                    allColumns, "id_reunion=" + id, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                int identifiant = cursor.getInt(0);
                Profil profil = profilDataSource.getProfilById(identifiant);
                profils.add(profil);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd


        return profils;
    }

    /// Permet de supprimer un participant
    public void DeleteParticipant(Profil profil, Reunion reunion) {
        long id_profil = profil.getId_profil();
        long id_reunion = reunion.getId();
        try {
            open();
            database.delete("participant", "id_profil=? AND id_reunion=? ", new String[]{"" + id_profil, "" + id_reunion});
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /// Permet de supprimer tous les profils d'une réunion
    public void DeleteWithReunion(Reunion reunion) {
        try {
            open();
            database.delete("participant", "id_reunion=" + reunion.getId(), null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
