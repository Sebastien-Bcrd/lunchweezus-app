package team.iut.lunchweezus.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;

public class ProfilDataSource {

    CSqlite dbHelper;
    private SQLiteDatabase database;
    private String[] allColumns = {"id_profil",
            "nom_profil", "prenom_profil", "fonction_profil", "photo_profil", "mail_profil", "matricule_profil", "gps_site", "libelle_site", "id_site"};

    public ProfilDataSource(Context context) {
        dbHelper = new CSqlite(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
       // dbHelper.close();
    }

    public long insertProfil(Profil profil) {
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put("id_profil", profil.getId_profil());
        values.put("nom_profil", profil.getNom_profil());
        values.put("prenom_profil", profil.getPrenom_profil());
        values.put("fonction_profil", profil.getFonction_profil());
        values.put("photo_profil", profil.getPhoto_profil());
        values.put("mail_profil", profil.getMail_profil());
        values.put("matricule_profil", profil.getMatricule_profil());
        values.put("gps_site", profil.getGps_site());
        values.put("libelle_site", profil.getLibelle_site());
        values.put("id_site", profil.getId_site());

        long id = -1;

        try {
            open();
            //on insère l'objet dans la BDD via le ContentValues
            id = database.insert("profil", null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public List<Profil> getAllProfils() {
        List<Profil> profils = new ArrayList<Profil>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("profil",
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Profil profil = cursorToProfil(cursor);
                profils.add(profil);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return profils;
    }

    public void deleteProfil(Profil profil) {
        if(profil!=null) {
            long id = profil.getId_profil();
            try {
                open();
                database.delete("profil", "id_profil=" + id, null);
                close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    /// Permet de récupérer un profil par rapport à un id
    public Profil getProfilById(int id) {
        List<Profil> profils = new ArrayList<Profil>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("profil",
                    allColumns, "id_profil=" + id, null, null, null, null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                Profil profil = cursorToProfil(cursor);
                profils.add(profil);
            }

            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        Profil prfl = null; // Contient le résultat

        if (!profils.isEmpty()) prfl = profils.get(0); // récupére la réunion

        return prfl;
    }

    public List<Profil> getProfilsWithString(String filtre) {
        List<Profil> profils = new ArrayList<Profil>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("profil",
                    allColumns, "nom_profil LIKE '" + filtre + "%' OR prenom_profil LIKE '" + filtre + "%'", null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Profil profil = cursorToProfil(cursor);
                profils.add(profil);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return profils;
    }

    private Profil cursorToProfil(Cursor cursor) {
        Profil profil = new Profil();
        profil.setId_profil(cursor.getInt(0));
        profil.setNom_profil(cursor.getString(1));
        profil.setPrenom_profil(cursor.getString(2));
        profil.setFonction_profil(cursor.getString(3));
        profil.setPhoto_profil(cursor.getString(4));
        profil.setGps_site(cursor.getString(7));
        profil.setLibelle_site(cursor.getString(8));
        profil.setId_site(cursor.getInt(9));
        return profil;
    }

    public void DeleteAllProfil() {
        try {
            open();
            database.delete("profil", "id_profil != 0", null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
