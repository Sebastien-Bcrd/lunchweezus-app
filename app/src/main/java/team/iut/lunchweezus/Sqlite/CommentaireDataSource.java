package team.iut.lunchweezus.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Beans.Reunion;

public class CommentaireDataSource {

    CSqlite dbHelper;
    private SQLiteDatabase database;
    private String[] allColumns = {"id_commentaire", "contenu_commentaire", "date_commentaire", "heure_commentaire", "nom_profil", "prenom_profil", "id_profil", "id_reunion"};

    public CommentaireDataSource(Context context) {
        dbHelper = new CSqlite(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        //dbHelper.close();
    }

    /// Permet d'inserer un Participant dans une réunion
    public long insertCommentaire(Commentaire commentaire, Reunion reunion) {

        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put("id_commentaire", commentaire.getId_commentaire());
        values.put("contenu_commentaire", commentaire.getContenu_commentaire());
        values.put("date_commentaire", commentaire.getDate());
        values.put("heure_commentaire", commentaire.getHeure());
        values.put("nom_profil", commentaire.nom_profil);
        values.put("prenom_profil", commentaire.prenom_profil);
        values.put("id_profil", commentaire.getId_profil());
        values.put("id_reunion", reunion.getId());

        long id = -1;

        try {
            open();
            //on insère l'objet dans la BDD via le ContentValues
            id = database.insert("commentaire", null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /// Permet de récupérer tout les commentaire d'une réunion
    public List<Commentaire> getCommentairesWithreunion(Reunion reunion) {
        List<Commentaire> commentaires = new ArrayList<Commentaire>();
        long id = reunion.getId();

        try {
            open(); // open bdd
            Cursor cursor = database.query("commentaire",
                    allColumns, "id_reunion=" + id, null, null, null, null);


            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Commentaire commentaire = cursorToCommentaire(cursor);
                commentaires.add(commentaire);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return commentaires;
    }

    public void deleteCommentaire(Commentaire commentaire) {
        long id = commentaire.getId_commentaire();
        try {
            open();
            database.delete("commentaire", "id_commentaire=" + id, null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //TODO :
    private Commentaire cursorToCommentaire(Cursor c) {
        Commentaire commentaire = new Commentaire();
        commentaire.setId_commentaire(c.getInt(0));
        commentaire.setContenu_commentaire(c.getString(1));
        commentaire.setDate(c.getString(2));
        commentaire.setHeure(c.getString(3));
        commentaire.setNom_profil(c.getString(4));
        commentaire.setPrenom_profil(c.getString(5));
        commentaire.setId_profil(c.getInt(6));
        return commentaire;
    }
}
