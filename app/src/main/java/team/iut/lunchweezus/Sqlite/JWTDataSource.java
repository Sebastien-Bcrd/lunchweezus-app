package team.iut.lunchweezus.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

public class JWTDataSource {

    CSqlite dbHelper;
    private SQLiteDatabase database;
    private String[] allColumns = {"id", "jwt"};

    public JWTDataSource(Context context) {
        dbHelper = new CSqlite(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        //dbHelper.close();
    }

    /// Permet d'inserer le jwt
    public long insertJWT(String jwt) {
        DeleteJWT();
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put("id", 0);
        values.put("jwt", jwt);

        long id = -1;

        try {
            open();
            //on insère l'objet dans la BDD via le ContentValues
            id = database.insert("jwt", null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /// Permet de récupérer le jwt
    public String getJWT() {
        String resultat = null;
        try {
            open(); // open bdd
            Cursor cursor = database.query("jwt", allColumns, "id=0", null, null, null, null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                resultat = cursor.getString(1);
            }

            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return resultat;
        }
        close(); // close bdd

        return resultat;
    }

    public void DeleteJWT() {
        try {
            open();
            database.delete("jwt", "id=0", null);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
