package team.iut.lunchweezus.Sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CSqlite extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    private static final String DATABASE_NAME = "lunchWeezUs.db";
    private static final int DATABASE_VERSION = 1;

    public CSqlite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        /// Creation de la table site
        database.execSQL("CREATE TABLE site (id_site INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "libelle_site VARCHAR (64),\n" +
                "adresse_site VARCHAR (128))");

        /// Creation de la table profil
        database.execSQL("CREATE TABLE profil (id_profil INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "nom_profil VARCHAR (32),\n" +
                "prenom_profil VARCHAR (32),\n" +
                "fonction_profil VARCHAR (64),\n" +
                "photo_profil VARCHAR (64),\n" +
                "mail_profil VARCHAR (64),\n" +
                "matricule_profil VARCHAR (16),\n" +
                "gps_site VARCHAR (64),\n" +
                "libelle_site VARCHAR (64),\n" +
                "id_site BIGINT (3))");

        /// Creation de la table reunion
        database.execSQL("CREATE TABLE reunion (id_reunion INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "objet_reunion VARCHAR (64),\n" +
                "heure_reunion TIME (4),\n" +
                "date_reunion DATE,\n" +
                "adresse_reunion VARCHAR (128),\n" +
                "lieu_reunion VARCHAR (64),\n" +
                "nbPlace_reunion INT (3),\n" +
                "photo_reunion VARCHAR (64),\n" +
                "duree_reunion VARCHAR (32),\n" +
                "valid_reunion BOOLEAN,\n" +
                "id_profil BIGINT (3))");

        /// Creation de la table association participant
        database.execSQL("CREATE TABLE participant (id_profil INTEGER NOT NULL,\n" +
                "id_reunion INTEGER NOT NULL,\n" +
                "FOREIGN KEY (id_profil) REFERENCES profil(id_profil),\n" +
                "FOREIGN KEY (id_reunion) REFERENCES reunion(id_reunion),\n" +
                "PRIMARY KEY (id_profil,\n" +
                " id_reunion))");

        /// Creation de la table association inviter
        database.execSQL("CREATE TABLE inviter (id_reunion INTEGER NOT NULL,\n" +
                "id_profil INTERGER NOT NULL,\n" +
                "FOREIGN KEY (id_profil) REFERENCES profil(id_profil),\n" +
                "FOREIGN KEY (id_reunion) REFERENCES reunion(id_reunion),\n" +
                "PRIMARY KEY (id_reunion,\n" +
                " id_profil))");

        /// Creation de la table association inviter
        database.execSQL("CREATE TABLE jwt (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "jwt VARCHAR (512) NOT NULL)");

        /// Creation de la table commentaire
        database.execSQL("CREATE TABLE commentaire (id_commentaire INTEGER NOT NULL,\n" +
                "contenu_commentaire VARCHAR (256),\n" +
                "date_commentaire VARCHAR (32),\n" +
                "heure_commentaire VARCHAR (32),\n" +
                "nom_profil VARCHAR (32),\n" +
                "prenom_profil VARCHAR (32),\n" +
                "id_profil INTEGER,\n" +
                "id_reunion INTEGER,\n" +
                "FOREIGN KEY (id_reunion) REFERENCES reunion(id_reunion),\n" +
                "FOREIGN KEY (id_profil) REFERENCES profil(id_profil),\n" +
                "PRIMARY KEY (id_commentaire))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
