package team.iut.lunchweezus.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;

public class ReunionDataSource {

    CSqlite dbHelper;
    private SQLiteDatabase database;
    private String[] allColumns = { "id_reunion",
            "objet_reunion", "heure_reunion", "date_reunion, adresse_reunion, lieu_reunion, nbPlace_reunion," +
            " photo_reunion, duree_reunion, valid_reunion, id_profil" };

    public ReunionDataSource(Context context)
    {
        dbHelper = new CSqlite(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
       // dbHelper.close();
    }

    /// Permet d'inserer une réunion
    public long insertReunion(Reunion reunion) {

        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put("id_reunion", reunion.getId());
        values.put("objet_reunion", reunion.getObjet_reunion());
        values.put("heure_reunion", reunion.getHeure_reunion());
        values.put("date_reunion", reunion.getDate_reunion());
        values.put("adresse_reunion", reunion.getAdresse_reunion());
        values.put("lieu_reunion", reunion.getLieu_reunion());
        values.put("nbPlace_reunion", reunion.getNbPlace_reunion());
        values.put("photo_reunion", reunion.getPhoto_reunion());
        values.put("duree_reunion", reunion.getDuree_reunion());
     //   values.put("valid_reunion", reunion.getValid_reunion());
        values.put("id_profil", reunion.getId_profil());

        long id = -1;

        try {
            open();
            //on insère l'objet dans la BDD via le ContentValues
            id = database.insert("reunion", null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    /// Permet de récupérer toutes les réunions dans Sqlite
    public List<Reunion> getAllReunions() {
        List<Reunion> reunions = new ArrayList<Reunion>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("reunion",
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Reunion reunion = cursorToReunion(cursor);
                reunions.add(reunion);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return reunions;
    }

    /// Permet de récupérer toutes les réunions qu'a organisaer un profil dans Sqlite
    public List<Reunion> getReunionsOrganisateur(Profil organisateur) {
        List<Reunion> reunions = new ArrayList<Reunion>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("reunion",
                    allColumns, "id_profil="+organisateur.id_profil, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Reunion reunion = cursorToReunion(cursor);
                reunions.add(reunion);
                cursor.moveToNext();
            }
            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        return reunions;
    }

    /// Permet de supprimer une réunion en particulière par l'id d'une réunion
    public void deleteReunion(Reunion reunion) {
        long id = reunion.getId();
        try {
            open();
            database.delete("reunion", "id_reunion=" + id, null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /// Permet de récupérer une réunion par rapport à un id
    public Reunion getReunionById(int id) {
        List<Reunion> reunions = new ArrayList<Reunion>();

        try {
            open(); // open bdd
            Cursor cursor = database.query("reunion",
                    allColumns, "id_reunion="+id, null, null, null, null);

            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                Reunion reunion = cursorToReunion(cursor);
                reunions.add(reunion);
            }

            // assurez-vous de la fermeture du curseur
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close(); // close bdd

        Reunion reu = null; // Contient le résultat

        if(!reunions.isEmpty()) reu = reunions.get(0); // récupére la réunion

        return reu;
    }

    private Reunion cursorToReunion(Cursor cursor) {
        Reunion reunion = new Reunion();
        reunion.setId(cursor.getInt(0));
        reunion.setObjet_reunion(cursor.getString(1));
        reunion.setHeure_reunion(cursor.getString(2));
        reunion.setDate_reunion(cursor.getString(3));
        reunion.setAdresse_reunion(cursor.getString(4));
        reunion.setLieu_reunion(cursor.getString(5));
        reunion.setNbPlace_reunion(cursor.getInt(6));
        reunion.setPhoto_reunion(cursor.getString(7));
        reunion.setDuree_reunion(cursor.getString(8));
      //  reunion.setValid_reunion(cursor.getInt(9));
        reunion.setId_profil(cursor.getInt(10));
        return reunion;
    }

    /// Permet de supprimer toute les réunion en local
    public void DeleteAllReunion()
    {
        try {
            open();
            database.delete("reunion",null,null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /// Permet de vider les données
    public void DropAllTables()
    {
        try {
            open();
            database.delete("inviter", null, null);
            database.delete("participant", null, null);
            database.delete("commentaire",null,null);
            database.delete("reunion",null,null);
            database.delete("profil",null,null);
            database.delete("site",null,null);
            database.delete("jwt",null,null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
