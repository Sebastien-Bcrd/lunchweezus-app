package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.ui.Invites.Adapter.inviterAdapter;
import team.iut.lunchweezus.ui.inviter_sur_reu.AdapterInviterSurReu;
import team.iut.lunchweezus.ui.utils;

public class InviterSurReuActivity extends ActivityFonction {

    private List<Profil> profils;//garde les données depuis la base de données
    private ListView listViewInvi_reu;
    public FloatingActionButton FABaccepterInviter;
    public EditText search_box;
    public List<Profil> profils_saved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Debut();

        profils_saved = new ArrayList<>();

        setContentView(R.layout.activity_inviter_sur_reu);

        listViewInvi_reu = findViewById(R.id.ListViewInvi_reu);
        search_box = findViewById(R.id.search_box_reu);
        FABaccepterInviter = findViewById(R.id.FABaccepterInviter_reu);
        profils = new ArrayList<>();


        final Toolbar toolbar = findViewById(R.id.inviter_reu_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Inviter");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        sys.getAllProfils();
    }

    public void AfficherProfil(String filtre) {
        List<Profil> profilList = sys.profilDataSource.getProfilsWithString(filtre);

        /// Crée une list participants avec invités confondu
        List<Profil> participants = new ArrayList<>(Arrays.asList(sys.detailReunionSelected.getParticipants()));
        participants.addAll(Arrays.asList(sys.detailReunionSelected.getInviter()));
        participants.add(sys.detailReunionSelected.getOrganisateur()[0]);

        /// Supprime tout les profils déjà participants et invités, ainsi que le profil organisateur
        for (int i = 0; i < profilList.size(); i++) {
            if (profilList.get(i).id_profil == sys.userProfil.id_profil || profilList.get(i).id_profil == 0) {
                profilList.remove(i);
                i--;
            } else {
                for (Profil prfl : participants) {
                    if (i >= 0 && i < profilList.size() && prfl.id_profil == profilList.get(i).id_profil) {
                        profilList.remove(i);
                        i--;
                    }
                }
            }
        }

        final AdapterInviterSurReu adapter = new AdapterInviterSurReu(InviterSurReuActivity.this, profilList);
        listViewInvi_reu.setAdapter(adapter);
    }


    /// Est Appeler quand on récupere tout les profils
    @Override
    public void SuccessGetAllProfil(final List<Profil> listProfils) {

        profils = new ArrayList<>();

        sys.profilDataSource.DeleteAllProfil();
        for (int i = 0; i < listProfils.size(); i++) {
            if (sys.userProfil.id_profil != listProfils.get(i).id_profil) {
                profils.add(listProfils.get(i));
                sys.profilDataSource.deleteProfil(listProfils.get(i));
                sys.profilDataSource.insertProfil(listProfils.get(i));
            }
        }

        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String recherche = search_box.getText().toString();
                if (!recherche.equals("")) AfficherProfil(recherche);
            }
        };

        search_box.addTextChangedListener(fieldValidatorTextWatcher);

        search_box.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String recherche = search_box.getText().toString();
                if (!recherche.equals("")) AfficherProfil(recherche);
                return false;
            }
        });

        search_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String recherche = search_box.getText().toString();
                if (!recherche.equals("")) AfficherProfil(recherche);
                return true;
            }
        });

        FABaccepterInviter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nbPart = sys.detailReunionSelected.getParticipants().length + sys.detailReunionSelected.getInviter().length + profils_saved.size();
                if (nbPart < sys.detailReunionSelected.getReunion()[0].nbPlace_reunion) {
                    /// Création tableau pour encode JSon l'id du profil avec l'id de la réunion
                    int[] idInviter = new int[profils_saved.size()];
                    for (int i = 0; i < profils_saved.size(); i++) {
                        idInviter[i] = profils_saved.get(i).id_profil;
                    }
                    String inviter = new Gson().toJson(idInviter);
                    sys.inviter(inviter);
                } else {
                    utils.toaster(InviterSurReuActivity.this, "Trop d'invité", 0);
                }
            }
        });
    }

    /// Est Appeler quand on ne récupere pas tout les profils
    @Override
    public void ErreurGetAllProfil(final String message) {
    }

    /// Est appler quand on a réussie a ajouter des inviter
    @Override
    public void successInviter(final boolean result) {
        if (result) {
            utils.toaster(InviterSurReuActivity.this, "Bien invité(e)s", 0);
            finish();
        }
    }

    /// Est appler quand on a pas réussie a ajouter des inviter
    @Override
    public void erreurInviter(String message) {
        utils.toaster(InviterSurReuActivity.this, message, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        InviterSurReuActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        InviterSurReuActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        super.onBackPressed();
    }

}
