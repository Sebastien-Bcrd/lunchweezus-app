package team.iut.lunchweezus.Beans.Geoloc;

import java.io.Serializable;

public class DataApi implements Serializable {
    ResponseData response;

    public ResponseData getResponse() {
        return response;
    }

    public void setResponse(ResponseData response) {
        this.response = response;
    }
}
