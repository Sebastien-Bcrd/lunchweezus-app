package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Commentaire implements Serializable {

    public int id_commentaire = 0;
    public String contenu_commentaire = null;
    public int id_profil = 0;
    public String nom_profil;
    public String prenom_profil;
    public int id_reunion;
    public String date_commentaire;
    public String heure_commentaire;

    public String getDate() {
        return date_commentaire;
    }

    public void setDate(String date) {
        this.date_commentaire = date;
    }

    public String getHeure() {
        return heure_commentaire;
    }

    public void setHeure(String heure) {
        this.heure_commentaire = heure;
    }

    public int getId_reunion() {
        return id_reunion;
    }

    public void setId_reunion(int id_reunion) {
        this.id_reunion = id_reunion;
    }

    public String getNom_profil() {
        return nom_profil;
    }

    public void setNom_profil(String nom_profil) {
        this.nom_profil = nom_profil;
    }

    public String getPrenom_profil() {
        return prenom_profil;
    }

    public void setPrenom_profil(String prenom_profil) {
        this.prenom_profil = prenom_profil;
    }

    public int getId_commentaire() {
        return id_commentaire;
    }

    public void setId_commentaire(int id_commentaire) {
        this.id_commentaire = id_commentaire;
    }

    public String getContenu_commentaire() {
        return contenu_commentaire;
    }

    public void setContenu_commentaire(String contenu_commentaire) {
        this.contenu_commentaire = contenu_commentaire;
    }

    public int getId_profil() {
        return id_profil;
    }

    public void setId_profil(int id_profil) {
        this.id_profil = id_profil;
    }
}
