package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Profil implements Serializable {

   public int id_profil = 1;
   public String nom_profil = null;
   public String prenom_profil = null;
   public String mail_profil = null;
   public String fonction_profil = null;
   public String photo_profil = null;
   public String matricule_profil = null;
   public int id_site = 0;
   public String libelle_site= null;
   public String gps_site = null;
   public String oldPassword = null;
   public String newPassword = null;
   public String reponse_profil = null;


    public int getId_profil() {
        return id_profil;
    }

    public void setId_profil(int id_profil) {
        this.id_profil = id_profil;
    }

    public String getNom_profil() {
        return nom_profil;
    }

    public void setNom_profil(String nom_profil) {
        this.nom_profil = nom_profil;
    }

    public String getPrenom_profil() {
        return prenom_profil;
    }

    public void setPrenom_profil(String prenom_profil) {
        this.prenom_profil = prenom_profil;
    }

    public String getFonction_profil() {
        return fonction_profil;
    }

    public void setFonction_profil(String fonction_profil) {
        this.fonction_profil = fonction_profil;
    }

    public String getPhoto_profil() {
        return photo_profil;
    }

    public void setPhoto_profil(String photo_profil) {
        this.photo_profil = photo_profil;
    }

    public String getMail_profil() {
        return mail_profil;
    }

    public void setMail_profil(String mail_profil) {
        this.mail_profil = mail_profil;
    }

    public String getMatricule_profil() {
        return matricule_profil;
    }

    public void setMatricule_profil(String matricule_profil) {
        this.matricule_profil = matricule_profil;
    }

    public int getId_site() {
        return id_site;
    }

    public void setId_site(int id_site) {
        this.id_site = id_site;
    }

    public String getLibelle_site() {
        return libelle_site;
    }

    public void setLibelle_site(String libelle_site) {
        this.libelle_site = libelle_site;
    }

    public String getGps_site() {
        return gps_site;
    }

    public void setGps_site(String gps_site) {
        this.gps_site = gps_site;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReponse_profil() {
        return reponse_profil;
    }

    public void setReponse_profil(String reponse_profil) {
        this.reponse_profil = reponse_profil;
    }
}
