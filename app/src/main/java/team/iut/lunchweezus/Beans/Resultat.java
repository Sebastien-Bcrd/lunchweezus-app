package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Resultat implements Serializable {

    Boolean res;
    private String msg= null;

   public Resultat(Boolean res) {
        this.res = res;
    }

    public Boolean getRes() {
        return res;
    }

    public void setRes(Boolean res) {
        this.res = res;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
