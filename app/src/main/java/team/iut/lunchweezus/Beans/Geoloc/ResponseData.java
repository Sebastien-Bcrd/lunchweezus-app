package team.iut.lunchweezus.Beans.Geoloc;

import java.io.Serializable;

public class ResponseData  implements Serializable {
    DataAdresse[] venues;

    public DataAdresse[] getVenues() {
        return venues;
    }

    public void setVenues(DataAdresse[] venues) {
        this.venues = venues;
    }
}
