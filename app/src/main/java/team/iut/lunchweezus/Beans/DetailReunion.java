package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class DetailReunion implements Serializable {

    Profil[] participants = new Profil[0];
    Reunion[] reunion;
    Profil[] organisateur;
    Commentaire[] commentaires = new Commentaire[0];
    Profil[] inviter = new Profil[0];
    Note[] notes = new Note[0];

    public Profil[] getParticipants() {
        return participants;
    }

    public void setParticipants(Profil[] participants) {
        this.participants = participants;
    }

    public Reunion[] getReunion() {
        return reunion;
    }

    public void setReunion(Reunion[] reunion) {
        this.reunion = reunion;
    }

    public Profil[] getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(Profil[] organisateur) {
        this.organisateur = organisateur;
    }

    public Commentaire[] getComments() {
        return commentaires;
    }

    public void setComments(Commentaire[] comments) {
        this.commentaires = comments;
    }

    public Profil[] getInviter() {
        return inviter;
    }

    public void setInviter(Profil[] inviter) {
        this.inviter = inviter;
    }

    public Note[] getNotes() {
        return notes;
    }

    public void setNotes(Note[] notes) {
        this.notes = notes;
    }
}
