package team.iut.lunchweezus.Beans;

public class Accueil {

    private Reunion[] Event;
    boolean hasNotif;

    public boolean isHasNotif() {
        return hasNotif;
    }

    public void setHasNotif(boolean hasNotif) {
        this.hasNotif = hasNotif;
    }

    public Reunion[] getEvent() {
        return Event;
    }

    public void setEvent(Reunion[] event) {
        Event = event;
    }
}
