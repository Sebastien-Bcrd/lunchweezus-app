package team.iut.lunchweezus.Beans;

public class Notification {

    int id_notif;
    String contenu_notif;
    String date_notif;
    int statu_notif;
    int type_notif;
    int id_profil;
    int id_reunion;

    public int getId_notif() {
        return id_notif;
    }

    public void setId_notif(int id_notif) {
        this.id_notif = id_notif;
    }

    public String getContenu_notif() {
        return contenu_notif;
    }

    public void setContenu_notif(String contenu_notif) {
        this.contenu_notif = contenu_notif;
    }

    public int getStatu_notif() {
        return statu_notif;
    }

    public void setStatu_notif(int statu_notif) {
        this.statu_notif = statu_notif;
    }

    public int getType_notif() {
        return type_notif;
    }

    public void setType_notif(int type_notif) {
        this.type_notif = type_notif;
    }

    public int getId_profil() {
        return id_profil;
    }

    public void setId_profil(int id_profil) {
        this.id_profil = id_profil;
    }

    public int getId_reunion() {
        return id_reunion;
    }

    public void setId_reunion(int id_reunion) {
        this.id_reunion = id_reunion;
    }

    public String getDate_notif() {
        return date_notif;
    }

    public void setDate_notif(String date_notif) {
        this.date_notif = date_notif;
    }

    public boolean isChecked() {
        return statu_notif != 0;
    }
}
