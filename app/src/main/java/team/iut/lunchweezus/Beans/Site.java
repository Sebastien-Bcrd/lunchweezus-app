package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Site implements Serializable {
    public int id_site = 0;
    public String libelle_site = null;
    public String adresse_site = null;

    public int getId_site() {
        return id_site;
    }

    public void setId_site(int id_site) {
        this.id_site = id_site;
    }

    public String getLibelle_site() {
        return libelle_site;
    }

    public void setLibelle_site(String libelle_site) {
        this.libelle_site = libelle_site;
    }

    public String getAdresse_site() {
        return adresse_site;
    }

    public void setAdresse_site(String adresse_site) {
        this.adresse_site = adresse_site;
    }
}
