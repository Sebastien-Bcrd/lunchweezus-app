package team.iut.lunchweezus.Beans.Geoloc;

import java.io.Serializable;

public class DataAdresse implements Serializable {
    private String name;
    private LocationData location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationData getLocation() {
        return location;
    }

    public void setLocation(LocationData location) {
        this.location = location;
    }
}
