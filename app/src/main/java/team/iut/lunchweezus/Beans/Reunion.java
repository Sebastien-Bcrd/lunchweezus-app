package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Reunion implements Serializable {
    public int id_reunion = 0;
    public String objet_reunion = null;
    public String heure_reunion = null;
    public String date_reunion = null;
    public String adresse_reunion = null;
    public String lieu_reunion = null;
    public int nbPlace_reunion = 1;
    public String photo_reunion = null;
    public String duree_reunion = null;
    public String type_reunion = null;
    //public Boolean valid_reunion = false;
    public int id_profil = 0;

    public int getId() {
        return id_reunion;
    }

    public void setId(int id) {
        this.id_reunion = id;
    }

    public String getObjet_reunion() {
        return objet_reunion;
    }

    public void setObjet_reunion(String objet_reunion) {
        this.objet_reunion = objet_reunion;
    }

    public String getHeure_reunion() {
        return heure_reunion;
    }

    public void setHeure_reunion(String heure_reunion) {
        this.heure_reunion = heure_reunion;
    }

    public String getDate_reunion() {
        return date_reunion;
    }

    public void setDate_reunion(String date_reunion) {
        this.date_reunion = date_reunion;
    }

    public String getAdresse_reunion() {
        return adresse_reunion;
    }

    public void setAdresse_reunion(String adresse_reunion) {
        this.adresse_reunion = adresse_reunion;
    }

    public String getLieu_reunion() {
        return lieu_reunion;
    }

    public void setLieu_reunion(String lieu_reunion) {
        this.lieu_reunion = lieu_reunion;
    }

    public int getNbPlace_reunion() {
        return nbPlace_reunion;
    }

    public void setNbPlace_reunion(int nbPlace_reunion) {
        this.nbPlace_reunion = nbPlace_reunion;
    }

    public String getPhoto_reunion() {
        return photo_reunion;
    }

    public void setPhoto_reunion(String photo_reunion) {
        this.photo_reunion = photo_reunion;
    }

    public String getDuree_reunion() {
        return duree_reunion;
    }

    public void setDuree_reunion(String duree_reunion) {
        this.duree_reunion = duree_reunion;
    }

    public int getId_profil() {
        return id_profil;
    }

    public void setId_profil(int id_profil) {
        this.id_profil = id_profil;
    }

    public String getType_reunion() {
        return type_reunion;
    }

    public void setType_reunion(String type_reunion) {
        this.type_reunion = type_reunion;
    }
}
