package team.iut.lunchweezus.Beans;

import java.io.Serializable;

public class Note implements Serializable {

    public int id_note = 0;
    public int valeur_note;
    public int id_profil;
    public int id_reunion;
    public String nom_profil;
    public String prenom_profil;

    public int getId_note() {
        return id_note;
    }

    public void setId_note(int id_note) {
        this.id_note = id_note;
    }

    public int getValeur_note() {
        return valeur_note;
    }

    public void setValeur_note(int valeur_note) {
        this.valeur_note = valeur_note;
    }

    public int getId_profil() {
        return id_profil;
    }

    public void setId_profil(int id_profil) {
        this.id_profil = id_profil;
    }

    public int getId_reunion() {
        return id_reunion;
    }

    public void setId_reunion(int id_reunion) {
        this.id_reunion = id_reunion;
    }

    public String getNom_profil() {
        return nom_profil;
    }

    public void setNom_profil(String nom_profil) {
        this.nom_profil = nom_profil;
    }

    public String getPrenom_profil() {
        return prenom_profil;
    }

    public void setPrenom_profil(String prenom_profil) {
        this.prenom_profil = prenom_profil;
    }
}
