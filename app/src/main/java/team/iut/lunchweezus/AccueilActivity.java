package team.iut.lunchweezus;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import team.iut.lunchweezus.Chatbot.ChatBotActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.Beans.Notification;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.ui.BadgeDrawable;
import team.iut.lunchweezus.ui.main.SectionsPagerAdapter;
import team.iut.lunchweezus.ui.utils;

public class AccueilActivity extends ActivityFonction implements NavigationView.OnNavigationItemSelectedListener {

    public List<Reunion> reunionsAvenir;
    public List<Reunion> reunionsPasser;
    public int pos = 0;
    ActionBarDrawerToggle toggle;

    private FloatingActionButton fab;
   private FloatingActionButton fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Debut();

        setContentView(R.layout.activity_accueil);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Accueil");
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toggle.setHomeAsUpIndicator(R.drawable.ic_hamburger_menu);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        utils.makeTabTitle(this, tabs, true);

        tabs.getTabAt(0).setIcon(R.drawable.ic_event);

        fab = findViewById(R.id.btnFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeActivity(AccueilActivity.this, CreerReunionActivity.class);
                finish();
                overridePendingTransition(R.anim.accueil_creer_up, R.anim.accueil_creer_off);
            }
        });
        fab2 = findViewById(R.id.btnFabBot);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeActivity(AccueilActivity.this, ChatBotActivity.class);

                overridePendingTransition(R.anim.accueil_creer_up, R.anim.accueil_creer_off);
            }
        });
    }

    @Override
    public void onResume() {
        Debut();

        toggle.setHomeAsUpIndicator(R.drawable.ic_hamburger_menu);

        Profil profil = sys.GetUserProfilLocal();
        if (profil == null) {
            ChangeActivity(AccueilActivity.this, LoginActivity.class);
            finish();
        } else {
            sys.getNotifications();

            NavigationView navigationView = findViewById(R.id.nav_view);

            View navHeader = navigationView.getHeaderView(0);

            TextView username = navHeader.findViewById(R.id.username);
            username.setText(profil.prenom_profil + " " + profil.nom_profil);

            TextView localisation = navHeader.findViewById(R.id.localisation);
            localisation.setText(profil.libelle_site);

            ImageView photoProfil = navHeader.findViewById(R.id.photo_profil_nav);
            CSystem.AfficherPhoto(profil.photo_profil, photoProfil, false);

            navHeader.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ChangeActivity(AccueilActivity.this, ProfilActivity.class);
                }
            });

            navigationView.setNavigationItemSelectedListener(this);

            recupererAllReunion();
        }

        super.onResume();
    }

    public void recupererAllReunion() {
        sys.GetListReunionAll(sys.userProfil);

        TabLayout tabs = findViewById(R.id.tabs);
        pos = tabs.getSelectedTabPosition();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_profil:
                ChangeActivity(this, ProfilActivity.class);
                overridePendingTransition(R.anim.accueil_profil_up, R.anim.accueil_profil_off);//animation vers Profil
                break;

            case R.id.nav_notifications:
                ChangeActivity(this, NotificationsActivity.class);
                finish();
                overridePendingTransition(R.anim.accueil_notif_up, R.anim.accueil_notif_off);//animation vers Notifications
                break;

            case R.id.nav_reunions:
                ChangeActivity(this, MesReunionsActivity.class);
                overridePendingTransition(R.anim.accueil_mereunion_up, R.anim.accueil_mereunion_off);//animation vers MesReunions
                break;

            case R.id.nav_deconnexion:
                sys.userProfil.id_profil = 0;
                sys.profilDataSource.deleteProfil(sys.userProfil);
                sys.userProfil = null;
                /// ici supprimer les données
                sys.reunionDataSource.DropAllTables();
                ChangeActivity(AccueilActivity.this, LoginActivity.class);
                finish();
                break;

            case R.id.nav_parametres:
                ChangeActivity(this, ParametresActivity.class);
                overridePendingTransition(R.anim.accueil_mereunion_up, R.anim.accueil_mereunion_off);
                break;

            case R.id.nav_a_propos:
                ChangeActivity(this, AboutActivity.class);
                overridePendingTransition(R.anim.accueil_mereunion_up, R.anim.accueil_mereunion_off);
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /// Est appeler qaund on récupére la list de toute les réunions
    public void successAllReunion(final List<Reunion> reunionList, final boolean hasNotif) {
        fab.show(); // Affiche le bouton pour crée des réunions
        ConstruireListReunion(reunionList); // Construit les réunions

        TabLayout tabs = findViewById(R.id.tabs);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs.setupWithViewPager(viewPager);

        utils.makeTabTitle(this, tabs, true);

        tabs.getTabAt(0).setIcon(R.drawable.ic_event);

        TabLayout.Tab selected = tabs.getTabAt(pos);
        selected.select();

        rearangementLocalReunion(reunionList);

    }

    /// Est appler quand on arrive pas a récuperer la liste de toute les réunions
    public void errorAllReunion(String message) {
        fab.hide(); // Cache le bouton crée réunions
        utils.toaster(getApplicationContext(), message, 0);
    }

    /// Permet de ranger les list de réunions historique ou a venir
    public void ConstruireListReunion(List<Reunion> reunions) {
        TabLayout tabs = findViewById(R.id.tabs);
        pos = tabs.getSelectedTabPosition();

        reunionsAvenir = new ArrayList<>();
        reunionsPasser = new ArrayList<>();
        Date currentDate = new Date();
        for (int i = 0; i < reunions.size(); i++) {
            try {
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date dateReunion = fmt.parse(reunions.get(i).date_reunion + " " + reunions.get(i).heure_reunion);
                if (currentDate.before(dateReunion)) {
                    reunionsAvenir.add(reunions.get(i));
                } else if (dateReunion.equals(currentDate)) {
                    reunionsAvenir.add(reunions.get(i));
                } else {
                    reunionsPasser.add(reunions.get(i));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    //animation vers Details en appuyant sur les CardView
    public void ChangeAnimation() {
        ChangeActivity(AccueilActivity.this, ReunionDetailsActivity.class);
        overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
    }

    private Drawable setBadgeCount(Context context, int res, int count) {
        LayerDrawable icon = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.ic_hamburger_badge);
        Drawable mainIcon = ContextCompat.getDrawable(context, res);
        BadgeDrawable badge = new BadgeDrawable(context);
        icon.mutate();
        badge.setCount("" + count);
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
        icon.setDrawableByLayerId(R.id.ic_main_icon, mainIcon);

        return icon;
    }

    @Override
    public void successNotifications(List<Notification> notifications) {
        NavigationView navigationView = findViewById(R.id.nav_view);
        MenuItem notifItem = navigationView.getMenu().getItem(1);
        if (!notifications.isEmpty()) {
            List<Notification> checked = new ArrayList<>();
            for (int i = 0; i < notifications.size(); i++)
                if (!notifications.get(i).isChecked())
                    checked.add(notifications.get(i));

            if (!checked.isEmpty()) {
                toggle.setHomeAsUpIndicator(setBadgeCount(this, R.drawable.ic_hamburger_badge, checked.size()));
                notifItem.setIcon(setBadgeCount(this, R.drawable.ic_notifications_badge, checked.size()));
            } else {
                toggle.setHomeAsUpIndicator(R.drawable.ic_hamburger_menu);
                notifItem.setIcon(R.drawable.ic_notifications);
            }
        } else {
            toggle.setHomeAsUpIndicator(R.drawable.ic_hamburger_menu);
            notifItem.setIcon(R.drawable.ic_notifications);
        }
    }

    /**
     * Réarange les réunions en local permet de vider les réunions qui n'existante plus
     *
     * @param reunions est la list des Réunions recu par le WebService
     */
    public void rearangementLocalReunion(List<Reunion> reunions) {
        List<Reunion> reunionLocal = sys.reunionDataSource.getAllReunions();
        List<Reunion> reunionGarder = new ArrayList<>();
        for (Reunion reu : reunions) // Parcours la list de réunion recu par le webService
        {
            for (Reunion reuLocal : reunionLocal) // Parcours la list des réunion local
            {
                if (reu.id_reunion == reuLocal.id_reunion) // vérifie si les réunion local et du webService ne sont pas les même
                {
                    reunionGarder.add(reu);
                }
            }
        }

        sys.reunionDataSource.DeleteAllReunion(); // supprime toute les réunions
        for (Reunion reunion : reunionGarder) // Parcours les réunion garder
        {
            sys.reunionDataSource.insertReunion(reunion); // Insert les réunions garder
        }
    }
}