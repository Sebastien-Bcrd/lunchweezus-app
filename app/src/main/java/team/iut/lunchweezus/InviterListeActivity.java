package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.ui.Invites.Adapter.inviterAdapter;

public class InviterListeActivity extends Fragment {

    private CreerReunionActivity reunionActivity;
    private ArrayList<Profil> profils;//garde les données depuis la base de données
    private RecyclerView RecyListInvi;
    public FloatingActionButton FABaccepterInviter;
    public EditText search_box;
    public Toolbar toolbar;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_inviter_liste, container, false);

        reunionActivity = (CreerReunionActivity) getActivity();
        RecyListInvi = v.findViewById(R.id.RecyListInvi);
        search_box = v.findViewById(R.id.search_box);
        FABaccepterInviter = v.findViewById(R.id.FABaccepterInviter);
        profils = new ArrayList<>();
        toolbar = v.findViewById(R.id.inviter_toolbar);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        toolbar.setTitle("Inviter");
    }

    public void dataset(List<Profil> profilList) {
        CreerReunionActivity creerReunionActivity = (CreerReunionActivity) getActivity();
        creerReunionActivity.inviterOuvert = true;
        creerReunionActivity.sys.profilDataSource.DeleteAllProfil();
        for (int i = 0; i < profilList.size(); i++) {
            if (creerReunionActivity.sys.userProfil.id_profil != profilList.get(i).id_profil) {
                profils.add(profilList.get(i));
                creerReunionActivity.sys.profilDataSource.deleteProfil(profilList.get(i));
                creerReunionActivity.sys.profilDataSource.insertProfil(profilList.get(i));
            }
        }

        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String recherche = search_box.getText().toString();
                if (!recherche.equals("")) AfficherProfil(recherche);
            }
        };

        search_box.addTextChangedListener(fieldValidatorTextWatcher);

        search_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String recherche = search_box.getText().toString();
                if (!recherche.equals("")) AfficherProfil(recherche);
                return true;
            }
        });
    }

    public void AfficherProfil(String filtre) {
        CreerReunionActivity creerReunionActivity = (CreerReunionActivity) getActivity();
        List<Profil> profilList = creerReunionActivity.sys.profilDataSource.getProfilsWithString(filtre);

        for (int i = 0; i < profilList.size(); i++) {
            if (profilList.get(i).id_profil == creerReunionActivity.sys.userProfil.id_profil || profilList.get(i).id_profil == 0) {
                profilList.remove(i);
            }
        }

        inviterAdapter adapter = new inviterAdapter(reunionActivity, new ArrayList<Profil>(profilList), reunionActivity.profils_saved);
        RecyListInvi.setLayoutManager(new LinearLayoutManager(reunionActivity));
        RecyListInvi.setAdapter(adapter);
        FABaccepterInviter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreerReunionActivity creerReunionActivity = (CreerReunionActivity) getActivity();
                creerReunionActivity.creerListeSaved();
                getFragmentManager().beginTransaction().remove(InviterListeActivity.this).commit();
            }
        });
    }

    public void Fermer() {
        CreerReunionActivity creerReunionActivity = (CreerReunionActivity) getActivity();
        creerReunionActivity.creerListeSaved();
        getFragmentManager().beginTransaction().remove(InviterListeActivity.this).commit();
    }
}
