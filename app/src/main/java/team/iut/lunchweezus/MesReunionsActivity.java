package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.ui.MyCardView;
import team.iut.lunchweezus.ui.utils;

public class MesReunionsActivity extends ActivityFonction {

    LinearLayout mesReunionsLayout;
    View placeholder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_reunions);

        Debut();

        int margins = utils.dpCalc(this, 8);
        int cardHeight = utils.dpCalc(this, 248);
        int cardRadius = utils.dpCalc(this, 4);
        int cardElevation = utils.dpCalc(this, 2);

        final Toolbar toolbar = findViewById(R.id.mes_reunions_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Mes réunions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        mesReunionsLayout = findViewById(R.id.mes_reunions_container);
        placeholder = findViewById(R.id.placeholder_mes_reunions);

        List<Reunion> reunions = sys.reunionDataSource.getAllReunions();

        final List<MyCardView> listCards = utils.makeReunionCard(getApplicationContext(), this, reunions);

        if (listCards.size() != 0) {
            mesReunionsLayout.removeAllViewsInLayout();
            placeholder.setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < listCards.size(); i++) {
            MyCardView item = listCards.get(i);
            MyCardView.LayoutParams cardParams = new MyCardView.LayoutParams(
                    MyCardView.LayoutParams.MATCH_PARENT,
                    cardHeight
            );
            if (i != 0)
                cardParams.setMargins(margins, 0, margins, margins);
            else
                cardParams.setMargins(margins, margins, margins, margins);
            item.setLayoutParams(cardParams);
            item.setRadius(cardRadius);
            item.setElevation(cardElevation);
            item.setBackgroundColor(getResources().getColor(R.color.colorPrimaryNight));

            mesReunionsLayout.addView(item);
        }

        final SwipeRefreshLayout refresh = findViewById(R.id.mesreu_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (listCards.size() != 0) {
                    mesReunionsLayout.removeAllViewsInLayout();
                    placeholder.setVisibility(View.INVISIBLE);
                }

                for (int i = 0; i < listCards.size(); i++)
                    mesReunionsLayout.addView(listCards.get(i));

                refresh.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        MesReunionsActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }
}
