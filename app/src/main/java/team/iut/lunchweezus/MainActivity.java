package team.iut.lunchweezus;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Logique.CSystem;


public class MainActivity extends ActivityFonction {

    TextView txtJson;
    ProgressDialog pd;

    Thread background = new Thread() {
        public void run() {
            try {

                Profil userProfil = sys.GetUserProfilLocal(); // On va cherche le UserProfil Local

                if (userProfil != null) { // Si le User profil existe
                    sys.GetUserProfil();
                } else { // Si le UserPrfil existe pas
                    ChangeActivity(MainActivity.this, LoginActivity.class); // Alors on va vers l'activité Login
                }

            } catch (Exception e) {
                // ...
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Debut();

        setContentView(R.layout.activity_main);
        background.start();


    }

    @Override
    public void Debut() {
        sys = new CSystem(); // Crée le système
        sys.Start(this); // s'iniasilise dans CSystem
    }

    /// Est Appeler si le profil est récupérer sur le webService
    public void ProfilSync() {
        ChangeActivity(MainActivity.this, AccueilActivity.class);  // Alors on va vers l'activité Accueil
        finish();
    }

    /// Est appeler s le profil n'est pas récupérer sur le serveur
    public void ProfilNonSync() {
        ChangeActivity(MainActivity.this, AccueilActivity.class);  // Alors on va vers l'activité Accueil
        finish();
    }

}