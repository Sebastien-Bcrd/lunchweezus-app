package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.ui.MyCardView;
import team.iut.lunchweezus.ui.utils;

public class UserListReunionsActivity extends ActivityFonction {
    String title; // Titre de l'action bar
    int type; // Type de requête
    Profil user; // Objet contenant le profil de l'utilisateur
    TextView comment; // Message du placeholder
    View placeholder; // Vue contenant le placeholder, s'affiche si il n'y a pas de réunions à afficher

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list_reunions);

        Debut();

        type = getIntent().getIntExtra("arg", 0);
        user = (Profil) getIntent().getSerializableExtra("user");
        comment = findViewById(R.id.placeholder_comment);
        placeholder = findViewById(R.id.placeholder_reunion_user_list);

        requestReu(type);

        final Toolbar toolbar = findViewById(R.id.user_reunions_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SwipeRefreshLayout refresh = findViewById(R.id.userlist_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestReu(type);

                refresh.setRefreshing(false);
            }
        });
    }

    /**
     * Affiche les réunions sur l'activié sous forme de liste
     *
     * @param reunion Liste contenant les réunions
     */
    private void addReunions(final List<Reunion> reunion) {
        int margins = utils.dpCalc(this, 8);
        int cardHeight = utils.dpCalc(this, 248);
        int cardRadius = utils.dpCalc(this, 4);
        int cardElevation = utils.dpCalc(this, 2);

        LinearLayout parent = findViewById(R.id.user_reunions_container);

        if (reunion.size() != 0)
            placeholder.setVisibility(View.INVISIBLE);

        if (parent.getChildCount() != 0)
            parent.removeAllViewsInLayout();

        List<MyCardView> listCards = utils.makeReunionCard(getApplicationContext(), this, reunion);

        for (int i = 0; i < reunion.size(); i++) {
            MyCardView item = listCards.get(i);
            MyCardView.LayoutParams cardParams = new MyCardView.LayoutParams(
                    MyCardView.LayoutParams.MATCH_PARENT,
                    cardHeight
            );
            if (i != 0)
                cardParams.setMargins(margins, 0, margins, margins);
            else
                cardParams.setMargins(margins, margins, margins, margins);
            item.setLayoutParams(cardParams);
            item.setRadius(cardRadius);
            item.setElevation(cardElevation);
            item.setBackgroundColor(getResources().getColor(R.color.colorPrimaryNight));

            parent.addView(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        UserListReunionsActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }

    /**
     * Permet de récupérer les réunions selon ce qui est demandé
     *
     * @param t Va de 0 à 3 selon ce qui est demandé
     *          0 pour afficher vos participations
     *          1 pour afficher vos organisations
     *          2 pour afficher les participations d'un utilisateur
     *          3 pour afficher les organisations d'un utilisateur
     */
    private void requestReu(int t) {
        switch (t) {
            case 0:
                title = "Participations";
                comment.setText("Vous ne participez à aucune réunion. Et si vous en rejoignez-une ?");
                addReunions(sys.reunionDataSource.getAllReunions());
                break;
            case 1:
                title = "Réunions organisées";
                comment.setText("Vous n'organisez aucune réunion. Et si vous créez un moment ?");
                addReunions(sys.reunionDataSource.getReunionsOrganisateur(user));
                break;
            case 2:
                title = "Participations de " + user.prenom_profil;
                comment.setText(user.prenom_profil + " ne participe à aucune réunion. Et si vous l'invitez ?");
                addReunions(Arrays.asList((Reunion[]) getIntent().getSerializableExtra("participer")));
                break;
            case 3:
                title = "Organisées par " + user.prenom_profil;
                comment.setText(user.prenom_profil + " n'organise aucune réunion.");
                addReunions(Arrays.asList((Reunion[]) getIntent().getSerializableExtra("organiser")));
                break;

                /* ajouté */
                /* case 4:
                title = "Note de " + user.prenom_profil;
                comment.setText(user.prenom_profil + " j'ai atteri où ? .");
                addReunions(Arrays.<Reunion>asList((Reunion[]) getIntent().getSerializableExtra("note")));
                break;*/
        }
    }
}
