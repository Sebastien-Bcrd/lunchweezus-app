package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Site;
import team.iut.lunchweezus.ui.utils;

public class InscriptionActivity extends ActivityFonction implements AdapterView.OnItemSelectedListener {

    //animation variables
    private CheckBox cguConfirm;
    private TextInputLayout nom_layout, matricule_layout, password_layout, reponse_layout;
    private TextInputEditText nom_edit_text, matricule_edit_text, password_edit_text, prenom_edit_text, reponse_edit_text;

    //pour le menu Spinner
    private Spinner spnEtreprise, spnFonction, spnQuestion;
    private int idSite;
    private String fonction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        Debut();


        nom_layout = findViewById(R.id.nom_layout);
        nom_edit_text = findViewById(R.id.nom_edit_text);
        matricule_layout = findViewById(R.id.matricule_layout);
        matricule_edit_text = findViewById(R.id.matricule_edit_text);
        password_layout = findViewById(R.id.password_layout);
        password_edit_text = findViewById(R.id.password_edit_text);
        prenom_edit_text = findViewById(R.id.prenom_edit_text);
        reponse_layout = findViewById(R.id.reponse_layout);
        reponse_edit_text = findViewById(R.id.reponse_edit_text);
        Button btnseconn = findViewById(R.id.btnInscription);
        TextView btnLogin = findViewById(R.id.btnLogin);
        TextView cguTxt = findViewById(R.id.cgu_accept_txt);
        cguConfirm = findViewById(R.id.cgu_accept);

        final LayoutInflater inflater = this.getLayoutInflater();

        String cguText1 = "J'ai lu et j'accepte les ";
        String cguText2 = "<font color='#00E676'><b>conditions générales d'utilisation</b></font>";
        cguTxt.setText(Html.fromHtml(cguText1 + cguText2));
        cguTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = inflater.inflate(R.layout.conteneur_cgu, null);
                utils.viewDialog(v.getContext(), null, dialogView).show();
            }
        });

        //variables pour le menu Spinner qui montre les Entreprises
        spnEtreprise = findViewById(R.id.spnEtreprise);//men

        sys.getAllSite();


        spnFonction = findViewById(R.id.spnFonction);
        getAllFonction();

        spnQuestion = findViewById(R.id.spnQuestion);
        getAllQuestion();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

              overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
            }
        });
        btnseconn.setOnClickListener(new View.OnClickListener() {
          
        @Override
        public void onClick(View view) {
            LinearLayout errorSpn = (LinearLayout) spnEtreprise.getSelectedView();
            TextView errorSpnTxt = errorSpn.findViewById(R.id.txtSpinner);

            LinearLayout errorSpn2 = (LinearLayout) spnFonction.getSelectedView();
            TextView errorSpnTxt2 = errorSpn2.findViewById(R.id.txtSpinner_fonction);



            ArrayList<String> values = new ArrayList<>();

            if (prenom_edit_text.getText().toString().isEmpty()) {
                prenom_edit_text.setError("Requis");
            } else {
                prenom_edit_text.setError(null);
                values.add(prenom_edit_text.getText().toString());
            }

            if (nom_edit_text.getText().toString().isEmpty()) {
                nom_layout.setError("Requis"); // show error
            } else {
                nom_layout.setError(null); // hide error
                values.add(nom_edit_text.getText().toString());
            }

            if (password_edit_text.getText().toString().isEmpty()) {
                password_layout.setError("Requis");
            } else {
                password_layout.setError(null); // hide error
                values.add("pwd");
            }

            if (matricule_edit_text.getText().toString().isEmpty()) {
                matricule_layout.setError("Requis");
            } else {
                matricule_layout.setError(null); // hide error
                values.add(matricule_edit_text.getText().toString());
            }

            if (reponse_edit_text.getText().toString().isEmpty()) {
                reponse_layout.setError("Requis");
            } else {
                reponse_layout.setError(null); // hide error
                values.add(reponse_edit_text.getText().toString());
            }

            if (spnEtreprise.getSelectedItemPosition() == 0) {
                errorSpnTxt.setTextColor(getResources().getColor(R.color.colorRefuse));
            } else {
                errorSpnTxt.setTextColor(getResources().getColor(R.color.textWhite));
                values.add("" + spnEtreprise.getSelectedItemPosition());
            }

            if (spnFonction.getSelectedItemPosition() == 0) {
                errorSpnTxt2.setTextColor(getResources().getColor(R.color.colorRefuse));
            } else {
                errorSpnTxt2.setTextColor(getResources().getColor(R.color.textWhite));
                values.add("" + spnFonction.getSelectedItemPosition());
            }

            if (!cguConfirm.isChecked()) {
                cguConfirm.setError("Requis");
            } else {
                cguConfirm.setError(null);
                values.add("CGU-true");
            }

            if (values.size() == 8) {
                Profil profil = new Profil();
                profil.nom_profil = nom_edit_text.getText().toString();
                profil.matricule_profil = matricule_edit_text.getText().toString();
                profil.prenom_profil = prenom_edit_text.getText().toString();
                profil.setId_site(idSite);
                profil.setFonction_profil(fonction);
                profil.reponse_profil = reponse_edit_text.getText().toString();
                sys.inscrireProfil(profil, password_edit_text.getText().toString());
            }
        }
    });
}

    /**
     * CSystem fait appel à cette fonction si l'inscription a réussi
     */
    public void SuccessInscription() {
        utils.toaster(getApplicationContext(), "Inscription réussie ! Veuillez vous connecter", 0);
        finish();
        overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
    }

    /**
     * CSystem fait appel à cette fonction si l'inscription a échoué
     *
     * @param message Message d'erreur
     */
    public void ErreurInscription(String message) {
        utils.toaster(getApplicationContext(), "Impossible de s'inscrire", 0);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spnEtreprise) {
            idSite = position;
        }

        if (parent.getId() == R.id.spnFonction) {
            fonction = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // DO NOT REMOVE
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
    }

    /**
     * CSystem fait appel à cette fonction si la récupération des sites d'Armatis depuis le server a réussi
     *
     * @param sites Liste de sites obtenue avec la requette au serveur
     */
    @Override
    public void successGetAllSite(final List<Site> sites) {
        final String[] strVille = new String[sites.size() + 1];
        strVille[0] = "Choisissez un site...";

        for (int i = 1; i <= sites.size(); i++) {
            strVille[i] = sites.get(i - 1).libelle_site;
        }

        spnEtreprise.setOnItemSelectedListener(InscriptionActivity.this);
        List<String> listEntr = new ArrayList<>();
        Collections.addAll(listEntr, strVille);
        ArrayAdapter<String> cmbAdapter = new ArrayAdapter<>(this, R.layout.spineer_entreprises_liste, R.id.txtSpinner, listEntr);
        spnEtreprise.setAdapter(cmbAdapter);
    }

    /**
     *  Permet d'initiliser le spinner qui permet de choisir son poste
     */
    public void getAllFonction() {
        final String[] strFunc = new String[4];
        strFunc[0] = "Choisissez une fonction...";

        strFunc[1] = "Conseiller";
        strFunc[2] = "Manager";
        strFunc[3] = "Chef d'équipe";

        spnFonction.setOnItemSelectedListener(InscriptionActivity.this);
        List<String> listFunc = new ArrayList<>();
        Collections.addAll(listFunc, strFunc);
        ArrayAdapter<String> cmbAdapter2 = new ArrayAdapter<>(this, R.layout.spineer_fonctions_liste, R.id.txtSpinner_fonction, listFunc);
        spnFonction.setAdapter(cmbAdapter2);
    }

    /**
     * Permet d'initiliser le spinner qui permet de choisir la question secrète à la laquelle répondre
     */
    public void getAllQuestion() {
        final String[] strQuestion = new String[4];
        strQuestion[0] = "Choisissez une question secrète...";

        strQuestion[1] = "Quel est le nom de votre meilleur ami d'enfance ?";
        strQuestion[2] = "Quel est votre ville de naissance ?";
        strQuestion[3] = "Quel est le nom de votre animal de compagnie ?";

        spnQuestion.setOnItemSelectedListener(InscriptionActivity.this);
        List<String> listQuestion = new ArrayList<>();
        Collections.addAll(listQuestion, strQuestion);
        ArrayAdapter<String> cmbAdapter3 = new ArrayAdapter<>(this, R.layout.spineer_questions_liste, R.id.txtSpinner_question, listQuestion);
        spnQuestion.setAdapter(cmbAdapter3);
    }
}
