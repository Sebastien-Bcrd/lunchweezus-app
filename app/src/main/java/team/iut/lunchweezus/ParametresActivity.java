package team.iut.lunchweezus;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import team.iut.lunchweezus.ui.utils;

public class ParametresActivity extends ActivityFonction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);

        Debut();

        final Toolbar toolbar = findViewById(R.id.parametres_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Paramètres");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        LinearLayout cache = findViewById(R.id.cache_delete);

        cache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.alert(
                        v.getContext(),
                        "Confirmation",
                        "Voulez-vous vraiment vider le cache ?",
                        0,
                        "Oui",
                        "Non",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sys.viderCache();
                                ChangeActivity(ParametresActivity.this,LoginActivity.class);
                            }
                        }, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                sys.viderCache();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        ParametresActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }
}
