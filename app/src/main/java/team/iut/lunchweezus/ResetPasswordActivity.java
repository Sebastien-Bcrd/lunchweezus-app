package team.iut.lunchweezus;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import team.iut.lunchweezus.ui.utils;

public class ResetPasswordActivity extends ActivityFonction {

    private TextInputEditText reset_mail_edit_text, reset_reponse_edit_text, reset_newpassword_edit_text;
    private TextInputLayout reset_mail_layout, reset_reponse_layout, reset_newpasswordLayout;
    private Spinner spnQuestion;
    private String question;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Debut();

        reset_mail_layout = findViewById(R.id.reset_mail_layout);
        reset_mail_edit_text = findViewById(R.id.reset_mail_edit_text);
        reset_reponse_layout = findViewById(R.id.reset_reponse_layout);
        reset_reponse_edit_text = findViewById(R.id.reset_reponse_edit_text);
        reset_newpasswordLayout = findViewById(R.id.reset_newpasswordLayout);
        reset_newpassword_edit_text = findViewById(R.id.reset_newpassword_edit_text);


        spnQuestion = findViewById(R.id.spnResetQuestion);
        getAllQuestion();

        Button bnReinit = findViewById(R.id.btnReset);

        bnReinit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> values = new ArrayList<>();

                //verifier si les champs ne sont pas vide
                reset_mail_layout.setError(null); // hide error
                reset_reponse_layout.setError(null); // hide error
                reset_newpasswordLayout.setError(null); // hide error

                if (reset_mail_edit_text.getText().toString().isEmpty()) {
                    reset_mail_layout.setError("Requis"); // show error
                } else {
                    reset_mail_layout.setError(null); // hide error
                    values.add(reset_mail_edit_text.getText().toString());
                }

                if (reset_reponse_edit_text.getText().toString().isEmpty()) {
                    reset_reponse_layout.setError("Requis");
                } else {
                    reset_reponse_layout.setError(null);
                    values.add("");
                }

                if (reset_newpassword_edit_text.getText().toString().isEmpty()) {
                    reset_newpasswordLayout.setError("Requis");
                } else {
                    reset_newpasswordLayout.setError(null);
                    values.add("");
                }

                if (values.size() == 3) {
                    String mail = reset_mail_edit_text.getText().toString();
                    String reponse = reset_reponse_edit_text.getText().toString();
                    String newPassword = reset_newpassword_edit_text.getText().toString();
                    sys.ResetPassword(mail, reponse, newPassword, ResetPasswordActivity.this);
                }
            }
        });
    }

    @Override
    public void onResume() {
        Debut();
        super.onResume();
    }

    /**
     * CSystem fait appel à cette fonction si la connexion a réussi
     *
     * @param con Vaut true si il n'y a pas de problème de connexion, false si il y a des problèmes
     *            commme une mauvaise réponse
     */
    public void Success(boolean con) {
        if (con) {
            ChangeActivity(ResetPasswordActivity.this, AccueilActivity.class);
            finish();
        } else {
            utils.toaster(getApplicationContext(), "Mauvaise réponse", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si la connexion a échoué
     *
     * @param message Message d'erreur
     */
    public void Error(String message) {
        utils.toaster(getApplicationContext(), "Impossible de se connecter", 0);
    }

    /**
     * Laisser cette fonction vide permet d'éviter de retourner sur l'animation de démarrage de l'application
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /**
     * Permet d'initiliser le spinner qui permet de choisir une question à laquelle répondre
     */
    public void getAllQuestion() {
        final String[] strQuestion = new String[4];
        strQuestion[0] = "Choisissez une question secrète...";

        strQuestion[1] = "Quel est le nom de votre meilleur ami d'enfance ?";
        strQuestion[2] = "Quel est votre ville de naissance ?";
        strQuestion[3] = "Quel est le nom de votre animal de compagnie ?";

        spnQuestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        List<String> listQuestion = new ArrayList<>();
        Collections.addAll(listQuestion, strQuestion);
        ArrayAdapter<String> cmbAdapter3 = new ArrayAdapter<>(this, R.layout.spineer_questions_liste, R.id.txtSpinner_question, listQuestion);
        spnQuestion.setAdapter(cmbAdapter3);
    }
}
