package team.iut.lunchweezus;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import team.iut.lunchweezus.Beans.Commentaire;
import team.iut.lunchweezus.Beans.DetailReunion;
import team.iut.lunchweezus.Beans.Note;
import team.iut.lunchweezus.Beans.Profil;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.Logique.onEventListener;
import team.iut.lunchweezus.reu_fragments.ReunionCommentairesFragment;
import team.iut.lunchweezus.reu_fragments.ReunionDetailsFragment;
import team.iut.lunchweezus.reu_fragments.ReunionNotesFragment;
import team.iut.lunchweezus.reu_fragments.ReunionProfilFragment;
import team.iut.lunchweezus.ui.reunion.ViewPagerAdapter;
import team.iut.lunchweezus.ui.utils;

import static java.lang.Thread.*;

public class ReunionDetailsActivity extends ActivityFonction implements ReunionProfilFragment.BottomSheetListener {

    private ReunionDetailsFragment reunionDetailsFragment; // Permet d'utiliser le fragment des details de la réunion
    private ViewPagerAdapter adapter; // Adapteur de la vue de pagination
    private DetailReunion detailReunion; // Objet qui contiendra les détails de la réunion
    private ReunionProfilFragment reunionProfilFragment; // Permet d'utiliser le fragment du profil d'un utilisateur
    private View fabs, commentaireInput, orgInvitLayout, avisLayout;
    private boolean connected = false; // Permet de dire si la personne est connectée ou non
    private boolean photoCharger = false; // Permet de savoir si la photo à déja était charger ou non
    private ImageView header; // Vue d'image contenant le header
    private int userStatus;
    FloatingActionButton fab;

    //variables pour la PHOTO
    public Bitmap bitmap;
    public static final int READ_EXTERNAL_STORAGE = 0;//variable pour identifier l'option dont on accpete
    private static final int CAMERA_ACTION_PICK_REQUEST_CODE = 610;
    private static final int PICK_IMAGE_GALLERY_REQUEST_CODE = 609;
    String path;

    @Override
    public void Debut()
    {
        sys = (CSystem) getIntent().getSerializableExtra("CSystem");
        sys.Start(this); // s'iniasilise dans CSystem
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Debut();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reunion_details);

        // Initialisation des attributs
        header = findViewById(R.id.reu_header);
        commentaireInput = findViewById(R.id.commentaire_coordinator);
        commentaireInput.setVisibility(View.INVISIBLE);
        orgInvitLayout = findViewById(R.id.org_invit_l);
        orgInvitLayout.setVisibility(View.INVISIBLE);
        avisLayout = findViewById(R.id.avis_coordinator);
        avisLayout.setVisibility(View.INVISIBLE);

        // Creation de la toolbar
        final Toolbar toolbar = findViewById(R.id.reu_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Réunion");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        fabs = findViewById(R.id.fab_cl);

        FloatingActionButton fabOrgInvit = findViewById(R.id.org_invit);
        fabOrgInvit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeActivity(ReunionDetailsActivity.this, InviterSurReuActivity.class);
            }
        });

        final ViewPager viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        TabLayout tabLayout = findViewById(R.id.reu_tabs);
        tabLayout.setupWithViewPager(viewPager);

        utils.makeTabTitle(this, tabLayout, false);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_reunion_details);

        final CoordinatorLayout layout = findViewById(R.id.fab_cl);
        layout.removeAllViewsInLayout();

        fab = makeFab(false);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
                if (detailReunion != null) {
                    switch (tab.getPosition()) {
                        case 0:
                            if (connected)
                                SuccessDetailReunion(detailReunion, true);
                            else
                                SuccessDetailReunion(detailReunion, false);

                            if (userStatus == 0 && siReunionHistorique(sys.reunionSelected))
                                userStatus = -1;
                            changeOnUserState(userStatus);

                            fabs.setVisibility(View.VISIBLE);

                            commentaireInput.setVisibility(View.INVISIBLE);

                            if (userStatus == 3) { // Organisateur
                                if (siReunionHistorique(sys.reunionSelected)) { // Réu finie + organisateur, permet d'ajouter une photo finish
                                    userStatus = 4;
                                    orgInvitLayout.setVisibility(View.INVISIBLE);
                                }
                            }

                            if (avisLayout != null)
                                avisLayout.setVisibility(View.INVISIBLE);

                            CoordinatorLayout layout = findViewById(R.id.fab_cl);
                            layout.removeAllViewsInLayout();



                            FloatingActionButton fab = makeFab(false);

                            // Calcul le nombre de place prise actuellement
                            int nbParticipant = detailReunion.getInviter().length + detailReunion.getParticipants().length + 1; // +1 pour l'organisateur

                            switch (userStatus) {
                                case 0: // Pas rejoind, pas invité
                                    // Vérifie si le nombre de participant actuel et inférieur au nombre de place max
                                    if (nbParticipant < detailReunion.getReunion()[0].nbPlace_reunion) {
                                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_reply));
                                        fab.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                sys.addParticipant("" + detailReunion.getReunion()[0].id_reunion);
                                            }
                                        });
                                        layout.addView(fab);
                                    }
                                    if (avisLayout != null)
                                        avisLayout.setVisibility(View.INVISIBLE);
                                    changeOptionItemOnStatus(0);
                                    break;
                                case 1: // A déjà rejoind
                                    // Vérifie si le nombre de participant actuel est inférieur au nombre de place max
                                    if (nbParticipant < detailReunion.getReunion()[0].nbPlace_reunion) {
                                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_add));
                                        fab.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ChangeActivity(ReunionDetailsActivity.this, InviterSurReuActivity.class);
                                            }
                                        });
                                        layout.addView(fab);
                                    }
                                    changeOptionItemOnStatus(1);
                                    break;
                                case 2: // Est invité
                                    layout.addView(makeFabInvit());
                                    changeOptionItemOnStatus(0);
                                    break;
                                case 3: // Organisateur
                                    if (!siReunionHistorique(sys.reunionSelected)) {
                                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
                                        fab.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ChangeActivity(ReunionDetailsActivity.this, ModifierReunionActivity.class);
                                            }
                                        });
                                        layout.addView(fab);
                                        orgInvitLayout.setVisibility(View.VISIBLE);
                                        changeOptionItemOnStatus(2);
                                    }
                                    break;
                                case 4: // Réu finie + organisateur, permet d'ajouter une photo finish
                                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera));
                                    fab.getDrawable().setTint(getResources().getColor(R.color.white));
                                    fab.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                                    != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                                    && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
                                                }
                                            } else {
                                                callgalery();
                                            }
                                        }
                                    });
                                    layout.addView(fab);
                                    changeOptionItemOnStatus(0);
                                    break;
                                case -1:
                                    fab = null;

                                    changeOptionItemOnStatus(0);
                                    break;
                            }

                            break;

                        case 1:
                            afficherCommentaires(detailReunion);

                            final ReunionCommentairesFragment reunionCommentairesFragment = (ReunionCommentairesFragment) adapter.getItem(1);
                            reunionCommentairesFragment.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                @Override
                                public void onRefresh() {
                                    afficherCommentaires(detailReunion);
                                    reunionCommentairesFragment.refresh.setRefreshing(false);
                                }
                            });

                            fabs.setVisibility(View.INVISIBLE);

                            commentaireInput.setVisibility(View.VISIBLE);

                            orgInvitLayout.setVisibility(View.INVISIBLE);

                            if (avisLayout != null)
                                avisLayout.setVisibility(View.INVISIBLE);

                            break;
                        case 2:
                            AfficherNotes(detailReunion);

                            final ReunionNotesFragment reunionNotesFragment = (ReunionNotesFragment) adapter.getItem(2);
                            reunionNotesFragment.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                @Override
                                public void onRefresh() {
                                    AfficherNotes(detailReunion);
                                    reunionNotesFragment.refresh.setRefreshing(false);
                                }
                            });

                            fabs.setVisibility(View.INVISIBLE);
                            orgInvitLayout.setVisibility(View.INVISIBLE);
                            commentaireInput.setVisibility(View.INVISIBLE);
                            if (avisLayout != null)
                                avisLayout.setVisibility(View.VISIBLE);
                            else {
                                if (!siReunionHistorique(sys.reunionSelected))
                                    utils.toaster(getApplicationContext(), "La réunion n'est pas encore passée, vous ne pouvez pas laisser de note", 1);
                            }
                            break;
                    }
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // DO NOT REMOVE
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // DO NOT REMOVE
            }
        });

        FloatingActionButton send = findViewById(R.id.send_note);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean alreadyRated = false;
                int id = 0;
                for (Note note : detailReunion.getNotes())
                    if (note.id_profil == sys.userProfil.id_profil) {
                        alreadyRated = true;
                        id = note.id_note;
                    }
                final int idN = id;

                if (alreadyRated) {
                    AlertDialog.Builder a = utils.alert(ReunionDetailsActivity.this,
                            "Note",
                            "Vous avez déjà noté cette réunion ! Voulez-vous modifier votre note ?",
                            0,
                            "Oui",
                            "Non",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    RatingBar rating = findViewById(R.id.user_note);
                                    Note note = new Note();
                                    note.id_note = idN;
                                    note.valeur_note = (int) rating.getRating();
                                    sys.updateNote(note, detailReunion);
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    a.show();
                } else {
                    RatingBar rating = findViewById(R.id.user_note);
                    Note note = new Note();
                    note.id_profil = sys.userProfil.id_profil;
                    note.id_reunion = sys.reunionSelected.id_reunion;
                    note.valeur_note = (int) rating.getRating();
                    sys.addNote(note, detailReunion);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!sys.siViensDeCreeReu) {
            finish();
            ReunionDetailsActivity.this.overridePendingTransition(R.anim.accueil_creer_up, R.anim.accueil_creer_off);
        } else {
            sys.siViensDeCreeReu = false;
            ChangeActivity(ReunionDetailsActivity.this, AccueilActivity.class);
        }
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        Debut();
        photoCharger = false;
        sys.getReunion(sys.reunionSelected.id_reunion);
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.details_action, menu);

        MenuItem quit = menu.findItem(R.id.details_quit);
        MenuItem delete = menu.findItem(R.id.details_delete);
        quit.setVisible(false);
        delete.setVisible(false);

        return true;
    }

    /**
     * CSystem fait appel à cette fonction quand la récupération des détails de la réunion a réussi
     *
     * @param detail Objet contenant les détails de la réunion
     * @param con    Servant à vérifier si la personne est connectée (true) ou non (false)
     */
    @Override
    public void SuccessDetailReunion(final DetailReunion detail, final boolean con) {
        if (detail != null && detail.getReunion().length > 0) {
            sys.detailReunionSelected = detail;
            sys.reunionSelected = detail.getReunion()[0];

            String[] date = sys.reunionSelected.date_reunion.split("-");
            String annnee = date[0];
            date[0] = date[2];
            date[2] = annnee;
            sys.reunionSelected.date_reunion = date[0] + "-" + date[1] + "-" + date[2];

            if (!photoCharger) // Si la photo n'est pas chager
            {
                photoCharger = true; // alors  photocharger = true
                // Charger la photo
                if (sys.reunionSelected.photo_reunion != null && !sys.reunionSelected.photo_reunion.equals("")) {
                    CSystem.AfficherPhoto(sys.reunionSelected.photo_reunion, header, true);
                } else {
                    header.setImageResource(R.drawable.reudefault);
                }
            }

            connected = con;
            AfficherDetailReunion(detail);
            if (con) {
                sys.getStatusProfil(detailReunion.getReunion()[0].id_reunion, new onEventListener() {
                    @Override
                    public void onSuccess(Object notation) {
                        userStatus = sys.status;
                        changeOnUserState(userStatus);

                        if (!siReunionHistorique(sys.reunionSelected)) { /// On vérifie si la réunion à passer la date
                            changeOnUserState(userStatus);
                            if (avisLayout != null)
                                avisLayout = null;
                        } else {
                            if (userStatus == 0) {
                                changeOnUserState(-1);
                                if (avisLayout != null)
                                    avisLayout = null;
                            } else if (userStatus == 3) {
                                userStatus = 4;
                                changeOnUserState(userStatus);
                                if (avisLayout != null)
                                    avisLayout = null;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        ErreurDetailReunion("Status introuvable");
                    }
                });

            }
        } else {
            utils.toaster(getApplicationContext(), "Cette réunion n'existe pas ou plus", 0);
            finish();
        }
    }
    /**
     * Vérifie si la réunion est passée ou non
     *
     * @param reunion Objet contenant la réunion
     * @return Renvoie true si elle est en historique, false si elle ne l'est pas
     */
    public boolean siReunionHistorique(Reunion reunion) {
        Date currentDate = new Date();
        Calendar Hour = Calendar.getInstance();

        try {
            Date dateReunion;

            String dateStr = reunion.date_reunion + " " + reunion.heure_reunion;

            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            if (utils.isValidFormat("dd-MM-yyyy HH:mm:ss", dateStr))
                dateReunion = fmt.parse(dateStr);
            else
                dateReunion = fmt2.parse(dateStr);


                    currentDate.getTime();
            if (currentDate.after(dateReunion))
                return true;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Affiche les détails de la réunion sur le fragment des détails de la réunion
     *
     * @param detail Objet contenant les détails de la réunion
     */
    private void AfficherDetailReunion(DetailReunion detail) {
        detailReunion = detail;
        if (detail.getReunion() != null && detail.getOrganisateur() != null) {
            final Reunion reunion = detail.getReunion()[0];
            final Profil organisateur = detail.getOrganisateur()[0];
            if (organisateur != null) {
                reunion.setId_profil(organisateur.id_profil);
                final List<Profil> participants = Arrays.asList(detail.getParticipants());
                final List<Profil> inviter = Arrays.asList(detail.getInviter());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reunionDetailsFragment = (ReunionDetailsFragment) adapter.getItem(0);
                        reunionDetailsFragment.setReunion(reunion, organisateur);
                        LinearLayout participantsListeLayout = reunionDetailsFragment.getView().findViewById(R.id.listParticipants);
                        participantsListeLayout.removeAllViewsInLayout();
                        reunionDetailsFragment.addParticipantToList(reunion, organisateur, participants, inviter, sys);
                    }
                });
            }
        }
    }

    /**
     * Affiche les commentaires liés à la réunion sur le fragment des commentaires
     *
     * @param detail Objet contenant les détails de la réunion
     */
    private void afficherCommentaires(final DetailReunion detail) {
        RecyclerView commentsLayout = findViewById(R.id.messages);
        if (commentsLayout.getChildCount() != 0)
            commentsLayout.removeAllViewsInLayout();
        if (detail.getComments() != null) {
            ArrayList<Commentaire> listComm = new ArrayList<>();
            ReunionCommentairesFragment reunionCommentairesFragment = (ReunionCommentairesFragment) adapter.getItem(1);
            for (Commentaire comment : detail.getComments()) {
                String[] date = comment.getDate().split("-");
                String annnee = date[0];
                date[0] = date[2];
                date[2] = annnee;
                comment.setDate(date[0] + "-" + date[1] + "-" + date[2]);
                listComm.add(comment);
            }
            reunionCommentairesFragment.afficherComment(listComm, sys.userProfil.id_profil);
        }
    }

    /**
     * CSystem fait appel à cette fonction si la récupération des détails de la réunion a échoué
     *
     * @param message Message d'erreur
     */
    @Override
    public void ErreurDetailReunion(String message) {
        utils.toaster(this, "Impossible d'afficher cette réunion", 0);
    }

    /**
     * Permet d'ajouter des fragments au TabLayout, c'est à dire ajouter des onglets
     *
     * @param viewPager Objet de pagination, alimentera le TabLayout
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ReunionDetailsFragment(), "Informations"); // 0
        adapter.addFrag(new ReunionCommentairesFragment(), "Commentaires"); // 1
        adapter.addFrag(new ReunionNotesFragment(), "Notes"); // 3
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        final EditText p = new EditText(this);
        p.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        p.setTextColor(getResources().getColor(R.color.white));
        p.setPadding(utils.dpCalc(getApplicationContext(), 16), utils.dpCalc(getApplicationContext(), 8), utils.dpCalc(getApplicationContext(), 16), utils.dpCalc(getApplicationContext(), 8));

        if (id == R.id.details_quit) {
            AlertDialog.Builder pq = utils.prompt(this, "Pourquoi voulez-vous quitter la réunion ?", false);
            pq.setView(p);
            pq.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String pa = p.getText().toString();

                    DateFormat heureFormat = new SimpleDateFormat("HH:mm:ss");
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateComment = new Date();
                    Date heureComment = new Date();

                    Commentaire comment = new Commentaire();
                    comment.date_commentaire = dateFormat.format(dateComment);
                    comment.heure_commentaire = heureFormat.format(heureComment);
                    comment.id_reunion = detailReunion.getReunion()[0].id_reunion;
                    comment.id_profil = sys.userProfil.id_profil;
                    comment.prenom_profil = sys.userProfil.prenom_profil;
                    comment.nom_profil = sys.userProfil.nom_profil;

                    if (!pa.equals(""))
                        comment.contenu_commentaire = "J'ai quitté la réunion: " + pa;
                    else
                        comment.contenu_commentaire = "J'ai quitté la réunion";

                    sys.quitterReunion(sys.reunionSelected.id_reunion);
                    sys.addCommentaire(comment, false);
                }
            });
            pq.show();

        } else if (id == R.id.details_delete) {
            AlertDialog.Builder ps = utils.prompt(this, "Pourquoi voulez-vous annuler la réunion ?", true);
            ps.setView(p);
            ps.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String pa = p.getText().toString();
                    String msg = detailReunion.getOrganisateur()[0].prenom_profil + " " +
                            detailReunion.getOrganisateur()[0].nom_profil +
                            " a annulé la réunion " + detailReunion.getReunion()[0].objet_reunion;
                    if (!pa.equals("")) {
                        // Le message est stocké dans la variable pa
                        msg += ": " + pa;
                    }
                    sys.supprimerReunion(sys.reunionSelected.id_reunion, msg);
                }
            });
            ps.show();

        } else {
            if (!sys.siViensDeCreeReu) {
                finish();
                ReunionDetailsActivity.this.overridePendingTransition(R.anim.accueil_creer_up, R.anim.accueil_creer_off);
            } else {
                sys.siViensDeCreeReu = false;
                ChangeActivity(ReunionDetailsActivity.this, AccueilActivity.class);
            }

        }
        return true;
    }

    /**
     * Permet de changer l'interface par rapport au statut de l'utilisateur vis à vis de la réunion
     * C'est à dire si cette personne n'a pas rejoint, si elle participe à la réunion, si elle y est
     * invitée, si elle en est l'organisatrice, si la réunion est terminée et qu'elle est organisatrice,
     * ou si la réunion est tout simplement terminée
     *
     * @param state Statut de l'utilisateur
     */
    private void changeOnUserState(int state) {
        CoordinatorLayout layout = findViewById(R.id.fab_cl);
        layout.removeAllViewsInLayout();

        FloatingActionButton fab = makeFab(false);

        // Calcul le nombre de place prise actuellement
        int nbParticipant = detailReunion.getInviter().length + detailReunion.getParticipants().length + 1; // +1 pour l'organisateur

        switch (state) {
            case 0: // Pas rejoind, pas invité
                // Vérifie si le nombre de participant actuel et inférieur au nombre de place max
                if (nbParticipant < detailReunion.getReunion()[0].nbPlace_reunion) {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_reply));
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sys.addParticipant("" + detailReunion.getReunion()[0].id_reunion);
                        }
                    });
                    layout.addView(fab);
                }
                if (avisLayout != null)
                    avisLayout.setVisibility(View.INVISIBLE);
                changeOptionItemOnStatus(0);
                break;
            case 1: // A déjà rejoins
                // Vérifie si le nombre de participant actuel est inférieur au nombre de place max
                if (nbParticipant < detailReunion.getReunion()[0].nbPlace_reunion) {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_add));
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ChangeActivity(ReunionDetailsActivity.this, InviterSurReuActivity.class);
                        }
                    });
                    layout.addView(fab);
                }
                changeOptionItemOnStatus(1);
                break;
            case 2: // Est invité
                layout.addView(makeFabInvit());
                changeOptionItemOnStatus(0);
                break;
            case 3: // Organisateur
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChangeActivity(ReunionDetailsActivity.this, ModifierReunionActivity.class);
                    }
                });
                layout.addView(fab);
                orgInvitLayout.setVisibility(View.VISIBLE);
                changeOptionItemOnStatus(2);
                break;
            case 4: // Réu finie + organisateur, permet d'ajouter une photo finish
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera));
                fab.getDrawable().setTint(getResources().getColor(R.color.white));
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                && (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
                            }
                        } else {
                            callgalery();
                        }
                    }
                });
                layout.addView(fab);
                changeOptionItemOnStatus(0);
                break;
            case -1:
                fab = null;

                changeOptionItemOnStatus(0);
                break;
        }
    }

    /**
     * Permet de moduler les boutons de la barre d'action par rapport au statut de la personne
     * vis à vis de la réunion
     * Si elle n'a pas rejoint, n'est qu'invitée, ou que la réunion est passée: rien n'est affichée
     * Si elle a rejoint la réunion, le bouton quitter ma réunion est affiché
     * Si elle est organisatrice, le bouton annuler/supprimer la réunion est affiché
     *
     * @param status Statut de la personne
     */
    private void changeOptionItemOnStatus(int status) {
        final Toolbar toolbar = findViewById(R.id.reu_toolbar);
        Menu menu = toolbar.getMenu();

        MenuItem quit = menu.findItem(R.id.details_quit);
        MenuItem delete = menu.findItem(R.id.details_delete);

        switch (status) {
            case 0: // N'a pas rejoind, n'est pas organisateur, est juste invité, ou réu passée
                quit.setVisible(false);
                delete.setVisible(false);
                break;
            case 1: // A rejoins
                quit.setVisible(true);
                delete.setVisible(false);
                break;
            case 2: // Est organisateur
                quit.setVisible(false);
                delete.setVisible(true);
                break;
        }
    }

    /**
     * Permet de créer un bouton d'action flottant pour les affichages dynamique
     *
     * @param align Où aligner le bouton. Vaut true pour l'aligner en bas et au milieu de la vue parent,
     *              ou vaut false si il faut l'afficher dans un coin
     * @return Retourne le bouton
     */
    private FloatingActionButton makeFab(boolean align) {
        FloatingActionButton fab = new FloatingActionButton(this);

        int margins = utils.dpCalc(this, 16);

        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.WRAP_CONTENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(margins, margins, margins, margins);

        params.gravity = Gravity.BOTTOM + Gravity.END;

        if (align) {
            params.gravity = Gravity.BOTTOM;
        }

        fab.setLayoutParams(params);
        fab.setSize(FloatingActionButton.SIZE_NORMAL);

        return fab;
    }

    /**
     * Permet de créer les boutons d'action flottant concernant les boutons permettant d'accepter ou *
     * refuser une réunion
     *
     * @return Retourne les deux boutons dans une vue
     */
    private LinearLayout makeFabInvit() {
        LinearLayout prompt = new LinearLayout(this);
        prompt.setWeightSum(2);
        prompt.setOrientation(LinearLayout.HORIZONTAL);
        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
        );
        params.gravity = Gravity.BOTTOM;
        prompt.setLayoutParams(params);

        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        llParams.weight = 1;

        LinearLayout llOui = new LinearLayout(this);
        llOui.setLayoutParams(llParams);
        llOui.setGravity(Gravity.CENTER_HORIZONTAL);

        LinearLayout llNon = new LinearLayout(this);
        llNon.setLayoutParams(llParams);
        llNon.setGravity(Gravity.CENTER_HORIZONTAL);

        FloatingActionButton fabOui = makeFab(true);
        fabOui.setImageDrawable(getResources().getDrawable(R.drawable.ic_accept));
        fabOui.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
        fabOui.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccept)));
        fabOui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sys.accepterInvitation(detailReunion.getReunion()[0].id_reunion);
            }
        });

        FloatingActionButton fabNon = makeFab(true);
        fabNon.setImageDrawable(getResources().getDrawable(R.drawable.ic_croix));
        fabNon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
        fabNon.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorRefuse)));
        fabNon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sys.refuserInvitation(detailReunion.getReunion()[0].id_reunion);
            }
        });

        llOui.addView(fabOui);
        llNon.addView(fabNon);

        prompt.addView(llOui);
        prompt.addView(llNon);

        return prompt;
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                callgalery();
        }

    }

    /**
     * Crée un modal listant les choix possibles pour la source de l'image à envoyer
     */
    private void callgalery() {

        final CharSequence[] options = {"Caméra", "Galerie", "Annuler"};
        final android.support.v7.app.AlertDialog.Builder alertOp = new android.support.v7.app.AlertDialog.Builder(ReunionDetailsActivity.this, R.style.AlertDialogCustom);
        alertOp.setTitle("Choisissez une source");
        alertOp.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (options[i].equals("Caméra")) {
                    prendrePhoto();
                } else {
                    if (options[i].equals("Galerie")) {
                        openImagesDocument();
                    } else {
                        dialogInterface.dismiss();
                    }
                }
            }
        });
        alertOp.show();
    }

    /**
     * Ouvre l'appareil photo pour prendre une photo
     */
    private void prendrePhoto() {
        //CHEMIN de la photo prise depuis la CAMERA
        //varible qu'on crée pour la photo prise depuis la CAMERA
        File fileImage = new File(Environment.getExternalStorageDirectory(), "LunchWeezUs/");
        boolean isCreate = fileImage.exists();
        String nom_img = "";
        if (isCreate) {
            nom_img = System.currentTimeMillis() / 1000 + ".jpg";
        }
        path = Environment.getExternalStorageDirectory() + File.separator + "LunchWeezUs/" + File.separator + nom_img;

        File imgUpImage = new File(path);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri imageUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = getApplicationContext().getPackageName() + ".provider";
            imageUri = FileProvider.getUriForFile(ReunionDetailsActivity.this, authorities, imgUpImage);
        } else {
            imageUri = Uri.fromFile(imgUpImage);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        startActivityForResult(intent, CAMERA_ACTION_PICK_REQUEST_CODE);
    }

    /**
     * Ouvre le navigateur de documents pour choisir une image
     */
    private void openImagesDocument() {
        Intent pictureIntent = new Intent(Intent.ACTION_PICK);
        pictureIntent.setType("image/*");
        startActivityForResult(pictureIntent, PICK_IMAGE_GALLERY_REQUEST_CODE);
    }

    /**
     * Selon le retour des fonctions précédentes, on ouvre un découpeur d'image pour que l'utilisateur
     * découpe l'image au format nécessaire, ou on récupère l'image et l'affiche si la découpe vient
     * d'être effectuée. Cette fonction est gérée automatiquement par les activitées
     *
     * @param requestCode Code de requête
     * @param resultCode  Code de résultat
     * @param data        Données reçues depuis l'activité qui vient d'être fermée (appareil photo, documents,
     *                    ou découpeur d'image)
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_ACTION_PICK_REQUEST_CODE:
                    MediaScannerConnection.scanFile(this, new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                //savoir si le processus a fini ou pas
                                public void onScanCompleted(String s, Uri uri) {
                                    Log.i("Chemin d'hebergement", "Path: " + path);
                                    System.out.println("Path: " + path);
                                }
                            });
                    uri = Uri.parse("file:" + path);
                    openCropActivity(uri, uri);
                    break;

                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        uri = UCrop.getOutput(data);
                        showImage(uri);

                        Reunion r = detailReunion.getReunion()[0];
                        r.photo_reunion = convertImgString(bitmap);
                        sys.ModifierReunion(r);

                        utils.toaster(getApplicationContext(), "Envoi de la photo finish en cours...", 0);
                    }
                    break;

                case PICK_IMAGE_GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    uri = Uri.parse("file:" + picturePath);

                    openCropActivity(uri, uri);
                    break;
            }
        }
    }

    /**
     * Permet d'encoder l'image en base 64 avant l'envoi au serveur. Cette fonction compresse
     * également l'image au format jpeg.
     *
     * @param bitmap Image à encoder
     * @return Retourne l'image convertie en base 64
     */
    public String convertImgString(Bitmap bitmap) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, array);
        byte[] imageByte = array.toByteArray();

        return Base64.encodeToString(imageByte, Base64.DEFAULT);
    }

    /**
     * Ouvre le découpeur d'image
     *
     * @param sourceUri      Image source
     * @param destinationUri Chemin de destination, où sauvegarder l'image
     */
    private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(this, R.color.white));
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(1010, 720)
                .withAspectRatio(87, 62)
                .start(this);
    }

    /**
     * Permet d'afficher l'image sur le header de la page
     *
     * @param imageUri Chemin de l'image
     */
    private void showImage(Uri imageUri) {
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), imageUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onButtonClicked(String text) {
        // DO NOT REMOVE
    }

    /**
     * Crée un modal permettant d'afficher le profil d'une personne depuis la liste d'invités
     *
     * @param profilFragement Objet contenant le modal
     */
    public void setReunionProfilFragement(ReunionProfilFragment profilFragement) {
        reunionProfilFragment = profilFragement;
    }

    /**
     * CSystem appelle cette fonction si la récupération des stats d'une personne a réussi
     *
     * @param reunions Retourne une liste de réunions
     */


    public void SuccessYourReunions(final List<Reunion> reunions) {
        reunionProfilFragment.AfficherStat(reunions);
    }

    /**
     * CSystem appelle cette fonction si la récupération des stats d'une personne a échoué
     *
     * @param message Message d'erreur
     */
    @Override
    public void ErreurYourReunions(final String message) {
        utils.toaster(this, "Impossible de récupérer les informations de cette réunion", 0);
    }

    /**
     * CSystem fait appel à cette fonction si l'action de rejoindre la réunion a réussie
     */
    public void SuccessRejoindre() {
        /// Cette Partie la est obliger de faire comme cela sinon le add n'est pas pris en compte
        List<Profil> data = Arrays.asList(detailReunion.getParticipants());
        List<Profil> participants = new ArrayList<>(data);
        participants.add(sys.userProfil);
        ///////////////

        detailReunion.setParticipants(participants.toArray(new Profil[0]));
        sys.AddLocalDetailReunion(detailReunion);

        AfficherDetailReunion(detailReunion);
        changeOnUserState(1);

        utils.toaster(getApplicationContext(), "Réunion rejointe avec succès !", 0);
    }

    /**
     * CSystem fait appel à cette fonction quand l'action de rejoindre la réunion a échouée
     *
     * @param message Message d'erreur
     */
    public void ErreurRejoindre(String message) {
        utils.toaster(this, "Impossible de rejoindre cette réunion", 0);
    }

    /**
     * CSystem fait appel à cette fonction quand l'action d'accepter une invitation a réussie
     *
     * @param resultat Vaut true si l'action a réussi, false si elle n'a pas réussie
     */
    public void SuccessAccepterInvitation(boolean resultat) {
        if (resultat) {
            List<Profil> inviter = new ArrayList<Profil>(Arrays.asList(detailReunion.getInviter()));
            for (int i = 0; i < inviter.size(); i++) {
                if (inviter.get(i).id_profil == sys.userProfil.id_profil) {
                    inviter.remove(i); // supprimer l'utilisateur de la list des inviter
                }
            }
            detailReunion.setInviter(inviter.toArray(new Profil[0]));

            List<Profil> participants = new ArrayList<>(Arrays.asList(detailReunion.getParticipants()));
            participants.add(sys.userProfil); // Ajouter
            detailReunion.setParticipants(participants.toArray(new Profil[0]));

            AfficherDetailReunion(detailReunion);
            changeOnUserState(1);

            utils.toaster(getApplicationContext(), "Invitation acceptée avec succès !", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si l'action de refuser une invitation a réussie
     *
     * @param resultat Vaut true si l'action a réussie, false si elle a échouée
     */
    public void SuccessRefuserInvitation(boolean resultat) {
        if (resultat) {
            List<Profil> inviter = new ArrayList<>(Arrays.asList(detailReunion.getInviter()));
            for (int i = 0; i < inviter.size(); i++) {
                if (inviter.get(i).id_profil == sys.userProfil.id_profil) {
                    inviter.remove(i); // supprimer l'utilisateur de la list des inviter
                }
            }
            detailReunion.setInviter(inviter.toArray(new Profil[0]));

            AfficherDetailReunion(detailReunion);
            changeOnUserState(0);

            utils.toaster(getApplicationContext(), "Invitation refusée avec succès !", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si l'action de refuser une invitation a échouée
     *
     * @param message Message d'erreur
     */
    public void ErreurInvitationsAR(String message) {
        utils.toaster(getApplicationContext(), "Impossible de répondre à cette invitation", 0);
    }

    /**
     * CSystem fait appel à cette fonction si l'action de commenter la réunion a réussie
     *
     * @param result  Vaut true si l'action a réussie, false si elle a échouée
     * @param comment Objet contenant le commentaire
     */
    public void successCommenter(boolean result, Commentaire comment) {
        if (result) {
            comment.setNom_profil(sys.userProfil.nom_profil);
            comment.setPrenom_profil(sys.userProfil.prenom_profil);
            ArrayList<Commentaire> commentaires = new ArrayList<>(Arrays.asList(detailReunion.getComments()));
            commentaires.add(comment);
            detailReunion.setComments(commentaires.toArray(new Commentaire[commentaires.size()]));
            afficherCommentaires(detailReunion);
        }
    }

    /**
     * CSystem fait appel à cette fonction si l'action de commenter a échouée
     */
    public void erreurCommenter() {
        utils.toaster(getApplicationContext(), "Impossible d'envoyer ce commentaire", 0);
    }

    /**
     * CSystem fait appel à cette fonction si l'action de quitter la réunion a échoué
     */
    public void erreurQuiterReunion() {
        utils.toaster(getApplicationContext(), "Impossible de quitter cette réunion", 0);
    }

    /**
     * CSystem fait appel à cette fonction si l'action de quitter la réunion a réussi
     *
     * @param result Vaut true si l'action a réussi, false si elle a échoué
     */
    public void succesQuiterReunion(boolean result) {
        if (result) {
            List<Profil> part = Arrays.asList(detailReunion.getParticipants());
            List<Profil> participant = new ArrayList<>(part);
            for (int i = 0; i < participant.size(); i++) {
                if (participant.get(i).id_profil == sys.userProfil.id_profil) {
                    participant.remove(i); // supprimer l'utilisateur de la list des inviter
                }
            }
            detailReunion.setParticipants(participant.toArray(new Profil[participant.size()]));

            AfficherDetailReunion(detailReunion);
            changeOnUserState(0);

            utils.toaster(getApplicationContext(), "Réunion quittée avec succès !", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si la suppression de la réunion a échoué
     */
    public void erreurSupprimerReunion() {
        utils.toaster(getApplicationContext(), "Impossible de supprimer cette réunion", 0);
    }

    /**
     * CSystem fait appel à cette fonction si la suppression de la réunion a réussi
     */
    public void succesSupprimerReunion() {
        sys.reunionDataSource.deleteReunion(detailReunion.getReunion()[0]);
        utils.toaster(getApplicationContext(), "Réunion supprimée avec succès !", 0);
        if (!sys.siViensDeCreeReu) {
            finish();
            ReunionDetailsActivity.this.overridePendingTransition(R.anim.accueil_creer_up, R.anim.accueil_creer_off);
        } else {
            sys.siViensDeCreeReu = false;
            ChangeActivity(ReunionDetailsActivity.this, AccueilActivity.class);
        }
    }

    /**
     * CSystem fait appel à cette fonction si l'ajout de la photo finish a réussi
     *
     * @param resultat Vaut true si l'action a réussi, false si elle a échoué
     */
    @Override
    public void SuccessModifReunion(boolean resultat) {
        if (resultat) // Si la réunion est bien ajouter
        {
            Picasso.get().invalidate(CSystem.URL_SITE + "/img_Events/" + detailReunion.getReunion()[0].photo_reunion);
            utils.toaster(this, "Photo finish envoyée avec succès !", 0);
            CSystem.AfficherPhoto(detailReunion.getReunion()[0].photo_reunion, header, true);
        } else // Si la réunion n'a pu être ajouter
        {
            utils.toaster(this, "Impossible d'envoyer cette photo", 0);
        }
    }

    /**
     * CSystem fait appel à cette fonction si l'ajout de la photo finish a échoué
     *
     * @param message Message d'erreur
     */
    @Override
    public void ErreurModifReunion(String message) {
        utils.toaster(getApplicationContext(), "Impossible d'envoyer cette photo", 0);
    }


    /**
     * CSystem fait appel à cette fonction si l'envoi de note a réussi
     *
     * @param result Vaut true si l'envoi a réussi, false si il a échoué
     * @param note   Objet contenant la note
     * @param detail Objet contenant les détails de la réunion
     */
    public void successNoter(boolean result, Note note, DetailReunion detail) {
        if (result) {
            List<Note> notes = new ArrayList<>(Arrays.asList(detail.getNotes()));
            notes.add(note);
            detail.setNotes(notes.toArray(new Note[0]));

            AfficherNotes(detail);
            utils.toaster(getApplicationContext(), "Note envoyé avec succès !", 0);
        } else
            utils.toaster(getApplicationContext(), "Impossible d'envoyer votre note", 0);
    }

    /**
     * CSystem fait appel à cette fonction si la modification de note a réussi
     *
     * @param result Vaut true si la modification a réussi, false si elle a échoué
     * @param note   Objet contenant la note
     * @param detail Objet contenant les détails de la réunion
     */
    public void successUpdateNoter(boolean result, Note note, DetailReunion detail) {
        if (result) {
            for (int i = 0; i < detail.getNotes().length; i++)
                if (detail.getNotes()[i].id_note == note.id_note)
                    detail.getNotes()[i].valeur_note = note.valeur_note;

            AfficherNotes(detail);
            utils.toaster(getApplicationContext(), "Note envoyé avec succès !", 0);
        } else
            utils.toaster(getApplicationContext(), "Impossible d'envoyer votre note", 0);
    }

    /**
     * CSystem fait appel à cette fonction si l'envoi ou la modification de la note a échoué
     */
    public void erreurNoter() {
        utils.toaster(getApplicationContext(), "Impossible d'envoyer votre note", 0);
    }

    /**
     * Affiche les notes sur le fragment des notes
     *
     * @param detail Objet contenant les détails de la réunion
     */
    private void AfficherNotes(final DetailReunion detail) {
        ReunionNotesFragment reunionNotesFragment = (ReunionNotesFragment) adapter.getItem(2);

        RatingBar userRating = findViewById(R.id.user_note);
        LinearLayout notesLayout = reunionNotesFragment.avisLayout;

        if (notesLayout.getChildCount() != 0)
            notesLayout.removeAllViewsInLayout();

        if (detail.getNotes() != null) {
            float avg = 0;
            for (Note note : detailReunion.getNotes()) {
                if (note.getId_profil() == sys.userProfil.getId_profil()) {
                    reunionNotesFragment.addNote(note, true);
                    userRating.setRating(note.valeur_note);
                } else
                    reunionNotesFragment.addNote(note, false);
                avg += note.valeur_note;
            }
            avg /= detail.getNotes().length;
            if (Float.isNaN(avg))
                avg = 0;
            reunionNotesFragment.setCounts(avg, detail.getNotes().length);
        }
    }
}
