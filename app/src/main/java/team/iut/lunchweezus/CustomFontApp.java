package team.iut.lunchweezus;

import android.app.Application;

/**
 * Permet d'utiliser des polices d'écriture personnalisées en remplaçant celles existantes
 */
public class CustomFontApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "font/glegoo_regular.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SANS_SERIF", "font/glegoo_bold.ttf");
    }
}