package team.iut.lunchweezus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import team.iut.lunchweezus.Beans.Notification;
import team.iut.lunchweezus.Beans.Reunion;
import team.iut.lunchweezus.Logique.CSystem;
import team.iut.lunchweezus.Logique.onEventListener;
import team.iut.lunchweezus.ui.notifications.NotificationsAdapter;
import team.iut.lunchweezus.ui.notifications.NotificationsModel;
import team.iut.lunchweezus.ui.utils;

public class NotificationsActivity extends ActivityFonction {

    private ListView listLayout;
    // Statut & contexte de la notification
    private final int
            INVIT = 0, // Notification d'invitation
            JOIN = 1, // Quand quelqu'un a rejoint
            MODIF = 2, // Si une réu a été modifiée
            COMMENT = 3, // Si c'est un commentaire qui a été reçu
            QUIT = 4, // Si quelqu'un a quitté la réunion
            DELETE = 5, // Si une réunion a été supprimée
            ACCEPT = 6, // Si quelqu'un a accepté votre invitation
           //todo ici aussi
            ANNONCE = 7; // Lorsque les annonces sont faite en fin de réunion
    String
            datePattern = "dd/MM/yyyy",
            timePattern = "HH:mm",
            date,
            time;
    Date dateObj;
    private NotificationsAdapter notificationsAdapter;
    private ArrayList<NotificationsModel> notificationsModelArrayList;

    @Override
    public void Debut() {
        sys = new CSystem();
        sys.Start(this); // s'iniasilise dans CSystem
        sys.GetUserProfilLocal();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        Debut();


        final Toolbar toolbar = findViewById(R.id.notifications_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "Mes notifications");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        listLayout = findViewById(R.id.notifications_sv);

        sys.getNotifications();

        final SwipeRefreshLayout refresh = findViewById(R.id.notif_refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sys.getNotifications();
                refresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        Debut();

        sys.getNotifications();

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notif_activity_action, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.clear_notif) {
            sys.suppCheckedNotification();
            sys.getNotifications();
        } else if (id == R.id.clear_all_notif) {
            AlertDialog.Builder ab = utils.alert(this,
                    "Supprimer toutes les notifications",
                    "Voulez-vous vraiment supprimer toutes vos notifications ?",
                    0,
                    null,
                    null,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sys.suppAllNotif();
                            sys.getNotifications();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            ab.show();
        } else {
            ChangeActivity(NotificationsActivity.this, AccueilActivity.class);
            finish();
            NotificationsActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        }
        return true;
    }

    /**
     * Crée une liste de vues contenant les notifications à partir d'un modèle de vue
     *
     * @param n Liste d'objets contenant les notifications et leurs informations
     * @return Retourne la liste
     */
    private ArrayList<NotificationsModel> populateList(List<Notification> n) {

        ArrayList<NotificationsModel> list = new ArrayList<>();

        for (int i = 0; i < n.size(); i++) {
            NotificationsModel model = new NotificationsModel();

            model.setNotificationId(n.get(i).getId_notif());
            model.setReunionId(n.get(i).getId_reunion());
            model.setStatus(n.get(i).getStatu_notif());
            model.setType(n.get(i).getType_notif());

            switch (n.get(i).getType_notif()) {
                case INVIT: // Statut = 0, vous êtes invité à une réunion
                    model.setTitle("Invitation");
                    model.setImageDrawable(getDrawable(R.drawable.ic_reply));
                    break;
                case JOIN: // Statut = 1, quelqu'un a rejoin votre réunion
                    model.setTitle("Nouveau participant");
                    model.setImageDrawable(getDrawable(R.drawable.ic_person_add));
                    break;
                case MODIF: // Statut = 2, la réunion a été modifiée
                    model.setTitle("Modification");
                    model.setImageDrawable(getDrawable(R.drawable.ic_edit));
                    break;
                case COMMENT: // Statut = 3, un commentaire a été envoyé sur la réunion
                    model.setTitle("Commentaire");
                    model.setImageDrawable(getDrawable(R.drawable.ic_commentaires));
                    break;
                case QUIT: // Statut = 4, quelqu'un a quitté la réunion
                    model.setTitle("Un participant est parti");
                    model.setImageDrawable(getDrawable(R.drawable.ic_quit));
                    break;
                case DELETE: // Statut = 5, la réunion a été annulée / supprimée
                    model.setTitle("Réunion annulée");
                    model.setImageDrawable(getDrawable(R.drawable.ic_croix));
                    break;
                case ACCEPT:
                    model.setTitle("Invitation acceptée");
                    model.setImageDrawable(getDrawable(R.drawable.ic_reply));
                    break;
                //todo ajout d'un nouveau type de notification
                case ANNONCE:
                    model.setTitle("Annonce");
                    model.setImageDrawable(getDrawable(R.drawable.ic_notifications));
                    break;
            }


            model.setContent(n.get(i).getContenu_notif());

            try {
                dateObj = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(n.get(i).getDate_notif());
                date = new SimpleDateFormat(datePattern).format(dateObj);
                time = new SimpleDateFormat(timePattern).format(dateObj);

                model.setTimestamp("Reçu le " + date + " à " + time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            list.add(model);
        }

        return list;
    }

    /**
     * CSystem fait appel à cette fonction si la récupération des notifications a réussi
     *
     * @param notifications Liste contenant les notifications récupérées
     */
    @Override
    public void successNotifications(List<Notification> notifications) {
        if (listLayout.getChildCount() != 0) {
            listLayout.removeAllViewsInLayout();
        }

        notificationsModelArrayList = populateList(notifications);

        notificationsAdapter = new NotificationsAdapter(getApplicationContext(), notificationsModelArrayList);
        if (notificationsAdapter.getViewTypeCount() >= 1) {
            listLayout.setAdapter(notificationsAdapter);

            final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                    new SwipeToDismissTouchListener<>(
                            new ListViewAdapter(listLayout),
                            new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                                @Override
                                public boolean canDismiss(int position) {
                                    return true;
                                }

                                @Override
                                public void onPendingDismiss(ListViewAdapter recyclerView, int position) {
                                    // DO NOT REMOVE
                                }

                                @Override
                                public void onDismiss(ListViewAdapter recyclerView, int position) {
                                    int notificationId = notificationsModelArrayList.get(position).getNotificationId();
                                    sys.suppNotif(notificationId);

                                    notificationsAdapter.remove(position);
                                }
                            });
            touchListener.setDismissDelay(0);

            listLayout.setOnTouchListener(touchListener);
            listLayout.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
            listLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int notificationId = notificationsModelArrayList.get(position).getNotificationId();
                    int reunionId = notificationsModelArrayList.get(position).getReunionId();
                    int notificationType = notificationsModelArrayList.get(position).getType();

                    if (notificationType != DELETE) {
                        Reunion r = new Reunion();
                        r.id_reunion = reunionId;
                        sys.reunionSelected = r;
                        sys.setNotifChecked(notificationId, new onEventListener() {
                            @Override
                            public void onSuccess(Object test) {
                                ChangeActivity(NotificationsActivity.this, ReunionDetailsActivity.class);
                                successCheckNotifications();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                ChangeActivity(NotificationsActivity.this, ReunionDetailsActivity.class);
                            }
                        });

                    } else {
                        sys.setNotifChecked(notificationId, new onEventListener() {
                            @Override
                            public void onSuccess(Object test) {
                                ChangeActivity(NotificationsActivity.this, AccueilActivity.class);
                                successCheckNotifications();
                                finish();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                ChangeActivity(NotificationsActivity.this, AccueilActivity.class);
                                finish();
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * CSystem fait appel à cette fonction si la requête confirmant l'ouverture de la notification a réussi
     *
     */
    public void successCheckNotifications() {
        if (listLayout.getChildCount() != 0) {
            listLayout.removeAllViewsInLayout();
        }
    }

    @Override
    public void onBackPressed() {
        ChangeActivity(NotificationsActivity.this, AccueilActivity.class);
        finish();
        super.onBackPressed();
    }
}
