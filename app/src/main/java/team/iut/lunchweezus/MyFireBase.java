package team.iut.lunchweezus;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import team.iut.lunchweezus.Logique.CSystem;

public class MyFireBase extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: MMMAMAMAMAMAMAMAMAMAMAMAMAMMAMAMAMMAMAMAMAMAMA " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getNotification().getBody().length() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getNotification().getBody());

            String title, body;
            title = remoteMessage.getNotification().getTitle();
            body = remoteMessage.getNotification().getBody();


            Intent intent = new Intent(this, NotificationsActivity.class);
            intent.putExtra("CSystem", new CSystem());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setContentTitle(title);
            builder.setContentText(body);
            builder.setContentIntent(pendingIntent);
            builder.setSound(soundUri);
            builder.setSmallIcon(R.mipmap.ic_launcher);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, builder.build());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
