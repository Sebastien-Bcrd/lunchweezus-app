package team.iut.lunchweezus;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import team.iut.lunchweezus.ui.utils;

public class AboutActivity extends AppCompatActivity {
    LinearLayout changelog, cguElm, dataElm, firebase, foursquare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        final Toolbar toolbar = findViewById(R.id.about_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            utils.makeToolbarTitle(this, getSupportActionBar(), "A propos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        final LayoutInflater inflater = this.getLayoutInflater();

        changelog = findViewById(R.id.changelog_elm);
        changelog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = inflater.inflate(R.layout.conteneur_changelog, null);
                utils.viewDialog(v.getContext(), "Notes de mise à jour", dialogView).show();
            }
        });

        cguElm = findViewById(R.id.cgu_elm);
        cguElm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = inflater.inflate(R.layout.conteneur_cgu, null);
                utils.viewDialog(v.getContext(), "Charte pour la protection des données", dialogView).show();
            }
        });

        dataElm = findViewById(R.id.data_elm);
        dataElm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = inflater.inflate(R.layout.conteneur_charte_data, null);
                utils.viewDialog(v.getContext(), "Charte pour la protection des données", dialogView).show();
            }
        });

        firebase = findViewById(R.id.firebase_terms);
        firebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://developers.google.com/terms/"));
                startActivity(intent);
            }
        });

        foursquare = findViewById(R.id.foursquare_terms);
        foursquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.foursquare.com/legal/api/licenseagreement"));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        AboutActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        AboutActivity.this.overridePendingTransition(R.anim.accueil_details_up, R.anim.accueil_details_off);
        super.onBackPressed();
    }
}
